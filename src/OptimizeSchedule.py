# This code takes an input schedule and optimizes it further by randomized
# swapping of talks using Simulated Annealing while accommodating constraints for
# presentation in three different sets of sessions.
import argparse
import sys
import copy
import traceback
import functools
import numpy
import json
import tqdm
from itertools import combinations
from random import random, randrange
from multiprocessing import Process, Pool, cpu_count
from math import exp, ceil


def loadpreferences(filenames, column, blocks, num_talks):
    if num_talks == 0:
        return None

    talk_preferences = []

    if filenames == None:
        # Not using blocks, so make every talk compatible with block 0
        talk_preferences = [[0]] * num_talks
    else:
        no_preference = [*range(0, blocks)]

        # Load the time preferences
        for filename in filenames:
            with open(filename, "r") as file:
                for line in file:
                    cols = line.split("\t")

                    prefs_str = cols[column - 1].upper()
                    if prefs_str == "NP":
                        pref = no_preference
                    else:
                        pref = [ord(char) - ord("A") for char in prefs_str]

                    talk_preferences.append(pref)

    return talk_preferences


def populateschedulefromfile(filename):
    schedule = []
    with open(filename) as f:
        schedule = json.load(f)
    return schedule


def calculate(schedule, num_talks, scorematrix):
    intersimlist = []
    intrasimlist = []

    for block in schedule:
        for timeslot in block:
            # Calculate intra-session similarity (good!)
            for session in timeslot:
                if len(session) == 1:
                    continue
                else:
                    for i, j in combinations(range(len(session)), 2):
                        intrasimlist.append(scorematrix[session[i]][session[j]])

            # Calculate inter-session similarity (bad!)
            for i, j in combinations(range(len(timeslot)), 2):
                docs = timeslot[i] + timeslot[j]
                for k, l in combinations(docs, 2):
                    # Session indices are stored as negative numbers in the schedule,
                    # and their similarity scores appear following the individual
                    # talks
                    if k < 0:
                        k = -k - 1 + num_talks
                    if l < 0:
                        l = -l - 1 + num_talks

                    intersimlist.append(scorematrix[k][l])

    return intersimlist, intrasimlist


def populatescorematrix(filename):
    scorematrix = []
    with open(filename, "r") as file:
        for line in file:
            rowscores = [float(s.strip()) for s in line.split("\t")]
            scorematrix.append(rowscores)
    return scorematrix


def kirkpatrick_cooling(start_temp, alpha):
    T = start_temp
    while True:
        yield T
        T = alpha * T


def trace_unhandled_exceptions(func):
    @functools.wraps(func)
    def wrapped_func(*args, **kwargs):
        try:
            return func(*args, **kwargs)
        except:
            print("Exception in " + func.__name__)
            traceback.print_exc()

    return wrapped_func


@trace_unhandled_exceptions
def worker(
    schedule,
    scorematrix,
    indiv_preferences,
    sess_preferences,
    iterations,
    num_talks,
    num_sessions,
    temperature,
    alpha,
    worker_number,
):
    epsilon = 1.0e-8
    cooling = kirkpatrick_cooling(temperature, alpha)
    intrasimlist = []
    intersimlist = []
    drdistribution = []

    intersimlist, intrasimlist = calculate(schedule, num_talks, scorematrix)
    intramean = numpy.mean(intrasimlist)
    intermean = numpy.mean(intersimlist)
    drmean = intramean / intermean
    bestsolution = drmean
    bestschedule = copy.deepcopy(schedule)
    drdistribution.append(drmean)

    better = 0
    same = 0
    talk_probability = num_talks / (num_sessions + num_talks)
    probability = 0.0

    for temperature in cooling:
        if temperature < epsilon:
            break

        if better < iterations:
            is_session = random() > talk_probability

            if is_session:
                x = -randrange(num_sessions) - 1
                y = -randrange(num_sessions) - 1
            else:
                x = randrange(num_talks)
                y = randrange(num_talks)

            if x == y:
                continue

            for block in schedule:
                for timeslot in block:
                    for session in timeslot:
                        if x in session:
                            xindex = session.index(x)
                            xsessionindex = timeslot.index(session)
                            xtimeslotindex = block.index(timeslot)
                            xblockindex = schedule.index(block)

            for block in schedule:
                for timeslot in block:
                    for session in timeslot:
                        if y in session:
                            yindex = session.index(y)
                            ysessionindex = timeslot.index(session)
                            ytimeslotindex = block.index(timeslot)
                            yblockindex = schedule.index(block)

            # See if X could live in Y's block, and vice-versa; if not, just skip
            if is_session:
                xprefs = sess_preferences[-x - 1]
                yprefs = sess_preferences[-y - 1]
            else:
                xprefs = indiv_preferences[x]
                yprefs = indiv_preferences[y]

            if xblockindex not in yprefs or yblockindex not in xprefs:
                continue

            better += 1
            tempschedule = copy.deepcopy(schedule)
            (
                tempschedule[yblockindex][ytimeslotindex][ysessionindex][yindex],
                tempschedule[xblockindex][xtimeslotindex][xsessionindex][xindex],
            ) = (
                tempschedule[xblockindex][xtimeslotindex][xsessionindex][xindex],
                tempschedule[yblockindex][ytimeslotindex][ysessionindex][yindex],
            )

            intersimlist, intrasimlist = calculate(tempschedule, num_talks, scorematrix)

            intramean = numpy.mean(intrasimlist)
            intermean = numpy.mean(intersimlist)

            if intermean < epsilon:
                # No dividing by zero
                newdrmean = drmean
            else:
                newdrmean = intramean / intermean

            if abs(newdrmean - drmean) < epsilon:
                probability = 0.0
            elif newdrmean > bestsolution:
                bestsolution = newdrmean
                bestschedule = copy.deepcopy(tempschedule)
                probability = 1.0
            else:
                probability = exp(-abs(newdrmean - drmean) / temperature)

            if random() < probability:
                schedule = copy.deepcopy(tempschedule)
                drmean = newdrmean
                drdistribution.append(newdrmean)
                same = 0
            else:
                drdistribution.append(drmean)
                same += 1

                if same == 1000:
                    break
        else:
            break

    return (worker_number, drdistribution, bestsolution, bestschedule)


def writetofile(ofileloc, results):
    with open(ofileloc, "w") as ofile:
        for i, resultstuple in enumerate(results):
            ofile.write(str(resultstuple[0]) + "\t" + str(resultstuple[2]) + "\t")
            json.dump(resultstuple[3], ofile)
            ofile.write("\n")


def main():
    parser = argparse.ArgumentParser(
        description="Optimize an input schedule with simulated annealing",
        formatter_class=argparse.ArgumentDefaultsHelpFormatter,
    )
    parser.add_argument(
        "schedule",
        metavar="<Schedule.txt>",
        type=str,
        help="the initial schedule file to start swapping from",
    )
    parser.add_argument(
        "similarity",
        metavar="<DocumentSimilarity.txt>",
        type=str,
        help="the document similarity matrix",
    )
    parser.add_argument(
        "-f",
        "--abstract-file",
        type=str,
        metavar="FILE",
        nargs="*",
        help="if using multiple blocks, the file containing abstracts and time preferences (pass multiple files and they will be concatenated)",
    )
    parser.add_argument(
        "-e",
        "--session-file",
        type=str,
        metavar="FILE",
        nargs="*",
        help="if using sessions, the file containing session abstracts and time preferences (pass multiple files and they will be concatenated)",
    )
    parser.add_argument(
        "-p",
        "--prefs-column",
        type=int,
        default=3,
        metavar="NUM",
        help="column in TSV file (1-indexed) containing block preferences (only used with --abstract-file)",
    )
    parser.add_argument(
        "-r",
        "--runs",
        type=int,
        default=200,
        metavar="NUM",
        help="number of times to run the entire algorithm",
    )
    parser.add_argument(
        "-i",
        "--iterations",
        type=int,
        default=250000,
        metavar="NUM",
        help="number of iterations per run (upper bound)",
    )
    parser.add_argument(
        "-o",
        "--out",
        type=str,
        default="Optimized.txt",
        metavar="FILE",
        help="output path for optimized schedule",
    )
    parser.add_argument(
        "-t",
        "--temperature",
        type=float,
        default=50000.0,
        metavar="FLOAT",
        help="initial temperature value for simulated annealing",
    )
    parser.add_argument(
        "-a",
        "--alpha",
        type=float,
        default=0.99,
        metavar="FLOAT",
        help="temperature decay parameter for simulated annealing",
    )
    parser.add_argument(
        "-w",
        "--num-workers",
        type=int,
        default=cpu_count(),
        metavar="NUM",
        help="number of worker processes to spawn (default is your number of CPU cores)",
    )

    args = parser.parse_args()

    pool = Pool(processes=args.num_workers)
    schedule = populateschedulefromfile(args.schedule)
    scorematrix = populatescorematrix(args.similarity)

    blocks = len(schedule)
    num_talks = 0
    num_sessions = 0
    for block in schedule:
        for timeslot in block:
            for session in timeslot:
                for talk in session:
                    if talk > num_talks:
                        num_talks = talk
                    if talk < num_sessions:
                        num_sessions = talk
    num_talks += 1
    num_sessions = -num_sessions

    indiv_preferences = loadpreferences(
        args.abstract_file, args.prefs_column, blocks, num_talks
    )
    sess_preferences = loadpreferences(
        args.session_file, args.prefs_column, blocks, num_sessions
    )

    workerfunc = functools.partial(
        worker,
        schedule,
        scorematrix,
        indiv_preferences,
        sess_preferences,
        args.iterations,
        num_talks,
        num_sessions,
        args.temperature,
        args.alpha,
    )

    job_numbers = [i for i in range(args.runs)]

    with Pool(processes=args.num_workers) as p:
        results = list(
            tqdm.tqdm(
                p.imap_unordered(
                    workerfunc,
                    job_numbers,
                    chunksize=ceil(len(job_numbers) / args.num_workers),
                ),
                total=len(job_numbers),
            )
        )

    writetofile(args.out, results)


if __name__ == "__main__":
    main()
