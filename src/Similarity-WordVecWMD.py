# This code determines document similarity using the Stanford glove word2vec
# model.
#
# To run it, create TSV files containing the documents you want to read, in
# order, with at least the titles and abstracts of the papers and sessions you
# want to classify.

# To run it, create a TSV file containing one document per line and, at least, the
# titles and abstracts of each document.
#
# Then check out the various configurable parameters and output files by calling
# > python Similarity-WordVecWMD.py --help
import argparse
import logging
from math import ceil
from itertools import combinations
from functools import partial
from multiprocessing import Pool, cpu_count
import tqdm
from pyemd import emd
from nltk import corpus
from gensim import utils
import gensim.downloader as api


def create_stop_list():
    return set(corpus.stopwords.words("english"))


def prepare_text(filenames, title_column, abstract_column, stop_list):
    out = []

    for filename in filenames:
        with open(filename, "r") as file:
            for line in file:
                cols = line.split("\t")
                text = cols[title_column - 1] + " " + cols[abstract_column - 1]

                tokens = utils.simple_preprocess(text)
                out.append(tokens)

    return out


def distance(model, text, idxs):
    if idxs[0] == idxs[1]:
        return (idxs[0], idxs[1], 0)
    return (idxs[0], idxs[1], model.wmdistance(text[idxs[0]], text[idxs[1]]))


def write_similarity_matrix(text, num_workers, filename):
    logging.info("loading gigaword model")
    model = api.load("glove-wiki-gigaword-300")

    # Calculate distances
    corpus_size = len(text)
    tuples = [el for el in combinations(range(corpus_size), 2)]
    distfunc = partial(distance, model, text)

    with Pool(processes=num_workers) as p:
        ret = list(
            tqdm.tqdm(
                p.imap_unordered(
                    distfunc, tuples, chunksize=ceil(len(tuples) / num_workers)
                ),
                total=len(tuples),
            )
        )

    score_matrix = [[0 for x in range(corpus_size)] for x in range(corpus_size)]
    max_distance = 0
    for idx1, idx2, dist in ret:
        score_matrix[idx1][idx2] = dist
        score_matrix[idx2][idx1] = dist

        if dist > max_distance:
            max_distance = dist

    # Normalize these to scaled similarities of the kind the other tools want
    for row in range(corpus_size):
        for col in range(corpus_size):
            s = score_matrix[row][col]
            s = 1.0 - (s / max_distance)
            score_matrix[row][col] = s

    # Write
    with open(filename, "w") as matrixfile:
        for row in range(0, corpus_size):
            scores = []
            for col in range(0, corpus_size):
                scores.append(str(score_matrix[row][col]))
            matrixfile.write("\t".join(scores) + "\n")


def main():
    parser = argparse.ArgumentParser(
        description="Compute document similarity with word2vec and WMD",
        formatter_class=argparse.ArgumentDefaultsHelpFormatter,
    )
    parser.add_argument(
        "-t",
        "--title-column",
        type=int,
        default=1,
        metavar="COL",
        help="column in TSV file (1-indexed) containing titles",
    )
    parser.add_argument(
        "-a",
        "--abstract-column",
        type=int,
        default=2,
        metavar="COL",
        help="column in TSV file (1-indexed) containing abstracts",
    )
    parser.add_argument(
        "-w",
        "--num-workers",
        type=int,
        default=cpu_count(),
        metavar="NUM",
        help="number of worker processes to spawn (default is your number of CPU cores)",
    )
    parser.add_argument(
        "-l",
        "--log",
        type=str,
        default="Log.txt",
        metavar="FILE",
        help="output path to save debug log",
    )
    parser.add_argument(
        "-o",
        "--out",
        type=str,
        default="DocumentSimilarity.txt",
        metavar="FILE",
        help="output path for document similarity matrix",
    )
    parser.add_argument(
        "filenames",
        metavar="<file.tsv>",
        type=str,
        nargs="+",
        help="a file with titles and abstracts (multiple files will be concatenated)",
    )

    args = parser.parse_args()

    logging.basicConfig(
        format="%(asctime)s : %(levelname)s : %(message)s",
        level=logging.DEBUG,
        filename=args.log,
        filemode="w",
    )

    stop_list = create_stop_list()
    text_tokens = prepare_text(
        args.filenames, args.title_column, args.abstract_column, stop_list
    )

    write_similarity_matrix(text_tokens, args.num_workers, args.out)


if __name__ == "__main__":
    main()
