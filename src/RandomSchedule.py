# Creates a randomly population schedule that, optionally, takes into account the
# submitters' preferences for being scheduled into various "blocks" of sessions
# (think choices of session time for a large online conference).
#
# To see the parameters, run:
# > python RandomSchedule.py --help
#
# The conference organization will be:
# - Block A
#   - Time Slot 1
#     - Session 1
#       - Talk 1
#       - Talk 2
#       ...
#     - Session 2
#     ...
#     - Session N
#   - Time Slot 2
#   ...
# - Block B
# ...
#
# Time slots per block can either be a single number (same number of time slots in
# each block) or a list separated by commas (like 2,3,4,5), to specify different numbers
# of time slots for each block.
#
# The users specify their preferences in one of the columns of the abstracts file.
# This column should contain either "NP" (no preference), or some combination of the
# letters "AB..." to specify each preferred block. (NP is thus equivalent to all of
# the letters). Specify which column this is as the second argument to the script
# (this is one-indexed, as spreadsheet columns usually are).
#
# This program will then generate a random schedule that is (1) consistent with the
# expressed preferences, and (2) attempts to generate as close to an equal number of
# sessions as possible consistent with those preferences. (That is, 'N' above is not
# fixed in advance and will be computed by this script, and may differ in each
# different block.)
#
# Support is also present for loading a file of sessions in addition to a file of
# individual talks (see the top-level README for the difference).
import argparse
import sys
import json
from random import choice, shuffle
from math import ceil
import string


def randomizeblock(block_src, numslots, talkspersession):
    ret = []
    block = block_src[:]

    for _ in range(0, numslots):
        ret.append([])

    while len(block) > 0:
        for slot in range(0, numslots):
            session = []
            for _ in range(0, min(talkspersession, len(block))):
                r = choice(block)
                block.remove(r)
                session.append(r)

            ret[slot].append(session)

            if len(block) == 0:
                break

    return ret


def putsessionsinblocks(randomschedule, timeslotsperblock, time_preferences):
    # Don't mess with the original variable
    prefs = time_preferences[:]

    # Randomize the session list
    shuffle(prefs)

    # Until we're out of sessions, find the smallest time-slot, and add the next
    # available session to it that's compatible with it
    still_working = [True for block in randomschedule]

    while len(prefs) > 0:
        min_slot = 999999

        for bn, block in enumerate(randomschedule):
            if not still_working[bn]:
                continue

            slot_lens = [len(ts) for ts in block]
            smallest_slot = min(slot_lens)

            if smallest_slot < min_slot:
                bidx = bn
                sidx = slot_lens.index(smallest_slot)
                min_slot = smallest_slot

            # Look for sessions compatible with that block
            found = False

            block_char = chr(ord("A") + bidx)

            for tup in prefs[:]:
                i, session = tup
                idx = session.find(block_char)
                if idx == -1:
                    continue

                randomschedule[bidx][sidx].append([i])
                prefs.remove(tup)
                found = True
                break

            if not found:
                # No talks left for this block, stop working on it
                still_working[bidx] = False

    return randomschedule


def puttalksinblocks(ret, timeslotsperblock, time_preferences):
    # Don't mess with the original variable
    prefs = time_preferences[:]

    # First, go through and fix all the talks that expressed exactly one preference
    for tup in prefs[:]:
        i, talk = tup
        if len(talk) == 1:
            block = ord(talk) - ord("A")

            ret[block].append(i)
            prefs.remove(tup)

    # Randomize what's left
    shuffle(prefs)

    # Build our "fullness" calculator
    full_goal = [x / max(timeslotsperblock) for x in timeslotsperblock]
    full = [len(x) for x in ret]
    full = [x / max(full) for x in full]

    # Until we're out of talks, fill the least-full block with the next available talk
    # that's compatible with it, if there are any that remain
    while len(prefs) > 0:
        diffs = [g - f for f, g in zip(full, full_goal)]

        block = diffs.index(max(diffs))
        block_char = chr(ord("A") + block)

        # Looking for talks compatible with that block
        found = False

        for tup in prefs[:]:
            i, talk = tup
            idx = talk.find(block_char)
            if idx == -1:
                continue

            ret[block].append(i)
            prefs.remove(tup)
            found = True

            # Update the fullness counter
            full = [len(x) for x in ret]
            full = [x / max(full) for x in full]
            break

        if not found:
            # There are no talks left that are compatible with this block, so stop
            # trying to find talks for it (set its fullness goal to 0)
            full_goal[block] = 0

    return ret


def main():
    parser = argparse.ArgumentParser(
        description="Prepare randomized conference schedule with given numbers of blocks, time-slots, and sessions",
        formatter_class=argparse.ArgumentDefaultsHelpFormatter,
    )
    parser.add_argument(
        "-f",
        "--abstract-file",
        type=str,
        metavar="FILE",
        nargs="*",
        help="if using multiple blocks, the file containing abstracts and time preferences (pass multiple files and they will be concatenated)",
    )
    parser.add_argument(
        "-e",
        "--session-file",
        type=str,
        metavar="FILE",
        nargs="*",
        help="if also scheduling sessions, the file containing session abstracts and time preferences (pass multiple files and they will be concatenated",
    )
    parser.add_argument(
        "-p",
        "--prefs-column",
        type=int,
        default=3,
        metavar="NUM",
        help="column in TSV files (1-indexed) containing block preferences (used with --abstract-file and --session-file)",
    )
    parser.add_argument(
        "-n",
        "--num-talks",
        type=int,
        metavar="NUM",
        help="the number of talks to schedule (only used when not using --abstract-file)",
    )
    parser.add_argument(
        "-b",
        "--blocks",
        type=int,
        required=True,
        metavar="NUM",
        help="number of blocks in the conference",
    )
    parser.add_argument(
        "-s",
        "--time-slots",
        type=str,
        required=True,
        metavar="NUM|LIST",
        help='number of time slots per block (either a single number, for the same number of time slots in every block, or a comma-separated list, like "1,2,3,4", for different numbers of time slots in each block',
    )
    parser.add_argument(
        "-t",
        "--talks",
        type=int,
        required=True,
        metavar="NUM",
        help="number of talks per time slot",
    )
    parser.add_argument(
        "-o",
        "--out",
        type=str,
        default="RandomSchedule.txt",
        metavar="FILE",
        help="output path for randomized schedule",
    )

    args = parser.parse_args()

    # Time slots per block can either be a single integer (all blocks same number of
    # time slots) or a list separated by commas ("3,3,2,4") for different numbers of
    # time slots per block
    if args.time_slots.find(",") == -1:
        num = int(args.time_slots)
        time_slots = [num] * args.blocks
    else:
        time_slots = [int(x) for x in args.time_slots.split(",")]
        if len(time_slots) != args.blocks:
            sys.exit("--time_slots list doesn't have the right number of slots")

    no_preference = "".join(list(string.ascii_uppercase)[: args.blocks])
    num_talks = 0
    talk_preferences = []

    if args.abstract_file:
        # Load the time preferences
        for filename in args.abstract_file:
            with open(filename, "r") as file:
                for line in file:
                    cols = line.split("\t")

                    prefs_str = cols[args.prefs_column - 1].upper()
                    if prefs_str == "NP":
                        prefs_str = no_preference

                    talk_preferences.append((num_talks, prefs_str))
                    num_talks += 1
    else:
        if args.num_talks == None:
            sys.exit("you must pass either --abstract-file or --num-talks")

        num_talks = args.num_talks
        talk_preferences = [(i, no_preference) for i in range(num_talks)]

    # Use the time preferences to make an initial random division of talks into the
    # blocks, as equally as possible
    talks_in_blocks = []
    for _ in range(args.blocks):
        talks_in_blocks.append([])
    talks_in_blocks = puttalksinblocks(talks_in_blocks, time_slots, talk_preferences)

    # Turn each block into a random "mini-schedule"
    randomschedule = []
    for block, numslots in zip(talks_in_blocks, time_slots):
        randomschedule.append(randomizeblock(block, numslots, args.talks))

    # Are we adding sessions?
    if args.session_file:
        num_sessions = 1
        session_preferences = []

        # Load the session time preferences
        for filename in args.session_file:
            with open(filename, "r") as file:
                for line in file:
                    cols = line.split("\t")

                    prefs_str = cols[args.prefs_column - 1].upper()
                    if prefs_str == "NP":
                        prefs_str = no_preference

                    session_preferences.append((-num_sessions, prefs_str))
                    num_sessions += 1

        # Add the sessions to the blocks as well
        randomschedule = putsessionsinblocks(
            randomschedule, time_slots, session_preferences
        )

    with open(args.out, "w") as out:
        json.dump(randomschedule, out)


if __name__ == "__main__":
    main()
