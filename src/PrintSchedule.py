# Prints out the sessions in a schedule after optimization has been run. You
# can either specify a particular run number that you would like, or without
# one, it'll take the largest value (beware of artifacts).
#
# For information about the available parameters, run:
# > python PrintSchedule.py --help
import argparse
import json


def readabstracts(filenames):
    ret = []
    for infile in filenames:
        with open(infile, "r") as file:
            for line in file:
                ret.append(line.strip().split("\t"))
    return ret


def readschedulelist(infile):
    ret = []
    with open(infile, "r") as file:
        for line in file:
            elts = line.strip().split("\t")
            sched = dict(run_number=int(elts[0]), score=float(elts[1]), sched=elts[2])
            ret.append(sched)

    return ret


def findbestschedule(l):
    item = max(l, key=lambda item: item["score"])
    return item["sched"]


def findschedulenum(l, num):
    return [x for x in l if x["run_number"] == num][0]["sched"]


def main():
    parser = argparse.ArgumentParser(
        description="Print a schedule in a human-readable format",
        # formatter_class=argparse.ArgumentDefaultsHelpFormatter,
    )
    parser.add_argument(
        "-t",
        "--title-column",
        type=int,
        default=1,
        metavar="COL",
        help="column in TSV file (1-indexed) containing titles",
    )
    parser.add_argument(
        "-p",
        "--prefs-column",
        type=int,
        metavar="COL",
        help="column in TSV file (1-indexed) containing time preferences (if desired)",
    )
    parser.add_argument(
        "-e",
        "--session-file",
        type=str,
        metavar="FILE",
        nargs="*",
        help="if also scheduling sessions, the file containing session abstracts and time preferences (pass multiple files and they will be concatenated",
    )
    parser.add_argument(
        "-r",
        "--run",
        type=int,
        default=-1,
        metavar="NUM",
        help="the run number you would like to print (defaults to printing the run with the highest solution score)",
    )
    parser.add_argument(
        "schedule", metavar="<schedule.txt>", type=str, help="the schedule to print"
    )
    parser.add_argument(
        "filenames",
        metavar="<file.tsv>",
        type=str,
        nargs="+",
        help="a file with titles and abstracts (multiple files will be concatenated)",
    )

    args = parser.parse_args()

    abstracts = readabstracts(args.filenames)
    if args.session_file:
        session_abstracts = readabstracts(args.session_file)

    schedule_list = readschedulelist(args.schedule)

    if args.run == -1:
        schedule = findbestschedule(schedule_list)
    else:
        schedule = findschedulenum(schedule_list, args.run)

    schedule = json.loads(schedule)

    for bn, block in enumerate(schedule):
        if len(schedule) > 1:
            # Only print block ID information if there was more than one block
            print("===== Block %s =====" % (chr(bn + ord("A"))))
            print("")

        for tsn, slot in enumerate(block):
            print("----- Time Slot %2d -----" % (tsn + 1))
            print("")

            for sn, session in enumerate(slot):
                if len(session) == 0:
                    continue

                print("Session %d:" % (sn + 1))
                if len(session) == 1:
                    session_num = -session[0] - 1
                    alist = [session_abstracts[session_num]]
                else:
                    alist = [abstracts[talk] for talk in session]

                for a in alist:
                    if args.prefs_column == None:
                        pref = ""
                    else:
                        pref = "[%s] " % (a[args.prefs_column - 1])

                    print(
                        "  - %s%s"
                        % (
                            pref,
                            a[args.title_column - 1],
                        ),
                    )
                print("")
            print("")

        print("")
        print("")


if __name__ == "__main__":
    main()
