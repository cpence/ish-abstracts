# This code conducts topic modeling on a given corpus using LDA models. It also
# computes pairwise similarities between every pair of documents/talks in the
# corpus based on the topic model.
#
# To run it, create a TSV file containing one document per line and, at least, the
# titles and abstracts of each document.
#
# Then check out the various configurable parameters and output files by calling
# > python Similarity-LDA.py --help
import argparse
import logging
import os
import nltk
import spacy
import numpy
from spacy.lang.en import English
from gensim import corpora
from gensim.models import ldamodel, coherencemodel
from nltk.corpus import wordnet as wn
from nltk.corpus import stopwords as sw
from itertools import combinations
from gensim.matutils import cossim, hellinger


def lemma(word):
    lemma = wn.morphy(word)
    if lemma is None:
        return word
    else:
        return lemma


def create_stop_list():
    return set(sw.words("english"))


def prepare_text(filenames, title_column, abstract_column, stop_list):
    out = []
    parser = English()

    for filename in filenames:
        with open(filename, "r") as file:
            for line in file:
                cols = line.split("\t")
                text = cols[title_column - 1] + " " + cols[abstract_column - 1]

                tokens = parser(text)
                tokens = [token for token in tokens if not token.orth_.isspace()]
                tokens = [token for token in tokens if not token.orth_.isdigit()]
                tokens = [token.lower_ for token in tokens]
                tokens = [token for token in tokens if len(token) > 2]
                tokens = [token for token in tokens if token not in stop_list]
                tokens = [lemma(token) for token in tokens]

                out.append(tokens)

    return out


def write_similarity_matrix(model, lda_corpus, use_hd, filename):
    corpus_size = len(lda_corpus)
    score_matrix = [[0 for x in range(corpus_size)] for x in range(corpus_size)]
    tuples = [el for el in combinations(range(corpus_size), 2)]

    # The tuples don't include the matrix diagonal
    for i in range(corpus_size):
        score_matrix[i][i] = 1.0

    for row, col in tuples:
        doc1 = model.get_document_topics(lda_corpus[row], minimum_probability=0.0)
        doc2 = model.get_document_topics(lda_corpus[col], minimum_probability=0.0)

        if use_hd:
            sim = 1.0 - hellinger(doc1, doc2)
        else:
            sim = cossim(doc1, doc2)

        score_matrix[row][col] = sim
        score_matrix[col][row] = sim

    with open(filename, "w") as matrixfile:
        for row in range(0, corpus_size):
            scores = [str(val) for val in score_matrix[row]]
            matrixfile.write("\t".join(scores) + "\n")


def calc_self_similarity(lda):
    mdiff, _ = lda.diff(lda, distance="jaccard", num_words=50, annotation=False)

    # Print the average of the off-diagonal elements of this matrix
    nums = []
    for index, x in numpy.ndenumerate(mdiff):
        if index[0] == index[1]:
            continue
        nums.append(1.0 - x)

    logging.info("SELFSIMILARITY: " + str(numpy.mean(nums)))


# This was a metric present in the Manda et al (2019) article but for which
# there wasn't much code. I've reconstructed it here.
def calc_match_percent(lda, lda_corpus, text_tokens, filename):
    all_percents = []
    file = None

    if filename:
        file = open(filename, "w")

    for num, doc in enumerate(lda_corpus):
        doc_tokens = text_tokens[num]
        keywords = set()
        doc.sort(key=lambda tup: tup[1], reverse=True)

        i = 0
        while i < 3 and i < len(doc):  # 3 top topics
            topicscore = lda.show_topic(doc[i][0], topn=2)
            for j in range(2):  # top 2 words for each topic
                keywords.add(topicscore[j][0])
            i += 1

        total = len(keywords)
        if total == 0:
            # This has occasionally happened, though it... shouldn't?
            all_percents.append(0)
            continue

        found = 0
        for word in keywords:
            if word in doc_tokens:
                found += 1

        all_percents.append(found / total)

        if file:
            file.write(" ".join(keywords) + "\n")

    if file:
        file.close()

    return sum(all_percents) / len(all_percents)


def main():
    parser = argparse.ArgumentParser(
        description="Compute document similarity with LDA topic modeling",
        formatter_class=argparse.ArgumentDefaultsHelpFormatter,
    )
    parser.add_argument(
        "-t",
        "--title-column",
        type=int,
        default=1,
        metavar="COL",
        help="column in TSV file (1-indexed) containing titles",
    )
    parser.add_argument(
        "-a",
        "--abstract-column",
        type=int,
        default=1,
        metavar="COL",
        help="column in TSV file (1-indexed) containing abstracts",
    )
    parser.add_argument(
        "-n",
        "--num-topics",
        type=int,
        default=25,
        metavar="NUM",
        help="number of topics to generate",
    )
    parser.add_argument(
        "-p",
        "--passes",
        type=int,
        default=10,
        metavar="NUM",
        help="number of topic modeling passes to perform; higher is better, though makes training slower",
    )
    parser.add_argument(
        "-l",
        "--log",
        type=str,
        default="Log.txt",
        metavar="FILE",
        help="output path to save debug log (including coherence and match percent metrics)",
    )
    parser.add_argument(
        "-hd",
        "--hellinger",
        action="store_true",
        help="use Hellinger distance instead of cosine similarity to measure document similarity",
    )
    parser.add_argument(
        "-os",
        "--output-similarity",
        type=str,
        default="DocumentSimilarity.txt",
        metavar="FILE",
        help="output path for document similarity matrix",
    )
    parser.add_argument(
        "-otk",
        "--output-topic-keywords",
        type=str,
        metavar="FILE",
        help="output file for topic keywords (the top 20 keywords for each topic, not produced by default)",
    )
    parser.add_argument(
        "-odk",
        "--output-document-keywords",
        type=str,
        metavar="FILE",
        help="output file for document keywords (the top 2 keywords from the top 3 topics for each document, not produced by default)",
    )
    parser.add_argument(
        "filenames",
        metavar="<file.tsv>",
        type=str,
        nargs="+",
        help="a file with titles and abstracts (multiple files will be concatenated)",
    )

    args = parser.parse_args()

    logging.basicConfig(
        format="%(asctime)s : %(levelname)s : %(message)s",
        level=logging.DEBUG,
        handlers=[
            logging.FileHandler(args.log, "w"),
            logging.StreamHandler(),
        ],
    )

    nltk.download("wordnet", quiet=True)
    nltk.download("stopwords", quiet=True)
    spacy.load("en_core_web_sm")

    stop_list = create_stop_list()
    text_tokens = prepare_text(
        args.filenames, args.title_column, args.abstract_column, stop_list
    )
    corpus_size = len(text_tokens)

    dictionary = corpora.Dictionary(text_tokens)
    bow_corpus = [dictionary.doc2bow(text) for text in text_tokens]

    lda = ldamodel.LdaModel(
        bow_corpus,
        id2word=dictionary,
        num_topics=args.num_topics,
        passes=args.passes,
        dtype=numpy.float64,
    )
    lda_corpus = lda[bow_corpus]

    # Write out the top words for the topics for inspection
    if args.output_topic_keywords:
        with open(args.output_topic_keywords, "w") as kwfile:
            topics = lda.show_topics(num_topics=-1, num_words=20, formatted=False)
            for num, topic in topics:
                word_list = [tup[0] for tup in topic]
                kwfile.write(" ".join(word_list) + "\n")

    # Calculate topic coherence
    coherence_model = coherencemodel.CoherenceModel(
        model=lda,
        corpus=bow_corpus,
        dictionary=dictionary,
        texts=text_tokens,
        coherence="c_v",
    )
    coherence_lda = coherence_model.get_coherence()
    logging.info("COHERENCE: " + str(coherence_lda))

    # Calculate self-similarity
    calc_self_similarity(lda)

    # Calculate similarity matrix
    write_similarity_matrix(lda, lda_corpus, args.hellinger, args.output_similarity)

    # Calculate match percent and write keywords if needed
    mp = calc_match_percent(lda, lda_corpus, text_tokens, args.output_document_keywords)
    logging.info("MATCH PERCENT: " + str(mp))


if __name__ == "__main__":
    main()
