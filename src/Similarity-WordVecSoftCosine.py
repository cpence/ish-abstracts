# This code determines document similarity using the Stanford glove word2vec
# model.
#
# To run it, create TSV files containing the documents you want to read, in
# order, with at least the titles and abstracts of the papers and sessions you
# want to classify. Multiple documents, if provided, will be concatenated in
# order to build the similarity matrix (for, e.g., individual paper + session
# support).
#
# > python WordVec-Similarity.py <title-column> <abstract-column> file.tsv
#   file.tsv ...
#
# The column numbers are one-indexed, as usual with spreadsheet columns.
#
# Output will be:
# - Log.txt -- the log file, which is probably only useful for debugging.
# - DocumentSimilarity.txt -- the similarity matrix for all documents.
import logging
import argparse
from nltk import corpus
from gensim import utils
from gensim.corpora import Dictionary
from gensim.similarities import (
    SoftCosineSimilarity,
    SparseTermSimilarityMatrix,
    WordEmbeddingSimilarityIndex,
)
import gensim.downloader as api


def create_stop_list():
    return set(corpus.stopwords.words("english"))


def prepare_text(filenames, title_column, abstract_column, stop_list):
    out = []

    for filename in filenames:
        with open(filename, "r") as file:
            for line in file:
                cols = line.split("\t")
                text = cols[title_column - 1] + " " + cols[abstract_column - 1]

                tokens = utils.simple_preprocess(text)
                out.append(tokens)

    return out


def write_similarity_matrix(text, filename):
    logging.info("loading gigaword model")
    model = api.load("glove-wiki-gigaword-300")

    # Calculate distances
    corpus_size = len(text)

    termsim_index = WordEmbeddingSimilarityIndex(model)
    dictionary = Dictionary(text)
    bow_corpus = [dictionary.doc2bow(document) for document in text]

    similarity_matrix = SparseTermSimilarityMatrix(termsim_index, dictionary)
    docsim_index = SoftCosineSimilarity(bow_corpus, similarity_matrix)

    score_matrix = [[0 for x in range(corpus_size)] for x in range(corpus_size)]
    for row, doc in enumerate(bow_corpus):
        sims = docsim_index[doc]
        for col, score in enumerate(sims):
            score_matrix[row][col] = score

    # Write
    with open(filename, "w") as matrixfile:
        for row in range(corpus_size):
            scores = [str(val) for val in score_matrix[row]]
            matrixfile.write("\t".join(scores) + "\n")


def main():
    parser = argparse.ArgumentParser(
        description="Compute document similarity with word2vec and soft cosine",
        formatter_class=argparse.ArgumentDefaultsHelpFormatter,
    )
    parser.add_argument(
        "-t",
        "--title-column",
        type=int,
        default=1,
        metavar="COL",
        help="column in TSV file (1-indexed) containing titles",
    )
    parser.add_argument(
        "-a",
        "--abstract-column",
        type=int,
        default=2,
        metavar="COL",
        help="column in TSV file (1-indexed) containing abstracts",
    )
    parser.add_argument(
        "-l",
        "--log",
        type=str,
        default="Log.txt",
        metavar="FILE",
        help="output path to save debug log",
    )
    parser.add_argument(
        "-o",
        "--out",
        type=str,
        default="DocumentSimilarity.txt",
        metavar="FILE",
        help="output path for document similarity matrix",
    )
    parser.add_argument(
        "filenames",
        metavar="<file.tsv>",
        type=str,
        nargs="+",
        help="a file with titles and abstracts (multiple files will be concatenated)",
    )

    args = parser.parse_args()

    logging.basicConfig(
        format="%(asctime)s : %(levelname)s : %(message)s",
        level=logging.DEBUG,
        filename=args.log,
        filemode="w",
    )

    stop_list = create_stop_list()
    text_tokens = prepare_text(
        args.filenames, args.title_column, args.abstract_column, stop_list
    )

    write_similarity_matrix(text_tokens, args.out)


if __name__ == "__main__":
    main()
