2021-05-15 14:28:12,960 : INFO : adding document #0 to Dictionary(0 unique tokens: [])
2021-05-15 14:28:12,962 : INFO : built Dictionary(960 unique tokens: ['agential', 'construction', 'niche', 'discovery', 'epistemic']...) from 233 documents (total 3130 corpus positions)
2021-05-15 14:28:12,970 : DEBUG : starting a new internal lifecycle event log for Dictionary
2021-05-15 14:28:12,971 : INFO : Dictionary lifecycle event {'msg': "built Dictionary(960 unique tokens: ['agential', 'construction', 'niche', 'discovery', 'epistemic']...) from 233 documents (total 3130 corpus positions)", 'datetime': '2021-05-15T14:28:12.962782', 'gensim': '4.0.1', 'python': '3.9.4 (default, Apr 20 2021, 15:51:38) \n[GCC 10.2.0]', 'platform': 'Linux-5.11.16-arch1-1-x86_64-with-glibc2.33', 'event': 'created'}
2021-05-15 14:28:12,972 : INFO : using autotuned alpha, starting with [0.02, 0.02, 0.02, 0.02, 0.02, 0.02, 0.02, 0.02, 0.02, 0.02, 0.02, 0.02, 0.02, 0.02, 0.02, 0.02, 0.02, 0.02, 0.02, 0.02, 0.02, 0.02, 0.02, 0.02, 0.02, 0.02, 0.02, 0.02, 0.02, 0.02, 0.02, 0.02, 0.02, 0.02, 0.02, 0.02, 0.02, 0.02, 0.02, 0.02, 0.02, 0.02, 0.02, 0.02, 0.02, 0.02, 0.02, 0.02, 0.02, 0.02]
2021-05-15 14:28:12,973 : INFO : using serial LDA version on this node
2021-05-15 14:28:12,977 : INFO : running online (multi-pass) LDA training, 50 topics, 10 passes over the supplied corpus of 233 documents, updating model once every 233 documents, evaluating perplexity every 233 documents, iterating 50x with a convergence threshold of 0.001000
2021-05-15 14:28:12,978 : DEBUG : bound: at document #0
2021-05-15 14:28:13,061 : INFO : -45.310 per-word bound, 43618380955478.8 perplexity estimate based on a held-out corpus of 233 documents with 3130 words
2021-05-15 14:28:13,062 : INFO : PROGRESS: pass 0, at document #233/233
2021-05-15 14:28:13,062 : DEBUG : performing inference on a chunk of 233 documents
2021-05-15 14:28:13,146 : DEBUG : 211/233 documents converged within 50 iterations
2021-05-15 14:28:13,147 : INFO : optimized alpha [0.01891163, 0.019275514, 0.019176057, 0.0190006, 0.019271912, 0.01909376, 0.019180497, 0.018914046, 0.01909496, 0.019362846, 0.019183895, 0.019276194, 0.019003592, 0.019546589, 0.019366886, 0.01963677, 0.019275712, 0.019458631, 0.01900344, 0.019367201, 0.019459091, 0.019097492, 0.01954715, 0.019187443, 0.019367987, 0.019825287, 0.019095588, 0.019278288, 0.019185511, 0.019093342, 0.019095436, 0.018912, 0.019094279, 0.019818015, 0.019088464, 0.018998148, 0.018912936, 0.019093346, 0.019093856, 0.019095542, 0.018914845, 0.019091422, 0.01891074, 0.019732852, 0.019177226, 0.018913772, 0.019273242, 0.019094165, 0.01953048, 0.018812617]
2021-05-15 14:28:13,148 : DEBUG : updating topics
2021-05-15 14:28:13,152 : INFO : topic #49 (0.019): 0.001*"genetic" + 0.001*"perspective" + 0.001*"circuit" + 0.001*"nuclear" + 0.001*"architecture" + 0.001*"landscape" + 0.001*"chromatin" + 0.001*"organization" + 0.001*"integrating" + 0.001*"cell"
2021-05-15 14:28:13,152 : INFO : topic #31 (0.019): 0.054*"via" + 0.054*"redundancy" + 0.054*"cooperation" + 0.054*"selection" + 0.054*"explain" + 0.054*"blind" + 0.054*"ignorance" + 0.054*"cultural" + 0.054*"evolution" + 0.001*"policy"
2021-05-15 14:28:13,153 : INFO : topic #43 (0.020): 0.035*"cultural" + 0.023*"synthesis" + 0.023*"theory" + 0.023*"biological" + 0.023*"animal" + 0.023*"mental" + 0.012*"paper" + 0.012*"pajamo" + 0.012*"philosophy" + 0.012*"clocks"
2021-05-15 14:28:13,153 : INFO : topic #25 (0.020): 0.038*"biology" + 0.028*"learn" + 0.020*"technology" + 0.019*"function" + 0.019*"transfer" + 0.019*"war" + 0.019*"natural" + 0.019*"case" + 0.019*"study" + 0.019*"evolution"
2021-05-15 14:28:13,153 : INFO : topic #33 (0.020): 0.042*"biological" + 0.028*"understanding" + 0.028*"repertoire" + 0.028*"biology" + 0.014*"comparative" + 0.014*"cheating" + 0.014*"responsibility" + 0.014*"trace" + 0.014*"evolutionary" + 0.014*"robot"
2021-05-15 14:28:13,153 : INFO : topic diff=45.805870, rho=1.000000
2021-05-15 14:28:13,157 : DEBUG : bound: at document #0
2021-05-15 14:28:13,196 : INFO : -6.379 per-word bound, 83.2 perplexity estimate based on a held-out corpus of 233 documents with 3130 words
2021-05-15 14:28:13,196 : INFO : PROGRESS: pass 1, at document #233/233
2021-05-15 14:28:13,196 : DEBUG : performing inference on a chunk of 233 documents
2021-05-15 14:28:13,221 : DEBUG : 233/233 documents converged within 50 iterations
2021-05-15 14:28:13,223 : INFO : optimized alpha [0.018290823, 0.018882168, 0.018688437, 0.01842372, 0.018826962, 0.018609919, 0.018740423, 0.018343136, 0.018612541, 0.018913629, 0.018744979, 0.018882558, 0.018426962, 0.019239726, 0.018918712, 0.019325713, 0.018881848, 0.019106764, 0.018475628, 0.019019509, 0.019158188, 0.018615726, 0.019241069, 0.01874963, 0.01902095, 0.019663678, 0.018612694, 0.018886084, 0.018746788, 0.018560234, 0.018613003, 0.018389015, 0.01861069, 0.019703908, 0.018604172, 0.01837187, 0.018341528, 0.018560003, 0.018561602, 0.018612534, 0.018344311, 0.018558353, 0.01833836, 0.01957332, 0.018587135, 0.01839086, 0.018829567, 0.018610522, 0.01912428, 0.018149488]
2021-05-15 14:28:13,223 : DEBUG : updating topics
2021-05-15 14:28:13,226 : INFO : topic #49 (0.018): 0.001*"genetic" + 0.001*"perspective" + 0.001*"circuit" + 0.001*"nuclear" + 0.001*"architecture" + 0.001*"landscape" + 0.001*"chromatin" + 0.001*"organization" + 0.001*"integrating" + 0.001*"cell"
2021-05-15 14:28:13,227 : INFO : topic #0 (0.018): 0.064*"langer" + 0.064*"biology" + 0.064*"susanne" + 0.064*"philosophy" + 0.064*"process" + 0.011*"guideline" + 0.007*"human" + 0.007*"baby" + 0.007*"embryo" + 0.006*"crispr"
2021-05-15 14:28:13,227 : INFO : topic #43 (0.020): 0.035*"cultural" + 0.023*"synthesis" + 0.023*"theory" + 0.023*"biological" + 0.023*"animal" + 0.023*"mental" + 0.012*"paper" + 0.012*"pajamo" + 0.012*"philosophy" + 0.012*"clocks"
2021-05-15 14:28:13,227 : INFO : topic #25 (0.020): 0.038*"biology" + 0.029*"learn" + 0.019*"technology" + 0.019*"function" + 0.019*"transfer" + 0.019*"war" + 0.019*"case" + 0.019*"study" + 0.019*"evolution" + 0.019*"process"
2021-05-15 14:28:13,227 : INFO : topic #33 (0.020): 0.041*"biological" + 0.028*"repertoire" + 0.028*"understanding" + 0.028*"biology" + 0.014*"comparative" + 0.014*"cheating" + 0.014*"responsibility" + 0.014*"evolutionary" + 0.014*"trace" + 0.014*"robot"
2021-05-15 14:28:13,227 : INFO : topic diff=0.051356, rho=0.577350
2021-05-15 14:28:13,231 : DEBUG : bound: at document #0
2021-05-15 14:28:13,268 : INFO : -6.303 per-word bound, 79.0 perplexity estimate based on a held-out corpus of 233 documents with 3130 words
2021-05-15 14:28:13,268 : INFO : PROGRESS: pass 2, at document #233/233
2021-05-15 14:28:13,268 : DEBUG : performing inference on a chunk of 233 documents
2021-05-15 14:28:13,290 : DEBUG : 233/233 documents converged within 50 iterations
2021-05-15 14:28:13,292 : INFO : optimized alpha [0.017782878, 0.018553514, 0.018284822, 0.01794999, 0.018457182, 0.01820938, 0.018374562, 0.017874133, 0.018213084, 0.018540379, 0.018380037, 0.018553678, 0.017953396, 0.01898045, 0.018546272, 0.019063003, 0.01855277, 0.018811263, 0.018040419, 0.018727412, 0.018903758, 0.018216778, 0.018982414, 0.018385533, 0.018729368, 0.019522216, 0.018212877, 0.018558308, 0.018381985, 0.018120803, 0.018213524, 0.017957676, 0.018210333, 0.019601827, 0.0182033, 0.01785959, 0.017872142, 0.01812039, 0.018122809, 0.018212628, 0.01787557, 0.018118942, 0.01786823, 0.019433893, 0.01810298, 0.017959772, 0.018460771, 0.018210122, 0.018785197, 0.017608745]
2021-05-15 14:28:13,292 : DEBUG : updating topics
2021-05-15 14:28:13,296 : INFO : topic #49 (0.018): 0.001*"genetic" + 0.001*"perspective" + 0.001*"circuit" + 0.001*"nuclear" + 0.001*"architecture" + 0.001*"landscape" + 0.001*"chromatin" + 0.001*"organization" + 0.001*"integrating" + 0.001*"cell"
2021-05-15 14:28:13,296 : INFO : topic #0 (0.018): 0.067*"langer" + 0.067*"biology" + 0.067*"susanne" + 0.067*"philosophy" + 0.067*"process" + 0.006*"guideline" + 0.004*"human" + 0.004*"baby" + 0.004*"embryo" + 0.004*"crispr"
2021-05-15 14:28:13,296 : INFO : topic #43 (0.019): 0.035*"cultural" + 0.023*"synthesis" + 0.023*"theory" + 0.023*"biological" + 0.023*"animal" + 0.023*"mental" + 0.012*"paper" + 0.012*"pajamo" + 0.012*"philosophy" + 0.012*"clocks"
2021-05-15 14:28:13,296 : INFO : topic #25 (0.020): 0.038*"biology" + 0.029*"learn" + 0.019*"technology" + 0.019*"function" + 0.019*"transfer" + 0.019*"war" + 0.019*"case" + 0.019*"study" + 0.019*"evolution" + 0.019*"process"
2021-05-15 14:28:13,296 : INFO : topic #33 (0.020): 0.041*"biological" + 0.027*"repertoire" + 0.027*"understanding" + 0.027*"biology" + 0.014*"comparative" + 0.014*"responsibility" + 0.014*"cheating" + 0.014*"evolutionary" + 0.014*"robot" + 0.014*"trace"
2021-05-15 14:28:13,296 : INFO : topic diff=0.034698, rho=0.500000
2021-05-15 14:28:13,301 : DEBUG : bound: at document #0
2021-05-15 14:28:13,338 : INFO : -6.261 per-word bound, 76.7 perplexity estimate based on a held-out corpus of 233 documents with 3130 words
2021-05-15 14:28:13,339 : INFO : PROGRESS: pass 3, at document #233/233
2021-05-15 14:28:13,339 : DEBUG : performing inference on a chunk of 233 documents
2021-05-15 14:28:13,361 : DEBUG : 233/233 documents converged within 50 iterations
2021-05-15 14:28:13,363 : INFO : optimized alpha [0.01734979, 0.018268429, 0.017937418, 0.017544866, 0.01813784, 0.017864576, 0.018058522, 0.01747293, 0.017869156, 0.018218111, 0.018064741, 0.018268406, 0.017548406, 0.018753456, 0.018224651, 0.018833086, 0.018267345, 0.018553855, 0.017667007, 0.018472895, 0.018680857, 0.017873256, 0.018755935, 0.018070925, 0.018475277, 0.019394886, 0.017868657, 0.01827394, 0.018066807, 0.01774385, 0.017869584, 0.017587509, 0.017865673, 0.019508244, 0.017858228, 0.01742291, 0.017470649, 0.017743295, 0.017746383, 0.017868336, 0.017474594, 0.017742015, 0.017466132, 0.01930842, 0.017689267, 0.017589828, 0.018142225, 0.017865447, 0.018491289, 0.017148975]
2021-05-15 14:28:13,363 : DEBUG : updating topics
2021-05-15 14:28:13,367 : INFO : topic #49 (0.017): 0.001*"genetic" + 0.001*"perspective" + 0.001*"circuit" + 0.001*"nuclear" + 0.001*"architecture" + 0.001*"landscape" + 0.001*"chromatin" + 0.001*"organization" + 0.001*"integrating" + 0.001*"cell"
2021-05-15 14:28:13,368 : INFO : topic #0 (0.017): 0.068*"langer" + 0.068*"biology" + 0.068*"susanne" + 0.068*"philosophy" + 0.068*"process" + 0.004*"guideline" + 0.003*"human" + 0.003*"baby" + 0.003*"embryo" + 0.002*"crispr"
2021-05-15 14:28:13,369 : INFO : topic #43 (0.019): 0.035*"cultural" + 0.023*"synthesis" + 0.023*"theory" + 0.023*"biological" + 0.023*"animal" + 0.023*"mental" + 0.012*"pajamo" + 0.012*"paper" + 0.012*"philosophy" + 0.012*"clocks"
2021-05-15 14:28:13,369 : INFO : topic #25 (0.019): 0.039*"biology" + 0.029*"learn" + 0.019*"technology" + 0.019*"function" + 0.019*"transfer" + 0.019*"war" + 0.019*"case" + 0.019*"study" + 0.019*"evolution" + 0.019*"process"
2021-05-15 14:28:13,369 : INFO : topic #33 (0.020): 0.041*"biological" + 0.027*"repertoire" + 0.027*"understanding" + 0.027*"biology" + 0.014*"comparative" + 0.014*"responsibility" + 0.014*"cheating" + 0.014*"robot" + 0.014*"evolutionary" + 0.014*"trace"
2021-05-15 14:28:13,369 : INFO : topic diff=0.030769, rho=0.447214
2021-05-15 14:28:13,374 : DEBUG : bound: at document #0
2021-05-15 14:28:13,414 : INFO : -6.233 per-word bound, 75.2 perplexity estimate based on a held-out corpus of 233 documents with 3130 words
2021-05-15 14:28:13,414 : INFO : PROGRESS: pass 4, at document #233/233
2021-05-15 14:28:13,414 : DEBUG : performing inference on a chunk of 233 documents
2021-05-15 14:28:13,436 : DEBUG : 233/233 documents converged within 50 iterations
2021-05-15 14:28:13,438 : INFO : optimized alpha [0.01697065, 0.018015146, 0.01763083, 0.017189316, 0.017855207, 0.01756024, 0.017778747, 0.01712072, 0.017565554, 0.017932933, 0.017785579, 0.018014971, 0.01719297, 0.018550174, 0.017940003, 0.01862724, 0.018013768, 0.01832432, 0.017338349, 0.018245889, 0.01848114, 0.017569985, 0.01855309, 0.017792342, 0.01824864, 0.019278158, 0.0175648, 0.018021269, 0.017787741, 0.017412148, 0.017565966, 0.017261643, 0.017561458, 0.01942111, 0.017553674, 0.017040702, 0.0171182, 0.017411474, 0.017415106, 0.017564422, 0.01712256, 0.017410334, 0.017113201, 0.01919338, 0.017326396, 0.017264156, 0.017860262, 0.01756121, 0.018230349, 0.016747432]
2021-05-15 14:28:13,438 : DEBUG : updating topics
2021-05-15 14:28:13,442 : INFO : topic #49 (0.017): 0.001*"genetic" + 0.001*"perspective" + 0.001*"circuit" + 0.001*"nuclear" + 0.001*"architecture" + 0.001*"landscape" + 0.001*"chromatin" + 0.001*"organization" + 0.001*"integrating" + 0.001*"cell"
2021-05-15 14:28:13,442 : INFO : topic #0 (0.017): 0.068*"langer" + 0.068*"biology" + 0.068*"susanne" + 0.068*"philosophy" + 0.068*"process" + 0.003*"guideline" + 0.002*"human" + 0.002*"baby" + 0.002*"embryo" + 0.002*"crispr"
2021-05-15 14:28:13,442 : INFO : topic #43 (0.019): 0.035*"cultural" + 0.023*"synthesis" + 0.023*"theory" + 0.023*"biological" + 0.023*"animal" + 0.023*"mental" + 0.012*"philosophy" + 0.012*"pajamo" + 0.012*"paper" + 0.012*"clocks"
2021-05-15 14:28:13,442 : INFO : topic #25 (0.019): 0.039*"biology" + 0.029*"learn" + 0.019*"technology" + 0.019*"function" + 0.019*"transfer" + 0.019*"war" + 0.019*"case" + 0.019*"study" + 0.019*"evolution" + 0.019*"process"
2021-05-15 14:28:13,443 : INFO : topic #33 (0.019): 0.041*"biological" + 0.027*"repertoire" + 0.027*"understanding" + 0.027*"biology" + 0.014*"comparative" + 0.014*"responsibility" + 0.014*"cheating" + 0.014*"evolutionary" + 0.014*"robot" + 0.014*"forensic"
2021-05-15 14:28:13,443 : INFO : topic diff=0.029058, rho=0.408248
2021-05-15 14:28:13,447 : DEBUG : bound: at document #0
2021-05-15 14:28:13,483 : INFO : -6.214 per-word bound, 74.2 perplexity estimate based on a held-out corpus of 233 documents with 3130 words
2021-05-15 14:28:13,483 : INFO : PROGRESS: pass 5, at document #233/233
2021-05-15 14:28:13,483 : DEBUG : performing inference on a chunk of 233 documents
2021-05-15 14:28:13,506 : DEBUG : 233/233 documents converged within 50 iterations
2021-05-15 14:28:13,507 : INFO : optimized alpha [0.016632553, 0.017786326, 0.017355483, 0.016871545, 0.017600738, 0.017286886, 0.01752679, 0.016805872, 0.017292831, 0.017676208, 0.017534155, 0.017786022, 0.016875291, 0.018365247, 0.01768373, 0.01844002, 0.017784702, 0.0181163, 0.017043872, 0.018040117, 0.01829935, 0.017297534, 0.018368533, 0.017541401, 0.01804318, 0.019169778, 0.01729186, 0.017792972, 0.017536392, 0.017115016, 0.017293224, 0.016969644, 0.017288206, 0.019339124, 0.01728014, 0.016699929, 0.016803145, 0.017114237, 0.017118335, 0.017291432, 0.016807863, 0.01711322, 0.016797736, 0.01908653, 0.017002262, 0.016972318, 0.017606352, 0.017287936, 0.017994748, 0.016390104]
2021-05-15 14:28:13,507 : DEBUG : updating topics
2021-05-15 14:28:13,511 : INFO : topic #49 (0.016): 0.001*"genetic" + 0.001*"perspective" + 0.001*"circuit" + 0.001*"nuclear" + 0.001*"architecture" + 0.001*"landscape" + 0.001*"chromatin" + 0.001*"organization" + 0.001*"integrating" + 0.001*"cell"
2021-05-15 14:28:13,511 : INFO : topic #0 (0.017): 0.069*"langer" + 0.069*"biology" + 0.069*"susanne" + 0.069*"philosophy" + 0.069*"process" + 0.002*"guideline" + 0.001*"human" + 0.001*"baby" + 0.001*"embryo" + 0.001*"crispr"
2021-05-15 14:28:13,511 : INFO : topic #43 (0.019): 0.035*"cultural" + 0.023*"synthesis" + 0.023*"theory" + 0.023*"biological" + 0.023*"animal" + 0.023*"mental" + 0.012*"philosophy" + 0.012*"pajamo" + 0.012*"clocks" + 0.012*"paper"
2021-05-15 14:28:13,511 : INFO : topic #25 (0.019): 0.039*"biology" + 0.029*"learn" + 0.019*"technology" + 0.019*"function" + 0.019*"war" + 0.019*"transfer" + 0.019*"case" + 0.019*"evolution" + 0.019*"study" + 0.019*"process"
2021-05-15 14:28:13,512 : INFO : topic #33 (0.019): 0.041*"biological" + 0.027*"repertoire" + 0.027*"understanding" + 0.027*"biology" + 0.014*"comparative" + 0.014*"responsibility" + 0.014*"robot" + 0.014*"evolutionary" + 0.014*"trace" + 0.014*"cheating"
2021-05-15 14:28:13,512 : INFO : topic diff=0.026890, rho=0.377964
2021-05-15 14:28:13,516 : DEBUG : bound: at document #0
2021-05-15 14:28:13,553 : INFO : -6.202 per-word bound, 73.6 perplexity estimate based on a held-out corpus of 233 documents with 3130 words
2021-05-15 14:28:13,553 : INFO : PROGRESS: pass 6, at document #233/233
2021-05-15 14:28:13,553 : DEBUG : performing inference on a chunk of 233 documents
2021-05-15 14:28:13,575 : DEBUG : 233/233 documents converged within 50 iterations
2021-05-15 14:28:13,577 : INFO : optimized alpha [0.016326927, 0.01757705, 0.017104976, 0.016583705, 0.0173687, 0.017038168, 0.017297005, 0.016520625, 0.017044658, 0.017442137, 0.01730484, 0.017576627, 0.016587537, 0.018195044, 0.017450059, 0.018267753, 0.017575212, 0.017925486, 0.01677653, 0.017851347, 0.018131962, 0.017049614, 0.018198662, 0.017312502, 0.017854676, 0.019068198, 0.017043497, 0.017584158, 0.01730713, 0.016845306, 0.017045045, 0.016704503, 0.017039593, 0.019261383, 0.01703128, 0.016391912, 0.016517717, 0.016844435, 0.01684894, 0.01704304, 0.016522733, 0.016843535, 0.016511958, 0.018986363, 0.0167088, 0.016707335, 0.017374806, 0.0170393, 0.017779374, 0.016067676]
2021-05-15 14:28:13,577 : DEBUG : updating topics
2021-05-15 14:28:13,580 : INFO : topic #49 (0.016): 0.001*"genetic" + 0.001*"perspective" + 0.001*"circuit" + 0.001*"nuclear" + 0.001*"architecture" + 0.001*"landscape" + 0.001*"chromatin" + 0.001*"organization" + 0.001*"integrating" + 0.001*"cell"
2021-05-15 14:28:13,581 : INFO : topic #0 (0.016): 0.069*"langer" + 0.069*"biology" + 0.069*"susanne" + 0.069*"philosophy" + 0.069*"process" + 0.001*"guideline" + 0.001*"human" + 0.001*"baby" + 0.001*"embryo" + 0.001*"crispr"
2021-05-15 14:28:13,581 : INFO : topic #43 (0.019): 0.035*"cultural" + 0.023*"synthesis" + 0.023*"theory" + 0.023*"biological" + 0.023*"animal" + 0.023*"mental" + 0.012*"philosophy" + 0.012*"clocks" + 0.012*"life" + 0.012*"phenomenon"
2021-05-15 14:28:13,581 : INFO : topic #25 (0.019): 0.039*"biology" + 0.029*"learn" + 0.019*"technology" + 0.019*"function" + 0.019*"war" + 0.019*"transfer" + 0.019*"case" + 0.019*"evolution" + 0.019*"study" + 0.019*"process"
2021-05-15 14:28:13,581 : INFO : topic #33 (0.019): 0.041*"biological" + 0.027*"repertoire" + 0.027*"understanding" + 0.027*"biology" + 0.014*"responsibility" + 0.014*"comparative" + 0.014*"robot" + 0.014*"evolutionary" + 0.014*"forensic" + 0.014*"trace"
2021-05-15 14:28:13,581 : INFO : topic diff=0.023824, rho=0.353553
2021-05-15 14:28:13,586 : DEBUG : bound: at document #0
2021-05-15 14:28:13,623 : INFO : -6.193 per-word bound, 73.2 perplexity estimate based on a held-out corpus of 233 documents with 3130 words
2021-05-15 14:28:13,623 : INFO : PROGRESS: pass 7, at document #233/233
2021-05-15 14:28:13,624 : DEBUG : performing inference on a chunk of 233 documents
2021-05-15 14:28:13,646 : DEBUG : 233/233 documents converged within 50 iterations
2021-05-15 14:28:13,647 : INFO : optimized alpha [0.016047696, 0.017383829, 0.016874785, 0.016320264, 0.017155042, 0.016809609, 0.017085394, 0.0162595, 0.016816571, 0.01722664, 0.017093625, 0.017383302, 0.016324155, 0.018036993, 0.017234912, 0.018107817, 0.017381793, 0.017748851, 0.01653135, 0.017676564, 0.01797647, 0.016821738, 0.018040907, 0.017101664, 0.017680129, 0.018972317, 0.016815249, 0.017391337, 0.017095983, 0.016597984, 0.016816951, 0.016461316, 0.01681112, 0.01918723, 0.01680259, 0.016110554, 0.01625644, 0.016597044, 0.01660189, 0.016814755, 0.016261715, 0.016596235, 0.016250376, 0.018891793, 0.016440324, 0.016464273, 0.017161587, 0.016810805, 0.017580617, 0.01577359]
2021-05-15 14:28:13,648 : DEBUG : updating topics
2021-05-15 14:28:13,652 : INFO : topic #49 (0.016): 0.001*"genetic" + 0.001*"perspective" + 0.001*"circuit" + 0.001*"nuclear" + 0.001*"architecture" + 0.001*"landscape" + 0.001*"chromatin" + 0.001*"organization" + 0.001*"integrating" + 0.001*"cell"
2021-05-15 14:28:13,652 : INFO : topic #0 (0.016): 0.069*"langer" + 0.069*"biology" + 0.069*"susanne" + 0.069*"philosophy" + 0.069*"process" + 0.001*"guideline" + 0.001*"human" + 0.001*"baby" + 0.001*"embryo" + 0.001*"crispr"
2021-05-15 14:28:13,652 : INFO : topic #43 (0.019): 0.035*"cultural" + 0.023*"synthesis" + 0.023*"theory" + 0.023*"biological" + 0.023*"animal" + 0.023*"mental" + 0.012*"philosophy" + 0.012*"clocks" + 0.012*"phenomenon" + 0.012*"pajamo"
2021-05-15 14:28:13,653 : INFO : topic #25 (0.019): 0.039*"biology" + 0.029*"learn" + 0.019*"technology" + 0.019*"function" + 0.019*"war" + 0.019*"transfer" + 0.019*"case" + 0.019*"process" + 0.019*"evolution" + 0.019*"study"
2021-05-15 14:28:13,653 : INFO : topic #33 (0.019): 0.041*"biological" + 0.027*"repertoire" + 0.027*"understanding" + 0.027*"biology" + 0.014*"responsibility" + 0.014*"comparative" + 0.014*"robot" + 0.014*"evolutionary" + 0.014*"trace" + 0.014*"forensic"
2021-05-15 14:28:13,653 : INFO : topic diff=0.020204, rho=0.333333
2021-05-15 14:28:13,657 : DEBUG : bound: at document #0
2021-05-15 14:28:13,693 : INFO : -6.188 per-word bound, 72.9 perplexity estimate based on a held-out corpus of 233 documents with 3130 words
2021-05-15 14:28:13,694 : INFO : PROGRESS: pass 8, at document #233/233
2021-05-15 14:28:13,694 : DEBUG : performing inference on a chunk of 233 documents
2021-05-15 14:28:13,715 : DEBUG : 233/233 documents converged within 50 iterations
2021-05-15 14:28:13,717 : INFO : optimized alpha [0.015790433, 0.017204069, 0.016661583, 0.016077144, 0.016956773, 0.016597899, 0.016888998, 0.01601848, 0.016605277, 0.017026687, 0.016897585, 0.017203446, 0.016081085, 0.017889176, 0.01703527, 0.017958269, 0.017201865, 0.017584126, 0.01630466, 0.017513536, 0.017830994, 0.016610634, 0.017893355, 0.01690595, 0.017517319, 0.018881295, 0.01660381, 0.01721193, 0.016899996, 0.016369352, 0.016605647, 0.016236447, 0.016599476, 0.01911618, 0.016590767, 0.015851358, 0.016015284, 0.016368343, 0.016373495, 0.016603284, 0.016020788, 0.016367616, 0.016008956, 0.018801993, 0.01619266, 0.016239509, 0.016963711, 0.016599147, 0.01739579, 0.015503056]
2021-05-15 14:28:13,717 : DEBUG : updating topics
2021-05-15 14:28:13,721 : INFO : topic #49 (0.016): 0.001*"genetic" + 0.001*"perspective" + 0.001*"circuit" + 0.001*"nuclear" + 0.001*"architecture" + 0.001*"landscape" + 0.001*"chromatin" + 0.001*"organization" + 0.001*"integrating" + 0.001*"cell"
2021-05-15 14:28:13,721 : INFO : topic #0 (0.016): 0.069*"langer" + 0.069*"biology" + 0.069*"philosophy" + 0.069*"susanne" + 0.069*"process" + 0.001*"guideline" + 0.001*"human" + 0.001*"baby" + 0.001*"embryo" + 0.001*"crispr"
2021-05-15 14:28:13,721 : INFO : topic #43 (0.019): 0.035*"cultural" + 0.023*"synthesis" + 0.023*"biological" + 0.023*"theory" + 0.023*"animal" + 0.023*"mental" + 0.012*"philosophy" + 0.012*"phenomenon" + 0.012*"clocks" + 0.012*"life"
2021-05-15 14:28:13,721 : INFO : topic #25 (0.019): 0.039*"biology" + 0.029*"learn" + 0.019*"technology" + 0.019*"function" + 0.019*"war" + 0.019*"transfer" + 0.019*"case" + 0.019*"process" + 0.019*"evolution" + 0.019*"study"
2021-05-15 14:28:13,721 : INFO : topic #33 (0.019): 0.041*"biological" + 0.027*"repertoire" + 0.027*"understanding" + 0.027*"biology" + 0.014*"responsibility" + 0.014*"evolutionary" + 0.014*"robot" + 0.014*"comparative" + 0.014*"trace" + 0.014*"forensic"
2021-05-15 14:28:13,722 : INFO : topic diff=0.016503, rho=0.316228
2021-05-15 14:28:13,726 : DEBUG : bound: at document #0
2021-05-15 14:28:13,763 : INFO : -6.184 per-word bound, 72.7 perplexity estimate based on a held-out corpus of 233 documents with 3130 words
2021-05-15 14:28:13,763 : INFO : PROGRESS: pass 9, at document #233/233
2021-05-15 14:28:13,763 : DEBUG : performing inference on a chunk of 233 documents
2021-05-15 14:28:13,785 : DEBUG : 233/233 documents converged within 50 iterations
2021-05-15 14:28:13,786 : INFO : optimized alpha [0.01555177, 0.017035808, 0.016462829, 0.015851261, 0.016771616, 0.016400516, 0.016705561, 0.015794512, 0.016408274, 0.016839974, 0.016714474, 0.017035095, 0.015855247, 0.01775013, 0.01684884, 0.01781763, 0.01703344, 0.01742959, 0.01609368, 0.017360572, 0.017694116, 0.01641379, 0.01775456, 0.016723132, 0.017364556, 0.018794492, 0.016406674, 0.017043987, 0.01671692, 0.01615659, 0.016408633, 0.016027134, 0.01640216, 0.019047847, 0.01639329, 0.015610927, 0.0157912, 0.016155513, 0.016160937, 0.016406115, 0.015796902, 0.016154869, 0.015784632, 0.018716335, 0.01596263, 0.016030295, 0.016778901, 0.016401818, 0.017222852, 0.01525244]
2021-05-15 14:28:13,787 : DEBUG : updating topics
2021-05-15 14:28:13,790 : INFO : topic #49 (0.015): 0.001*"genetic" + 0.001*"perspective" + 0.001*"circuit" + 0.001*"nuclear" + 0.001*"architecture" + 0.001*"landscape" + 0.001*"chromatin" + 0.001*"organization" + 0.001*"integrating" + 0.001*"cell"
2021-05-15 14:28:13,790 : INFO : topic #0 (0.016): 0.069*"langer" + 0.069*"biology" + 0.069*"susanne" + 0.069*"philosophy" + 0.069*"process" + 0.001*"guideline" + 0.001*"human" + 0.001*"baby" + 0.001*"embryo" + 0.001*"crispr"
2021-05-15 14:28:13,790 : INFO : topic #43 (0.019): 0.035*"cultural" + 0.023*"synthesis" + 0.023*"biological" + 0.023*"theory" + 0.023*"animal" + 0.023*"mental" + 0.012*"philosophy" + 0.012*"phenomenon" + 0.012*"life" + 0.012*"clocks"
2021-05-15 14:28:13,791 : INFO : topic #25 (0.019): 0.039*"biology" + 0.029*"learn" + 0.019*"technology" + 0.019*"function" + 0.019*"war" + 0.019*"transfer" + 0.019*"process" + 0.019*"case" + 0.019*"evolution" + 0.019*"study"
2021-05-15 14:28:13,791 : INFO : topic #33 (0.019): 0.041*"biological" + 0.027*"repertoire" + 0.027*"biology" + 0.027*"understanding" + 0.014*"responsibility" + 0.014*"evolutionary" + 0.014*"robot" + 0.014*"comparative" + 0.014*"reproduction" + 0.014*"humans"
2021-05-15 14:28:13,791 : INFO : topic diff=0.013087, rho=0.301511
2021-05-15 14:28:13,793 : DEBUG : starting a new internal lifecycle event log for LdaModel
2021-05-15 14:28:13,793 : INFO : LdaModel lifecycle event {'msg': 'trained LdaModel(num_terms=960, num_topics=50, decay=0.5, chunksize=2000) in 0.82s', 'datetime': '2021-05-15T14:28:13.793554', 'gensim': '4.0.1', 'python': '3.9.4 (default, Apr 20 2021, 15:51:38) \n[GCC 10.2.0]', 'platform': 'Linux-5.11.16-arch1-1-x86_64-with-glibc2.33', 'event': 'created'}
2021-05-15 14:28:13,799 : DEBUG : Setting topics to those of the model: LdaModel(num_terms=960, num_topics=50, decay=0.5, chunksize=2000)
2021-05-15 14:28:13,833 : INFO : COHERENCE: -16.340447739941194
2021-05-15 14:28:31,886 : INFO : MATCH PERCENT: 0.296137339055794
