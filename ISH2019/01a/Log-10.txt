2021-05-15 14:27:18,803 : INFO : adding document #0 to Dictionary(0 unique tokens: [])
2021-05-15 14:27:18,806 : INFO : built Dictionary(960 unique tokens: ['agential', 'construction', 'niche', 'discovery', 'epistemic']...) from 233 documents (total 3130 corpus positions)
2021-05-15 14:27:18,814 : DEBUG : starting a new internal lifecycle event log for Dictionary
2021-05-15 14:27:18,814 : INFO : Dictionary lifecycle event {'msg': "built Dictionary(960 unique tokens: ['agential', 'construction', 'niche', 'discovery', 'epistemic']...) from 233 documents (total 3130 corpus positions)", 'datetime': '2021-05-15T14:27:18.806646', 'gensim': '4.0.1', 'python': '3.9.4 (default, Apr 20 2021, 15:51:38) \n[GCC 10.2.0]', 'platform': 'Linux-5.11.16-arch1-1-x86_64-with-glibc2.33', 'event': 'created'}
2021-05-15 14:27:18,816 : INFO : using autotuned alpha, starting with [0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1]
2021-05-15 14:27:18,816 : INFO : using serial LDA version on this node
2021-05-15 14:27:18,817 : INFO : running online (multi-pass) LDA training, 10 topics, 10 passes over the supplied corpus of 233 documents, updating model once every 233 documents, evaluating perplexity every 233 documents, iterating 50x with a convergence threshold of 0.001000
2021-05-15 14:27:18,818 : DEBUG : bound: at document #0
2021-05-15 14:27:18,902 : INFO : -11.084 per-word bound, 2170.1 perplexity estimate based on a held-out corpus of 233 documents with 3130 words
2021-05-15 14:27:18,902 : INFO : PROGRESS: pass 0, at document #233/233
2021-05-15 14:27:18,902 : DEBUG : performing inference on a chunk of 233 documents
2021-05-15 14:27:18,988 : DEBUG : 202/233 documents converged within 50 iterations
2021-05-15 14:27:18,989 : INFO : optimized alpha [0.0760987, 0.076907545, 0.08226621, 0.07712988, 0.08025925, 0.08120996, 0.08654494, 0.08128109, 0.08148377, 0.08121106]
2021-05-15 14:27:18,989 : DEBUG : updating topics
2021-05-15 14:27:18,991 : INFO : topic #0 (0.076): 0.026*"model" + 0.021*"biology" + 0.013*"ethical" + 0.013*"prediction" + 0.013*"explanation" + 0.013*"experiment" + 0.013*"owen" + 0.013*"building" + 0.010*"science" + 0.007*"analysis"
2021-05-15 14:27:18,991 : INFO : topic #1 (0.077): 0.018*"cultural" + 0.018*"theory" + 0.017*"experiment" + 0.012*"development" + 0.012*"exploration" + 0.012*"disorder" + 0.012*"animal" + 0.012*"genetic" + 0.012*"life" + 0.012*"system"
2021-05-15 14:27:18,991 : INFO : topic #8 (0.081): 0.019*"biological" + 0.015*"process" + 0.015*"ontology" + 0.011*"early" + 0.011*"scientific" + 0.010*"organizational" + 0.010*"property" + 0.010*"change" + 0.010*"metaphysics" + 0.010*"non"
2021-05-15 14:27:18,991 : INFO : topic #2 (0.082): 0.019*"animal" + 0.015*"biology" + 0.014*"natural" + 0.011*"study" + 0.010*"evolutionary" + 0.010*"evolution" + 0.010*"contemporary" + 0.010*"relation" + 0.010*"selective" + 0.010*"function"
2021-05-15 14:27:18,991 : INFO : topic #6 (0.087): 0.025*"biology" + 0.015*"theory" + 0.015*"model" + 0.011*"biological" + 0.011*"evidence" + 0.011*"human" + 0.010*"account" + 0.009*"cell" + 0.009*"life" + 0.008*"natural"
2021-05-15 14:27:18,991 : INFO : topic diff=7.801816, rho=1.000000
2021-05-15 14:27:18,993 : DEBUG : bound: at document #0
2021-05-15 14:27:19,026 : INFO : -6.623 per-word bound, 98.6 perplexity estimate based on a held-out corpus of 233 documents with 3130 words
2021-05-15 14:27:19,027 : INFO : PROGRESS: pass 1, at document #233/233
2021-05-15 14:27:19,027 : DEBUG : performing inference on a chunk of 233 documents
2021-05-15 14:27:19,049 : DEBUG : 233/233 documents converged within 50 iterations
2021-05-15 14:27:19,050 : INFO : optimized alpha [0.067342825, 0.067987144, 0.0736044, 0.06792854, 0.0716284, 0.07370424, 0.08000497, 0.07333402, 0.072995245, 0.07298717]
2021-05-15 14:27:19,050 : DEBUG : updating topics
2021-05-15 14:27:19,051 : INFO : topic #0 (0.067): 0.032*"model" + 0.023*"biology" + 0.013*"ethical" + 0.013*"prediction" + 0.013*"explanation" + 0.013*"experiment" + 0.013*"owen" + 0.013*"building" + 0.008*"science" + 0.007*"analysis"
2021-05-15 14:27:19,051 : INFO : topic #3 (0.068): 0.012*"contribution" + 0.012*"causality" + 0.012*"editing" + 0.012*"genome" + 0.012*"evolutionary" + 0.012*"research" + 0.011*"biosocial" + 0.011*"today" + 0.006*"synthesis" + 0.006*"art"
2021-05-15 14:27:19,051 : INFO : topic #2 (0.074): 0.018*"animal" + 0.014*"biology" + 0.014*"natural" + 0.013*"study" + 0.010*"evolutionary" + 0.010*"evolution" + 0.009*"contemporary" + 0.009*"selective" + 0.009*"function" + 0.009*"relation"
2021-05-15 14:27:19,051 : INFO : topic #5 (0.074): 0.033*"evolution" + 0.024*"biological" + 0.023*"biology" + 0.014*"evolutionary" + 0.014*"moral" + 0.014*"philosophy" + 0.014*"explanation" + 0.010*"prospect" + 0.010*"problem" + 0.010*"causal"
2021-05-15 14:27:19,051 : INFO : topic #6 (0.080): 0.025*"biology" + 0.015*"theory" + 0.015*"model" + 0.011*"biological" + 0.011*"evidence" + 0.011*"human" + 0.010*"account" + 0.010*"life" + 0.010*"cell" + 0.008*"natural"
2021-05-15 14:27:19,051 : INFO : topic diff=0.069161, rho=0.577350
2021-05-15 14:27:19,053 : DEBUG : bound: at document #0
2021-05-15 14:27:19,087 : INFO : -6.554 per-word bound, 93.9 perplexity estimate based on a held-out corpus of 233 documents with 3130 words
2021-05-15 14:27:19,087 : INFO : PROGRESS: pass 2, at document #233/233
2021-05-15 14:27:19,087 : DEBUG : performing inference on a chunk of 233 documents
2021-05-15 14:27:19,107 : DEBUG : 233/233 documents converged within 50 iterations
2021-05-15 14:27:19,108 : INFO : optimized alpha [0.061254255, 0.06179357, 0.0674823, 0.06158207, 0.06554726, 0.06824206, 0.07502287, 0.06761525, 0.06697781, 0.06712502]
2021-05-15 14:27:19,108 : DEBUG : updating topics
2021-05-15 14:27:19,109 : INFO : topic #0 (0.061): 0.035*"model" + 0.024*"biology" + 0.013*"ethical" + 0.013*"prediction" + 0.013*"explanation" + 0.013*"experiment" + 0.013*"owen" + 0.013*"building" + 0.007*"science" + 0.007*"analysis"
2021-05-15 14:27:19,109 : INFO : topic #3 (0.062): 0.012*"contribution" + 0.012*"causality" + 0.012*"editing" + 0.012*"genome" + 0.012*"evolutionary" + 0.012*"research" + 0.012*"biosocial" + 0.012*"today" + 0.006*"synthesis" + 0.006*"art"
2021-05-15 14:27:19,110 : INFO : topic #7 (0.068): 0.018*"war" + 0.014*"cold" + 0.014*"case" + 0.009*"critical" + 0.009*"genetics" + 0.009*"transplantation" + 0.009*"nature" + 0.009*"historicizing" + 0.009*"plant" + 0.009*"science"
2021-05-15 14:27:19,110 : INFO : topic #5 (0.068): 0.032*"evolution" + 0.023*"biological" + 0.023*"biology" + 0.014*"evolutionary" + 0.014*"moral" + 0.014*"philosophy" + 0.014*"explanation" + 0.010*"prospect" + 0.010*"problem" + 0.010*"causal"
2021-05-15 14:27:19,110 : INFO : topic #6 (0.075): 0.024*"biology" + 0.015*"theory" + 0.015*"model" + 0.011*"biological" + 0.011*"evidence" + 0.011*"human" + 0.011*"life" + 0.010*"account" + 0.009*"cell" + 0.008*"natural"
2021-05-15 14:27:19,110 : INFO : topic diff=0.050225, rho=0.500000
2021-05-15 14:27:19,111 : DEBUG : bound: at document #0
2021-05-15 14:27:19,143 : INFO : -6.523 per-word bound, 91.9 perplexity estimate based on a held-out corpus of 233 documents with 3130 words
2021-05-15 14:27:19,144 : INFO : PROGRESS: pass 3, at document #233/233
2021-05-15 14:27:19,144 : DEBUG : performing inference on a chunk of 233 documents
2021-05-15 14:27:19,164 : DEBUG : 233/233 documents converged within 50 iterations
2021-05-15 14:27:19,165 : INFO : optimized alpha [0.056660365, 0.057126656, 0.0628129, 0.0568208, 0.060919616, 0.063993275, 0.07102557, 0.063202515, 0.062379632, 0.062627286]
2021-05-15 14:27:19,165 : DEBUG : updating topics
2021-05-15 14:27:19,166 : INFO : topic #0 (0.057): 0.037*"model" + 0.024*"biology" + 0.013*"ethical" + 0.013*"prediction" + 0.013*"explanation" + 0.013*"experiment" + 0.013*"owen" + 0.013*"building" + 0.007*"science" + 0.007*"analysis"
2021-05-15 14:27:19,166 : INFO : topic #3 (0.057): 0.012*"contribution" + 0.012*"causality" + 0.012*"editing" + 0.012*"genome" + 0.012*"evolutionary" + 0.012*"research" + 0.012*"biosocial" + 0.012*"today" + 0.006*"synthesis" + 0.006*"art"
2021-05-15 14:27:19,166 : INFO : topic #7 (0.063): 0.018*"war" + 0.014*"cold" + 0.014*"case" + 0.009*"critical" + 0.009*"genetics" + 0.009*"transplantation" + 0.009*"nature" + 0.009*"plant" + 0.009*"historicizing" + 0.009*"science"
2021-05-15 14:27:19,166 : INFO : topic #5 (0.064): 0.032*"evolution" + 0.023*"biological" + 0.023*"biology" + 0.014*"evolutionary" + 0.014*"moral" + 0.014*"philosophy" + 0.014*"explanation" + 0.009*"prospect" + 0.009*"problem" + 0.009*"causal"
2021-05-15 14:27:19,166 : INFO : topic #6 (0.071): 0.024*"biology" + 0.015*"theory" + 0.015*"model" + 0.011*"biological" + 0.011*"evidence" + 0.011*"human" + 0.011*"life" + 0.010*"account" + 0.009*"cell" + 0.008*"natural"
2021-05-15 14:27:19,166 : INFO : topic diff=0.038438, rho=0.447214
2021-05-15 14:27:19,168 : DEBUG : bound: at document #0
2021-05-15 14:27:19,200 : INFO : -6.506 per-word bound, 90.9 perplexity estimate based on a held-out corpus of 233 documents with 3130 words
2021-05-15 14:27:19,200 : INFO : PROGRESS: pass 4, at document #233/233
2021-05-15 14:27:19,200 : DEBUG : performing inference on a chunk of 233 documents
2021-05-15 14:27:19,220 : DEBUG : 233/233 documents converged within 50 iterations
2021-05-15 14:27:19,222 : INFO : optimized alpha [0.053013425, 0.05342555, 0.059075408, 0.053056993, 0.05722242, 0.060542695, 0.06758319, 0.059640683, 0.05869455, 0.05901186]
2021-05-15 14:27:19,222 : DEBUG : updating topics
2021-05-15 14:27:19,223 : INFO : topic #0 (0.053): 0.037*"model" + 0.025*"biology" + 0.013*"ethical" + 0.013*"prediction" + 0.013*"explanation" + 0.013*"experiment" + 0.013*"owen" + 0.013*"building" + 0.007*"science" + 0.007*"analysis"
2021-05-15 14:27:19,223 : INFO : topic #3 (0.053): 0.012*"contribution" + 0.012*"causality" + 0.012*"editing" + 0.012*"genome" + 0.012*"evolutionary" + 0.012*"research" + 0.012*"biosocial" + 0.012*"today" + 0.006*"synthesis" + 0.006*"art"
2021-05-15 14:27:19,223 : INFO : topic #7 (0.060): 0.018*"war" + 0.014*"cold" + 0.014*"case" + 0.009*"critical" + 0.009*"genetics" + 0.009*"transplantation" + 0.009*"nature" + 0.009*"plant" + 0.009*"science" + 0.009*"historicizing"
2021-05-15 14:27:19,223 : INFO : topic #5 (0.061): 0.032*"evolution" + 0.023*"biological" + 0.023*"biology" + 0.014*"moral" + 0.014*"evolutionary" + 0.014*"philosophy" + 0.014*"explanation" + 0.009*"problem" + 0.009*"prospect" + 0.009*"causal"
2021-05-15 14:27:19,223 : INFO : topic #6 (0.068): 0.023*"biology" + 0.015*"theory" + 0.015*"model" + 0.012*"biological" + 0.012*"evidence" + 0.011*"human" + 0.011*"life" + 0.009*"account" + 0.009*"cell" + 0.008*"natural"
2021-05-15 14:27:19,223 : INFO : topic diff=0.028676, rho=0.408248
2021-05-15 14:27:19,225 : DEBUG : bound: at document #0
2021-05-15 14:27:19,260 : INFO : -6.496 per-word bound, 90.2 perplexity estimate based on a held-out corpus of 233 documents with 3130 words
2021-05-15 14:27:19,260 : INFO : PROGRESS: pass 5, at document #233/233
2021-05-15 14:27:19,260 : DEBUG : performing inference on a chunk of 233 documents
2021-05-15 14:27:19,287 : DEBUG : 233/233 documents converged within 50 iterations
2021-05-15 14:27:19,288 : INFO : optimized alpha [0.0500175, 0.050387673, 0.055985127, 0.049975347, 0.054170012, 0.057657115, 0.06467174, 0.05667646, 0.055644695, 0.05601206]
2021-05-15 14:27:19,288 : DEBUG : updating topics
2021-05-15 14:27:19,289 : INFO : topic #0 (0.050): 0.038*"model" + 0.025*"biology" + 0.013*"ethical" + 0.013*"prediction" + 0.013*"explanation" + 0.013*"experiment" + 0.013*"owen" + 0.013*"building" + 0.007*"science" + 0.007*"analysis"
2021-05-15 14:27:19,289 : INFO : topic #3 (0.050): 0.012*"contribution" + 0.012*"editing" + 0.012*"causality" + 0.012*"genome" + 0.012*"evolutionary" + 0.012*"research" + 0.012*"biosocial" + 0.012*"today" + 0.006*"synthesis" + 0.006*"art"
2021-05-15 14:27:19,289 : INFO : topic #7 (0.057): 0.018*"war" + 0.014*"cold" + 0.014*"case" + 0.009*"critical" + 0.009*"genetics" + 0.009*"transplantation" + 0.009*"nature" + 0.009*"plant" + 0.009*"science" + 0.009*"law"
2021-05-15 14:27:19,289 : INFO : topic #5 (0.058): 0.032*"evolution" + 0.023*"biological" + 0.023*"biology" + 0.014*"moral" + 0.014*"evolutionary" + 0.014*"philosophy" + 0.014*"explanation" + 0.009*"problem" + 0.009*"prospect" + 0.009*"causal"
2021-05-15 14:27:19,289 : INFO : topic #6 (0.065): 0.023*"biology" + 0.015*"theory" + 0.015*"model" + 0.012*"biological" + 0.012*"evidence" + 0.012*"human" + 0.011*"life" + 0.009*"cell" + 0.009*"account" + 0.008*"natural"
2021-05-15 14:27:19,289 : INFO : topic diff=0.020612, rho=0.377964
2021-05-15 14:27:19,291 : DEBUG : bound: at document #0
2021-05-15 14:27:19,322 : INFO : -6.489 per-word bound, 89.8 perplexity estimate based on a held-out corpus of 233 documents with 3130 words
2021-05-15 14:27:19,322 : INFO : PROGRESS: pass 6, at document #233/233
2021-05-15 14:27:19,322 : DEBUG : performing inference on a chunk of 233 documents
2021-05-15 14:27:19,341 : DEBUG : 233/233 documents converged within 50 iterations
2021-05-15 14:27:19,342 : INFO : optimized alpha [0.047493603, 0.047830135, 0.05336778, 0.04738622, 0.05158795, 0.055190455, 0.062160116, 0.05415264, 0.0530596, 0.053464144]
2021-05-15 14:27:19,342 : DEBUG : updating topics
2021-05-15 14:27:19,343 : INFO : topic #3 (0.047): 0.012*"contribution" + 0.012*"editing" + 0.012*"causality" + 0.012*"genome" + 0.012*"evolutionary" + 0.012*"research" + 0.012*"biosocial" + 0.012*"today" + 0.006*"synthesis" + 0.006*"art"
2021-05-15 14:27:19,343 : INFO : topic #0 (0.047): 0.038*"model" + 0.025*"biology" + 0.013*"ethical" + 0.013*"prediction" + 0.013*"explanation" + 0.013*"experiment" + 0.013*"owen" + 0.013*"building" + 0.007*"science" + 0.007*"analysis"
2021-05-15 14:27:19,343 : INFO : topic #7 (0.054): 0.018*"war" + 0.014*"cold" + 0.014*"case" + 0.009*"critical" + 0.009*"genetics" + 0.009*"transplantation" + 0.009*"nature" + 0.009*"plant" + 0.009*"science" + 0.009*"law"
2021-05-15 14:27:19,343 : INFO : topic #5 (0.055): 0.032*"evolution" + 0.023*"biological" + 0.023*"biology" + 0.014*"moral" + 0.014*"evolutionary" + 0.014*"philosophy" + 0.014*"explanation" + 0.009*"problem" + 0.009*"prospect" + 0.009*"causal"
2021-05-15 14:27:19,343 : INFO : topic #6 (0.062): 0.023*"biology" + 0.015*"theory" + 0.015*"model" + 0.012*"biological" + 0.012*"evidence" + 0.012*"human" + 0.011*"life" + 0.009*"cell" + 0.008*"account" + 0.008*"natural"
2021-05-15 14:27:19,344 : INFO : topic diff=0.014588, rho=0.353553
2021-05-15 14:27:19,345 : DEBUG : bound: at document #0
2021-05-15 14:27:19,375 : INFO : -6.484 per-word bound, 89.5 perplexity estimate based on a held-out corpus of 233 documents with 3130 words
2021-05-15 14:27:19,375 : INFO : PROGRESS: pass 7, at document #233/233
2021-05-15 14:27:19,375 : DEBUG : performing inference on a chunk of 233 documents
2021-05-15 14:27:19,394 : DEBUG : 233/233 documents converged within 50 iterations
2021-05-15 14:27:19,395 : INFO : optimized alpha [0.045325972, 0.04563488, 0.05110967, 0.045167636, 0.049362674, 0.053045843, 0.059959646, 0.051965687, 0.050827935, 0.05126075]
2021-05-15 14:27:19,395 : DEBUG : updating topics
2021-05-15 14:27:19,397 : INFO : topic #3 (0.045): 0.012*"contribution" + 0.012*"editing" + 0.012*"causality" + 0.012*"genome" + 0.012*"evolutionary" + 0.012*"research" + 0.012*"biosocial" + 0.012*"today" + 0.006*"synthesis" + 0.006*"art"
2021-05-15 14:27:19,397 : INFO : topic #0 (0.045): 0.038*"model" + 0.025*"biology" + 0.013*"ethical" + 0.013*"prediction" + 0.013*"experiment" + 0.013*"explanation" + 0.013*"owen" + 0.013*"building" + 0.007*"science" + 0.007*"analysis"
2021-05-15 14:27:19,397 : INFO : topic #7 (0.052): 0.018*"war" + 0.014*"cold" + 0.014*"case" + 0.009*"critical" + 0.009*"genetics" + 0.009*"transplantation" + 0.009*"nature" + 0.009*"science" + 0.009*"plant" + 0.009*"law"
2021-05-15 14:27:19,397 : INFO : topic #5 (0.053): 0.032*"evolution" + 0.023*"biological" + 0.023*"biology" + 0.014*"moral" + 0.014*"evolutionary" + 0.014*"explanation" + 0.014*"philosophy" + 0.009*"problem" + 0.009*"prospect" + 0.009*"causal"
2021-05-15 14:27:19,397 : INFO : topic #6 (0.060): 0.023*"biology" + 0.015*"theory" + 0.015*"model" + 0.012*"biological" + 0.012*"evidence" + 0.012*"human" + 0.011*"life" + 0.009*"cell" + 0.008*"account" + 0.008*"guideline"
2021-05-15 14:27:19,397 : INFO : topic diff=0.010257, rho=0.333333
2021-05-15 14:27:19,398 : DEBUG : bound: at document #0
2021-05-15 14:27:19,429 : INFO : -6.481 per-word bound, 89.3 perplexity estimate based on a held-out corpus of 233 documents with 3130 words
2021-05-15 14:27:19,429 : INFO : PROGRESS: pass 8, at document #233/233
2021-05-15 14:27:19,429 : DEBUG : performing inference on a chunk of 233 documents
2021-05-15 14:27:19,448 : DEBUG : 233/233 documents converged within 50 iterations
2021-05-15 14:27:19,449 : INFO : optimized alpha [0.043435752, 0.04372151, 0.049132805, 0.043236732, 0.047416363, 0.05115587, 0.058007717, 0.050043922, 0.048873145, 0.049327873]
2021-05-15 14:27:19,449 : DEBUG : updating topics
2021-05-15 14:27:19,450 : INFO : topic #3 (0.043): 0.012*"contribution" + 0.012*"editing" + 0.012*"causality" + 0.012*"genome" + 0.012*"evolutionary" + 0.012*"research" + 0.012*"biosocial" + 0.012*"today" + 0.006*"synthesis" + 0.006*"art"
2021-05-15 14:27:19,451 : INFO : topic #0 (0.043): 0.038*"model" + 0.025*"biology" + 0.013*"ethical" + 0.013*"prediction" + 0.013*"experiment" + 0.013*"explanation" + 0.013*"owen" + 0.013*"building" + 0.007*"science" + 0.007*"analysis"
2021-05-15 14:27:19,451 : INFO : topic #7 (0.050): 0.018*"war" + 0.014*"cold" + 0.014*"case" + 0.009*"critical" + 0.009*"genetics" + 0.009*"transplantation" + 0.009*"nature" + 0.009*"law" + 0.009*"science" + 0.009*"plant"
2021-05-15 14:27:19,451 : INFO : topic #5 (0.051): 0.032*"evolution" + 0.023*"biological" + 0.023*"biology" + 0.014*"moral" + 0.014*"evolutionary" + 0.014*"explanation" + 0.014*"philosophy" + 0.009*"problem" + 0.009*"prospect" + 0.009*"medical"
2021-05-15 14:27:19,451 : INFO : topic #6 (0.058): 0.023*"biology" + 0.015*"theory" + 0.015*"model" + 0.012*"biological" + 0.012*"evidence" + 0.012*"human" + 0.012*"life" + 0.009*"cell" + 0.008*"account" + 0.008*"natural"
2021-05-15 14:27:19,451 : INFO : topic diff=0.007209, rho=0.316228
2021-05-15 14:27:19,453 : DEBUG : bound: at document #0
2021-05-15 14:27:19,483 : INFO : -6.477 per-word bound, 89.1 perplexity estimate based on a held-out corpus of 233 documents with 3130 words
2021-05-15 14:27:19,483 : INFO : PROGRESS: pass 9, at document #233/233
2021-05-15 14:27:19,483 : DEBUG : performing inference on a chunk of 233 documents
2021-05-15 14:27:19,502 : DEBUG : 233/233 documents converged within 50 iterations
2021-05-15 14:27:19,503 : INFO : optimized alpha [0.04176692, 0.042032946, 0.04738144, 0.041534856, 0.045693465, 0.0494718, 0.056258544, 0.048335828, 0.04714051, 0.047612417]
2021-05-15 14:27:19,503 : DEBUG : updating topics
2021-05-15 14:27:19,504 : INFO : topic #3 (0.042): 0.012*"contribution" + 0.012*"editing" + 0.012*"causality" + 0.012*"genome" + 0.012*"evolutionary" + 0.012*"research" + 0.012*"biosocial" + 0.012*"today" + 0.006*"synthesis" + 0.006*"art"
2021-05-15 14:27:19,504 : INFO : topic #0 (0.042): 0.038*"model" + 0.025*"biology" + 0.013*"ethical" + 0.013*"explanation" + 0.013*"prediction" + 0.013*"experiment" + 0.013*"owen" + 0.013*"building" + 0.007*"science" + 0.007*"analysis"
2021-05-15 14:27:19,504 : INFO : topic #7 (0.048): 0.018*"war" + 0.014*"cold" + 0.014*"case" + 0.009*"critical" + 0.009*"genetics" + 0.009*"transplantation" + 0.009*"nature" + 0.009*"law" + 0.009*"science" + 0.009*"plant"
2021-05-15 14:27:19,504 : INFO : topic #5 (0.049): 0.032*"evolution" + 0.023*"biological" + 0.023*"biology" + 0.014*"moral" + 0.014*"evolutionary" + 0.014*"explanation" + 0.014*"philosophy" + 0.009*"problem" + 0.009*"medical" + 0.009*"prospect"
2021-05-15 14:27:19,505 : INFO : topic #6 (0.056): 0.023*"biology" + 0.015*"theory" + 0.015*"model" + 0.012*"biological" + 0.012*"evidence" + 0.012*"human" + 0.012*"life" + 0.009*"cell" + 0.008*"account" + 0.008*"guideline"
2021-05-15 14:27:19,505 : INFO : topic diff=0.005084, rho=0.301511
2021-05-15 14:27:19,506 : DEBUG : starting a new internal lifecycle event log for LdaModel
2021-05-15 14:27:19,506 : INFO : LdaModel lifecycle event {'msg': 'trained LdaModel(num_terms=960, num_topics=10, decay=0.5, chunksize=2000) in 0.69s', 'datetime': '2021-05-15T14:27:19.506058', 'gensim': '4.0.1', 'python': '3.9.4 (default, Apr 20 2021, 15:51:38) \n[GCC 10.2.0]', 'platform': 'Linux-5.11.16-arch1-1-x86_64-with-glibc2.33', 'event': 'created'}
2021-05-15 14:27:19,507 : DEBUG : Setting topics to those of the model: LdaModel(num_terms=960, num_topics=10, decay=0.5, chunksize=2000)
2021-05-15 14:27:19,514 : INFO : COHERENCE: -18.743680151413123
2021-05-15 14:27:32,868 : INFO : MATCH PERCENT: 0.15393419170243205
