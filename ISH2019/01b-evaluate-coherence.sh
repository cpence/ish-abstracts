#!/bin/bash
# Display the coherence values for the given set of topic models, in gnuplot,
# for manual inspection.

data_str=""

for file in 01a/Log-*.txt
do
  num=`echo $file | sed 's@01a/Log-\([0-9]*\).txt@\1@'`
  coh=`grep "COHERENCE:" $file | sed 's@.*COHERENCE: @@'`

  data_str="$data_str
$num $coh"
done

echo "$data_str" | sort -g | gnuplot -p -e "unset key; plot '<cat' with linespoints pointtype 7"
