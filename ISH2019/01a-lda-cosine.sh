#!/bin/bash
# Build topic models for numbers of topics from 10 to 250, using LDA
# and cosine similarity.

for i in 10 25 35 50 75 100 125 150 175 200 225 250; do
  python ../src/Similarity-LDA.py Abstracts.tsv --num-topics $i \
    --log 01a/Log-$i.txt \
    --output-similarity 01a/DocumentSimilarity-$i.txt \
    --output-topic-keywords 01a/TopicKeywords-$i.txt \
    --output-document-keywords 01a/DocumentKeywords-$i.txt
done
