#!/bin/bash
# Compute document similarity using WordVec and WMD. This is *VERY* slow, bring
# a fast machine.

python ../src/Similarity-WordVecWMD.py Abstracts.tsv \
  --log 01f/Log.txt --out 01f/DocumentSimilarity.txt
