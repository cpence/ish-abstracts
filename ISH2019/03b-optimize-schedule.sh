#!/bin/bash
# Optimize the schedule using simulated annealing, the method that performed
# best in Manda et al. (2019).

python ../src/OptimizeSchedule.py 02b/RandomSchedule.txt \
  01a/DocumentSimilarity-150.txt --out 03b/Optimized.txt \
  --abstract-file AbstractsTimes.tsv --prefs-column 2
