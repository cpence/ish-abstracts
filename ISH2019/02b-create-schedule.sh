#!/bin/bash
# Create a random schedule that we'll populate with the talks. The structure
# here is based on the ISH 2021 conference structure: there are 3 "blocks"
# (A, B, and C), and in each block, there are either 10 time-slots (5 days x
# 2 time-slots, for blocks A and B) or 4 time-slots (2 days x 2 time-slots,
# for block C).

python ../src/RandomSchedule.py --abstract-file AbstractsTimes.tsv \
  --prefs-column 2 --blocks 3 --time-slots 10,10,4 --talks 3 \
  --out 02b/RandomSchedule.txt
