#!/bin/bash
# Run word2vec distance calculation on the corpus, with soft cosine
# distance calculation.

python ../src/Similarity-WordVecSoftCosine.py Abstracts.tsv \
  --log 01e/Log.txt --out 01e/DocumentSimilarity.txt
