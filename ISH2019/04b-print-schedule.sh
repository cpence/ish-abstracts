#!/bin/bash
# Print out the best (or the selected) schedule from the optimized run.

python ../src/PrintSchedule.py 03b/Optimized.txt AbstractsTimes.tsv \
  --prefs-column 2 > ScheduleConstraints.txt
