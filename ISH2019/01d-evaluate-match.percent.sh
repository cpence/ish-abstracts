#!/bin/bash
# Display the match percent values for the given set of topic models, in
# gnuplot, for manual inspection.

data_str=""

for file in 01c/Log-*.txt
do
  num=`echo $file | sed 's@01c/Log-\([0-9]*\).txt@\1@'`
  coh=`grep "MATCH PERCENT:" $file | sed 's@.*MATCH PERCENT: @@'`

  data_str="$data_str
$num $coh"
done

echo "$data_str" | sort -g | gnuplot -p -e "unset key; plot '<cat' with linespoints pointtype 7"
