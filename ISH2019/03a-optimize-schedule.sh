#!/bin/bash
# Optimize the schedule using simulated annealing, the method that performed
# best in Manda et al. (2019).

python ../src/OptimizeSchedule.py 02a/RandomSchedule.txt \
  01a/DocumentSimilarity-150.txt --out 03a/Optimized.txt
