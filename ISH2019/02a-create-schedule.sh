#!/bin/bash
# Create a random schedule that we'll populate with the talks. I looked at the
# actual ISH 2019 final program, and came up with the following values:
#
# - three talks per session, though occasionally more or less
# - number of individual-paper sessions per time slot:
#   - 1 - 6
#   - 2 - 6
#   - 3 - 4
#   - 4 - 5
#   - 5 - 6
#   - 6 - 6
#   - 7 - 2
#   - 8 - 5
#   - 9 - 5
#   - 10 - 5
#   - 11 - 5
#   - 12 - 5
#   - 13 - 6
#   - 14 - 6
#   - 15 - 0
#   - 16 - 1
# - total number of individual paper sessions: 73
# - total number of individual paper abstracts in my dataset: 233
#
# Those 233 talks would take 77 sessions of 3 talks a piece. Lovely odd numbers
# that don't divide well into anything.
#
# Let's take 16 time-slots of 5 sessions a piece. That gives us room for 80
# sessions, or 240 talks, which will let us see what the algorithm looks like
# when there's some extra room in the schedule.

python ../src/RandomSchedule.py --num-talks 233 --blocks 1 --time-slots 16 \
  --talks 3 --out 02a/RandomSchedule.txt
