2021-05-15 14:44:46,223 : INFO : adding document #0 to Dictionary(0 unique tokens: [])
2021-05-15 14:44:46,225 : INFO : built Dictionary(960 unique tokens: ['agential', 'construction', 'niche', 'discovery', 'epistemic']...) from 233 documents (total 3130 corpus positions)
2021-05-15 14:44:46,233 : DEBUG : starting a new internal lifecycle event log for Dictionary
2021-05-15 14:44:46,234 : INFO : Dictionary lifecycle event {'msg': "built Dictionary(960 unique tokens: ['agential', 'construction', 'niche', 'discovery', 'epistemic']...) from 233 documents (total 3130 corpus positions)", 'datetime': '2021-05-15T14:44:46.225831', 'gensim': '4.0.1', 'python': '3.9.4 (default, Apr 20 2021, 15:51:38) \n[GCC 10.2.0]', 'platform': 'Linux-5.11.16-arch1-1-x86_64-with-glibc2.33', 'event': 'created'}
2021-05-15 14:44:46,235 : INFO : using autotuned alpha, starting with [0.04, 0.04, 0.04, 0.04, 0.04, 0.04, 0.04, 0.04, 0.04, 0.04, 0.04, 0.04, 0.04, 0.04, 0.04, 0.04, 0.04, 0.04, 0.04, 0.04, 0.04, 0.04, 0.04, 0.04, 0.04]
2021-05-15 14:44:46,236 : INFO : using serial LDA version on this node
2021-05-15 14:44:46,238 : INFO : running online (multi-pass) LDA training, 25 topics, 10 passes over the supplied corpus of 233 documents, updating model once every 233 documents, evaluating perplexity every 233 documents, iterating 50x with a convergence threshold of 0.001000
2021-05-15 14:44:46,239 : DEBUG : bound: at document #0
2021-05-15 14:44:46,320 : INFO : -21.819 per-word bound, 3698579.1 perplexity estimate based on a held-out corpus of 233 documents with 3130 words
2021-05-15 14:44:46,320 : INFO : PROGRESS: pass 0, at document #233/233
2021-05-15 14:44:46,320 : DEBUG : performing inference on a chunk of 233 documents
2021-05-15 14:44:46,386 : DEBUG : 208/233 documents converged within 50 iterations
2021-05-15 14:44:46,387 : INFO : optimized alpha [0.03768551, 0.036149267, 0.037253402, 0.036341988, 0.03765529, 0.03786763, 0.03687157, 0.03653849, 0.035787445, 0.037123658, 0.036529176, 0.03728314, 0.03653488, 0.036315862, 0.03710753, 0.03692015, 0.036330894, 0.035973564, 0.036928058, 0.035774477, 0.03844981, 0.03596462, 0.03749222, 0.03633569, 0.036727484]
2021-05-15 14:44:46,387 : DEBUG : updating topics
2021-05-15 14:44:46,390 : INFO : topic #19 (0.036): 0.031*"action" + 0.020*"tradition" + 0.020*"step" + 0.020*"carrier" + 0.020*"disorder" + 0.020*"biomedicine" + 0.020*"counting" + 0.020*"insect" + 0.020*"genetic" + 0.020*"tay"
2021-05-15 14:44:46,390 : INFO : topic #8 (0.036): 0.033*"prediction" + 0.023*"scientific" + 0.017*"existence" + 0.017*"mechanism" + 0.017*"1869–1959" + 0.017*"loeb" + 0.017*"emil" + 0.017*"continuous" + 0.017*"leo" + 0.017*"shallow"
2021-05-15 14:44:46,390 : INFO : topic #0 (0.038): 0.018*"new" + 0.018*"cognition" + 0.018*"biological" + 0.018*"genetic" + 0.018*"practice" + 0.018*"teaching" + 0.018*"cell" + 0.009*"conceptual" + 0.009*"newport" + 0.009*"tissue"
2021-05-15 14:44:46,390 : INFO : topic #5 (0.038): 0.070*"biology" + 0.023*"explanation" + 0.016*"engineering" + 0.016*"control" + 0.016*"knowledge" + 0.016*"learn" + 0.016*"genetic" + 0.016*"today" + 0.016*"student" + 0.016*"climate"
2021-05-15 14:44:46,390 : INFO : topic #20 (0.038): 0.030*"evolution" + 0.021*"human" + 0.021*"biology" + 0.015*"evolutionary" + 0.014*"modern" + 0.014*"modelling" + 0.014*"mexico" + 0.014*"model" + 0.014*"mapping" + 0.014*"woman"
2021-05-15 14:44:46,390 : INFO : topic diff=21.701698, rho=1.000000
2021-05-15 14:44:46,393 : DEBUG : bound: at document #0
2021-05-15 14:44:46,425 : INFO : -6.472 per-word bound, 88.7 perplexity estimate based on a held-out corpus of 233 documents with 3130 words
2021-05-15 14:44:46,425 : INFO : PROGRESS: pass 1, at document #233/233
2021-05-15 14:44:46,425 : DEBUG : performing inference on a chunk of 233 documents
2021-05-15 14:44:46,445 : DEBUG : 233/233 documents converged within 50 iterations
2021-05-15 14:44:46,446 : INFO : optimized alpha [0.036467407, 0.034193814, 0.03576934, 0.034466684, 0.036228828, 0.03663879, 0.035223477, 0.034837775, 0.033786006, 0.03556355, 0.034629222, 0.035792775, 0.03473227, 0.03443612, 0.035640877, 0.03517766, 0.03445022, 0.0340436, 0.03528897, 0.03367441, 0.037375063, 0.033934325, 0.03568506, 0.034462027, 0.035102423]
2021-05-15 14:44:46,446 : DEBUG : updating topics
2021-05-15 14:44:46,448 : INFO : topic #19 (0.034): 0.035*"action" + 0.020*"tradition" + 0.020*"step" + 0.020*"carrier" + 0.020*"disorder" + 0.020*"biomedicine" + 0.020*"counting" + 0.020*"insect" + 0.020*"genetic" + 0.020*"tay"
2021-05-15 14:44:46,449 : INFO : topic #8 (0.034): 0.031*"prediction" + 0.028*"scientific" + 0.016*"existence" + 0.016*"mechanism" + 0.016*"1869–1959" + 0.016*"loeb" + 0.016*"emil" + 0.016*"continuous" + 0.016*"leo" + 0.016*"shallow"
2021-05-15 14:44:46,449 : INFO : topic #0 (0.036): 0.018*"new" + 0.018*"cognition" + 0.018*"biological" + 0.018*"genetic" + 0.018*"practice" + 0.018*"teaching" + 0.018*"cell" + 0.009*"newport" + 0.009*"conceptual" + 0.009*"tissue"
2021-05-15 14:44:46,449 : INFO : topic #5 (0.037): 0.069*"biology" + 0.023*"explanation" + 0.016*"engineering" + 0.016*"control" + 0.016*"knowledge" + 0.016*"learn" + 0.016*"genetic" + 0.016*"today" + 0.016*"student" + 0.016*"climate"
2021-05-15 14:44:46,449 : INFO : topic #20 (0.037): 0.029*"evolution" + 0.021*"human" + 0.021*"biology" + 0.015*"evolutionary" + 0.014*"modern" + 0.014*"modelling" + 0.014*"mexico" + 0.014*"model" + 0.014*"mapping" + 0.014*"woman"
2021-05-15 14:44:46,449 : INFO : topic diff=0.049721, rho=0.577350
2021-05-15 14:44:46,451 : DEBUG : bound: at document #0
2021-05-15 14:44:46,483 : INFO : -6.404 per-word bound, 84.7 perplexity estimate based on a held-out corpus of 233 documents with 3130 words
2021-05-15 14:44:46,483 : INFO : PROGRESS: pass 2, at document #233/233
2021-05-15 14:44:46,483 : DEBUG : performing inference on a chunk of 233 documents
2021-05-15 14:44:46,518 : DEBUG : 233/233 documents converged within 50 iterations
2021-05-15 14:44:46,519 : INFO : optimized alpha [0.035467032, 0.032657813, 0.034572497, 0.032987326, 0.03507371, 0.03562985, 0.033907488, 0.033483922, 0.032217875, 0.034310795, 0.033131886, 0.034591362, 0.033304866, 0.032953955, 0.034457088, 0.03379264, 0.032967336, 0.03252604, 0.033978917, 0.032036647, 0.0364807, 0.032345567, 0.034251988, 0.032983843, 0.033803295]
2021-05-15 14:44:46,520 : DEBUG : updating topics
2021-05-15 14:44:46,522 : INFO : topic #19 (0.032): 0.037*"action" + 0.019*"tradition" + 0.019*"step" + 0.019*"carrier" + 0.019*"disorder" + 0.019*"biomedicine" + 0.019*"counting" + 0.019*"insect" + 0.019*"genetic" + 0.019*"tay"
2021-05-15 14:44:46,523 : INFO : topic #8 (0.032): 0.031*"prediction" + 0.029*"scientific" + 0.016*"existence" + 0.016*"mechanism" + 0.016*"1869–1959" + 0.016*"loeb" + 0.016*"emil" + 0.016*"continuous" + 0.016*"leo" + 0.016*"shallow"
2021-05-15 14:44:46,523 : INFO : topic #0 (0.035): 0.018*"new" + 0.018*"cognition" + 0.018*"biological" + 0.018*"genetic" + 0.018*"practice" + 0.018*"teaching" + 0.018*"cell" + 0.009*"newport" + 0.009*"conceptual" + 0.009*"tissue"
2021-05-15 14:44:46,523 : INFO : topic #5 (0.036): 0.069*"biology" + 0.023*"explanation" + 0.015*"engineering" + 0.015*"control" + 0.015*"knowledge" + 0.015*"learn" + 0.015*"genetic" + 0.015*"today" + 0.015*"student" + 0.015*"climate"
2021-05-15 14:44:46,523 : INFO : topic #20 (0.036): 0.029*"evolution" + 0.021*"human" + 0.021*"biology" + 0.014*"evolutionary" + 0.014*"modern" + 0.014*"modelling" + 0.014*"mexico" + 0.014*"model" + 0.014*"mapping" + 0.014*"woman"
2021-05-15 14:44:46,523 : INFO : topic diff=0.036819, rho=0.500000
2021-05-15 14:44:46,526 : DEBUG : bound: at document #0
2021-05-15 14:44:46,559 : INFO : -6.369 per-word bound, 82.7 perplexity estimate based on a held-out corpus of 233 documents with 3130 words
2021-05-15 14:44:46,559 : INFO : PROGRESS: pass 3, at document #233/233
2021-05-15 14:44:46,559 : DEBUG : performing inference on a chunk of 233 documents
2021-05-15 14:44:46,578 : DEBUG : 233/233 documents converged within 50 iterations
2021-05-15 14:44:46,579 : INFO : optimized alpha [0.034612264, 0.031390503, 0.03356444, 0.031762686, 0.034097597, 0.03476791, 0.032807693, 0.03235523, 0.030926617, 0.03325921, 0.031893272, 0.033579744, 0.032119736, 0.031727407, 0.033459384, 0.0326393, 0.03174018, 0.03127294, 0.03288326, 0.03069287, 0.035708554, 0.031038588, 0.033060767, 0.031760093, 0.032716576]
2021-05-15 14:44:46,579 : DEBUG : updating topics
2021-05-15 14:44:46,581 : INFO : topic #19 (0.031): 0.037*"action" + 0.019*"tradition" + 0.019*"step" + 0.019*"carrier" + 0.019*"disorder" + 0.019*"biomedicine" + 0.019*"counting" + 0.019*"insect" + 0.019*"genetic" + 0.019*"tay"
2021-05-15 14:44:46,582 : INFO : topic #8 (0.031): 0.031*"prediction" + 0.030*"scientific" + 0.016*"existence" + 0.016*"mechanism" + 0.016*"1869–1959" + 0.016*"loeb" + 0.016*"emil" + 0.016*"leo" + 0.016*"continuous" + 0.016*"shallow"
2021-05-15 14:44:46,582 : INFO : topic #0 (0.035): 0.018*"new" + 0.018*"cognition" + 0.018*"biological" + 0.018*"genetic" + 0.018*"practice" + 0.018*"teaching" + 0.018*"cell" + 0.009*"newport" + 0.009*"tissue" + 0.009*"conceptual"
2021-05-15 14:44:46,582 : INFO : topic #5 (0.035): 0.069*"biology" + 0.023*"explanation" + 0.015*"engineering" + 0.015*"control" + 0.015*"knowledge" + 0.015*"learn" + 0.015*"genetic" + 0.015*"today" + 0.015*"student" + 0.015*"climate"
2021-05-15 14:44:46,582 : INFO : topic #20 (0.036): 0.029*"evolution" + 0.021*"human" + 0.021*"biology" + 0.014*"evolutionary" + 0.014*"modern" + 0.014*"modelling" + 0.014*"mexico" + 0.014*"model" + 0.014*"mapping" + 0.014*"woman"
2021-05-15 14:44:46,582 : INFO : topic diff=0.032503, rho=0.447214
2021-05-15 14:44:46,584 : DEBUG : bound: at document #0
2021-05-15 14:44:46,616 : INFO : -6.349 per-word bound, 81.5 perplexity estimate based on a held-out corpus of 233 documents with 3130 words
2021-05-15 14:44:46,616 : INFO : PROGRESS: pass 4, at document #233/233
2021-05-15 14:44:46,616 : DEBUG : performing inference on a chunk of 233 documents
2021-05-15 14:44:46,635 : DEBUG : 233/233 documents converged within 50 iterations
2021-05-15 14:44:46,636 : INFO : optimized alpha [0.033863045, 0.030311324, 0.032691278, 0.030716997, 0.033249833, 0.03401252, 0.031861212, 0.03138582, 0.029828852, 0.032350928, 0.030836282, 0.03270373, 0.031105313, 0.030680398, 0.032594744, 0.031649675, 0.030692648, 0.030205172, 0.03193977, 0.029553821, 0.03502592, 0.029928325, 0.032040168, 0.030715099, 0.03178065]
2021-05-15 14:44:46,636 : DEBUG : updating topics
2021-05-15 14:44:46,638 : INFO : topic #19 (0.030): 0.038*"action" + 0.019*"tradition" + 0.019*"step" + 0.019*"carrier" + 0.019*"disorder" + 0.019*"biomedicine" + 0.019*"counting" + 0.019*"insect" + 0.019*"genetic" + 0.019*"tay"
2021-05-15 14:44:46,638 : INFO : topic #8 (0.030): 0.031*"prediction" + 0.030*"scientific" + 0.015*"existence" + 0.015*"mechanism" + 0.015*"1869–1959" + 0.015*"loeb" + 0.015*"emil" + 0.015*"leo" + 0.015*"continuous" + 0.015*"real"
2021-05-15 14:44:46,639 : INFO : topic #0 (0.034): 0.018*"new" + 0.018*"biological" + 0.018*"cognition" + 0.018*"genetic" + 0.018*"practice" + 0.018*"teaching" + 0.018*"cell" + 0.009*"newport" + 0.009*"tissue" + 0.009*"landscape"
2021-05-15 14:44:46,639 : INFO : topic #5 (0.034): 0.069*"biology" + 0.023*"explanation" + 0.015*"engineering" + 0.015*"control" + 0.015*"knowledge" + 0.015*"learn" + 0.015*"genetic" + 0.015*"today" + 0.015*"student" + 0.015*"climate"
2021-05-15 14:44:46,639 : INFO : topic #20 (0.035): 0.029*"evolution" + 0.021*"human" + 0.021*"biology" + 0.014*"evolutionary" + 0.014*"modern" + 0.014*"modelling" + 0.014*"mexico" + 0.014*"model" + 0.014*"mapping" + 0.014*"woman"
2021-05-15 14:44:46,639 : INFO : topic diff=0.028579, rho=0.408248
2021-05-15 14:44:46,641 : DEBUG : bound: at document #0
2021-05-15 14:44:46,673 : INFO : -6.336 per-word bound, 80.8 perplexity estimate based on a held-out corpus of 233 documents with 3130 words
2021-05-15 14:44:46,673 : INFO : PROGRESS: pass 5, at document #233/233
2021-05-15 14:44:46,673 : DEBUG : performing inference on a chunk of 233 documents
2021-05-15 14:44:46,692 : DEBUG : 233/233 documents converged within 50 iterations
2021-05-15 14:44:46,693 : INFO : optimized alpha [0.033194475, 0.029371804, 0.031919982, 0.029804517, 0.03249926, 0.033338517, 0.03102975, 0.03053568, 0.028874472, 0.03155054, 0.02991442, 0.031930078, 0.030218264, 0.029766977, 0.03183064, 0.03078251, 0.029778771, 0.029275069, 0.031110521, 0.028566014, 0.034412295, 0.028963711, 0.031146973, 0.029803162, 0.030957943]
2021-05-15 14:44:46,693 : DEBUG : updating topics
2021-05-15 14:44:46,695 : INFO : topic #19 (0.029): 0.038*"action" + 0.019*"tradition" + 0.019*"step" + 0.019*"carrier" + 0.019*"disorder" + 0.019*"counting" + 0.019*"biomedicine" + 0.019*"insect" + 0.019*"genetic" + 0.019*"bee"
2021-05-15 14:44:46,695 : INFO : topic #8 (0.029): 0.031*"prediction" + 0.030*"scientific" + 0.015*"existence" + 0.015*"mechanism" + 0.015*"1869–1959" + 0.015*"loeb" + 0.015*"emil" + 0.015*"leo" + 0.015*"continuous" + 0.015*"real"
2021-05-15 14:44:46,695 : INFO : topic #0 (0.033): 0.018*"new" + 0.018*"cognition" + 0.018*"biological" + 0.018*"genetic" + 0.018*"teaching" + 0.018*"practice" + 0.018*"cell" + 0.009*"newport" + 0.009*"landscape" + 0.009*"tissue"
2021-05-15 14:44:46,696 : INFO : topic #5 (0.033): 0.069*"biology" + 0.023*"explanation" + 0.015*"engineering" + 0.015*"control" + 0.015*"knowledge" + 0.015*"learn" + 0.015*"genetic" + 0.015*"today" + 0.015*"student" + 0.015*"climate"
2021-05-15 14:44:46,696 : INFO : topic #20 (0.034): 0.029*"evolution" + 0.021*"human" + 0.021*"biology" + 0.014*"evolutionary" + 0.014*"modern" + 0.014*"modelling" + 0.014*"mexico" + 0.014*"model" + 0.014*"mapping" + 0.014*"woman"
2021-05-15 14:44:46,696 : INFO : topic diff=0.023916, rho=0.377964
2021-05-15 14:44:46,699 : DEBUG : bound: at document #0
2021-05-15 14:44:46,731 : INFO : -6.328 per-word bound, 80.4 perplexity estimate based on a held-out corpus of 233 documents with 3130 words
2021-05-15 14:44:46,731 : INFO : PROGRESS: pass 6, at document #233/233
2021-05-15 14:44:46,731 : DEBUG : performing inference on a chunk of 233 documents
2021-05-15 14:44:46,750 : DEBUG : 233/233 documents converged within 50 iterations
2021-05-15 14:44:46,751 : INFO : optimized alpha [0.032589894, 0.028540311, 0.03122868, 0.028995337, 0.031825174, 0.032729108, 0.030288108, 0.029778501, 0.028030867, 0.03083469, 0.029097248, 0.031236809, 0.029430212, 0.028957123, 0.031145502, 0.030010728, 0.028968498, 0.028451513, 0.030370498, 0.027694743, 0.033853836, 0.028111512, 0.030352894, 0.028994437, 0.030223692]
2021-05-15 14:44:46,751 : DEBUG : updating topics
2021-05-15 14:44:46,753 : INFO : topic #19 (0.028): 0.038*"action" + 0.019*"tradition" + 0.019*"step" + 0.019*"carrier" + 0.019*"disorder" + 0.019*"counting" + 0.019*"insect" + 0.019*"biomedicine" + 0.019*"genetic" + 0.019*"bee"
2021-05-15 14:44:46,753 : INFO : topic #8 (0.028): 0.031*"prediction" + 0.030*"scientific" + 0.015*"existence" + 0.015*"1869–1959" + 0.015*"mechanism" + 0.015*"loeb" + 0.015*"emil" + 0.015*"continuous" + 0.015*"leo" + 0.015*"real"
2021-05-15 14:44:46,754 : INFO : topic #0 (0.033): 0.018*"new" + 0.018*"cognition" + 0.018*"biological" + 0.018*"genetic" + 0.018*"practice" + 0.018*"teaching" + 0.018*"cell" + 0.009*"newport" + 0.009*"landscape" + 0.009*"tissue"
2021-05-15 14:44:46,754 : INFO : topic #5 (0.033): 0.069*"biology" + 0.023*"explanation" + 0.015*"engineering" + 0.015*"control" + 0.015*"knowledge" + 0.015*"learn" + 0.015*"genetic" + 0.015*"today" + 0.015*"student" + 0.015*"climate"
2021-05-15 14:44:46,754 : INFO : topic #20 (0.034): 0.029*"evolution" + 0.021*"human" + 0.021*"biology" + 0.014*"evolutionary" + 0.014*"modelling" + 0.014*"modern" + 0.014*"mexico" + 0.014*"model" + 0.014*"mapping" + 0.014*"woman"
2021-05-15 14:44:46,754 : INFO : topic diff=0.019025, rho=0.353553
2021-05-15 14:44:46,756 : DEBUG : bound: at document #0
2021-05-15 14:44:46,788 : INFO : -6.323 per-word bound, 80.1 perplexity estimate based on a held-out corpus of 233 documents with 3130 words
2021-05-15 14:44:46,788 : INFO : PROGRESS: pass 7, at document #233/233
2021-05-15 14:44:46,788 : DEBUG : performing inference on a chunk of 233 documents
2021-05-15 14:44:46,807 : DEBUG : 233/233 documents converged within 50 iterations
2021-05-15 14:44:46,808 : INFO : optimized alpha [0.032037504, 0.027795002, 0.03060205, 0.028268732, 0.031213041, 0.032172393, 0.029618686, 0.029095983, 0.02727553, 0.030187005, 0.028363777, 0.030608483, 0.02872143, 0.028230036, 0.030524224, 0.029315468, 0.028241053, 0.027713027, 0.029702302, 0.026916107, 0.033340722, 0.02734886, 0.029638255, 0.028268222, 0.029560635]
2021-05-15 14:44:46,808 : DEBUG : updating topics
2021-05-15 14:44:46,810 : INFO : topic #19 (0.027): 0.038*"action" + 0.019*"tradition" + 0.019*"carrier" + 0.019*"step" + 0.019*"counting" + 0.019*"insect" + 0.019*"disorder" + 0.019*"biomedicine" + 0.019*"genetic" + 0.019*"bee"
2021-05-15 14:44:46,810 : INFO : topic #8 (0.027): 0.031*"prediction" + 0.030*"scientific" + 0.015*"existence" + 0.015*"1869–1959" + 0.015*"mechanism" + 0.015*"emil" + 0.015*"loeb" + 0.015*"real" + 0.015*"continuous" + 0.015*"leo"
2021-05-15 14:44:46,810 : INFO : topic #0 (0.032): 0.018*"new" + 0.018*"cognition" + 0.018*"biological" + 0.018*"genetic" + 0.018*"practice" + 0.018*"teaching" + 0.018*"cell" + 0.009*"newport" + 0.009*"landscape" + 0.009*"feminist"
2021-05-15 14:44:46,810 : INFO : topic #5 (0.032): 0.069*"biology" + 0.023*"explanation" + 0.015*"engineering" + 0.015*"control" + 0.015*"knowledge" + 0.015*"learn" + 0.015*"genetic" + 0.015*"today" + 0.015*"student" + 0.015*"climate"
2021-05-15 14:44:46,811 : INFO : topic #20 (0.033): 0.028*"evolution" + 0.021*"human" + 0.021*"biology" + 0.014*"evolutionary" + 0.014*"modern" + 0.014*"modelling" + 0.014*"mexico" + 0.014*"model" + 0.014*"mapping" + 0.014*"woman"
2021-05-15 14:44:46,811 : INFO : topic diff=0.014545, rho=0.333333
2021-05-15 14:44:46,813 : DEBUG : bound: at document #0
2021-05-15 14:44:46,845 : INFO : -6.319 per-word bound, 79.8 perplexity estimate based on a held-out corpus of 233 documents with 3130 words
2021-05-15 14:44:46,845 : INFO : PROGRESS: pass 8, at document #233/233
2021-05-15 14:44:46,845 : DEBUG : performing inference on a chunk of 233 documents
2021-05-15 14:44:46,865 : DEBUG : 233/233 documents converged within 50 iterations
2021-05-15 14:44:46,866 : INFO : optimized alpha [0.031528663, 0.02712012, 0.03002889, 0.02760973, 0.030652238, 0.031659596, 0.029008737, 0.028474852, 0.026592238, 0.029595576, 0.027698772, 0.030033851, 0.028077688, 0.027570708, 0.029955788, 0.0286831, 0.027581394, 0.027044082, 0.029093264, 0.026212946, 0.032865666, 0.026659256, 0.028988786, 0.027609557, 0.028956218]
2021-05-15 14:44:46,867 : DEBUG : updating topics
2021-05-15 14:44:46,869 : INFO : topic #19 (0.026): 0.038*"action" + 0.019*"tradition" + 0.019*"carrier" + 0.019*"step" + 0.019*"counting" + 0.019*"insect" + 0.019*"disorder" + 0.019*"ant" + 0.019*"bee" + 0.019*"genetic"
2021-05-15 14:44:46,869 : INFO : topic #8 (0.027): 0.031*"prediction" + 0.030*"scientific" + 0.015*"existence" + 0.015*"1869–1959" + 0.015*"mechanism" + 0.015*"emil" + 0.015*"loeb" + 0.015*"leo" + 0.015*"real" + 0.015*"reymond"
2021-05-15 14:44:46,869 : INFO : topic #0 (0.032): 0.018*"new" + 0.018*"cognition" + 0.018*"biological" + 0.018*"teaching" + 0.018*"genetic" + 0.018*"practice" + 0.018*"cell" + 0.009*"landscape" + 0.009*"newport" + 0.009*"feminist"
2021-05-15 14:44:46,869 : INFO : topic #5 (0.032): 0.069*"biology" + 0.023*"explanation" + 0.015*"engineering" + 0.015*"control" + 0.015*"knowledge" + 0.015*"learn" + 0.015*"genetic" + 0.015*"today" + 0.015*"student" + 0.015*"climate"
2021-05-15 14:44:46,869 : INFO : topic #20 (0.033): 0.028*"evolution" + 0.021*"biology" + 0.021*"human" + 0.014*"evolutionary" + 0.014*"modern" + 0.014*"modelling" + 0.014*"mexico" + 0.014*"model" + 0.014*"mapping" + 0.014*"woman"
2021-05-15 14:44:46,869 : INFO : topic diff=0.010826, rho=0.316228
2021-05-15 14:44:46,872 : DEBUG : bound: at document #0
2021-05-15 14:44:46,903 : INFO : -6.316 per-word bound, 79.7 perplexity estimate based on a held-out corpus of 233 documents with 3130 words
2021-05-15 14:44:46,903 : INFO : PROGRESS: pass 9, at document #233/233
2021-05-15 14:44:46,903 : DEBUG : performing inference on a chunk of 233 documents
2021-05-15 14:44:46,922 : DEBUG : 233/233 documents converged within 50 iterations
2021-05-15 14:44:46,923 : INFO : optimized alpha [0.031056756, 0.026503913, 0.029500738, 0.02700716, 0.030134711, 0.03118408, 0.028448638, 0.027905114, 0.025968907, 0.02905145, 0.027090896, 0.029504415, 0.027488286, 0.02696791, 0.02943184, 0.028103346, 0.026978303, 0.026433086, 0.028533824, 0.025572473, 0.032423057, 0.026030403, 0.028393835, 0.027007269, 0.028400978]
2021-05-15 14:44:46,923 : DEBUG : updating topics
2021-05-15 14:44:46,925 : INFO : topic #19 (0.026): 0.038*"action" + 0.019*"tradition" + 0.019*"carrier" + 0.019*"step" + 0.019*"insect" + 0.019*"disorder" + 0.019*"counting" + 0.019*"ant" + 0.019*"biomedicine" + 0.019*"bee"
2021-05-15 14:44:46,926 : INFO : topic #8 (0.026): 0.031*"prediction" + 0.030*"scientific" + 0.015*"existence" + 0.015*"1869–1959" + 0.015*"emil" + 0.015*"mechanism" + 0.015*"loeb" + 0.015*"unit" + 0.015*"leo" + 0.015*"real"
2021-05-15 14:44:46,926 : INFO : topic #0 (0.031): 0.018*"new" + 0.018*"teaching" + 0.018*"genetic" + 0.018*"cognition" + 0.018*"practice" + 0.018*"biological" + 0.018*"cell" + 0.009*"newport" + 0.009*"landscape" + 0.009*"feminist"
2021-05-15 14:44:46,926 : INFO : topic #5 (0.031): 0.069*"biology" + 0.023*"explanation" + 0.015*"engineering" + 0.015*"control" + 0.015*"knowledge" + 0.015*"learn" + 0.015*"genetic" + 0.015*"today" + 0.015*"student" + 0.015*"climate"
2021-05-15 14:44:46,926 : INFO : topic #20 (0.032): 0.028*"evolution" + 0.021*"human" + 0.021*"biology" + 0.014*"evolutionary" + 0.014*"modelling" + 0.014*"modern" + 0.014*"mexico" + 0.014*"model" + 0.014*"mapping" + 0.014*"woman"
2021-05-15 14:44:46,926 : INFO : topic diff=0.007927, rho=0.301511
2021-05-15 14:44:46,927 : DEBUG : starting a new internal lifecycle event log for LdaModel
2021-05-15 14:44:46,927 : INFO : LdaModel lifecycle event {'msg': 'trained LdaModel(num_terms=960, num_topics=25, decay=0.5, chunksize=2000) in 0.69s', 'datetime': '2021-05-15T14:44:46.927908', 'gensim': '4.0.1', 'python': '3.9.4 (default, Apr 20 2021, 15:51:38) \n[GCC 10.2.0]', 'platform': 'Linux-5.11.16-arch1-1-x86_64-with-glibc2.33', 'event': 'created'}
2021-05-15 14:44:46,931 : DEBUG : Setting topics to those of the model: LdaModel(num_terms=960, num_topics=25, decay=0.5, chunksize=2000)
2021-05-15 14:44:46,948 : INFO : COHERENCE: -17.313635271123932
2021-05-15 14:45:02,869 : INFO : MATCH PERCENT: 0.2113733905579399
