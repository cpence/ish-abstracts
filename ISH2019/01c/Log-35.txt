2021-05-15 14:45:05,866 : INFO : adding document #0 to Dictionary(0 unique tokens: [])
2021-05-15 14:45:05,869 : INFO : built Dictionary(960 unique tokens: ['agential', 'construction', 'niche', 'discovery', 'epistemic']...) from 233 documents (total 3130 corpus positions)
2021-05-15 14:45:05,877 : DEBUG : starting a new internal lifecycle event log for Dictionary
2021-05-15 14:45:05,877 : INFO : Dictionary lifecycle event {'msg': "built Dictionary(960 unique tokens: ['agential', 'construction', 'niche', 'discovery', 'epistemic']...) from 233 documents (total 3130 corpus positions)", 'datetime': '2021-05-15T14:45:05.869249', 'gensim': '4.0.1', 'python': '3.9.4 (default, Apr 20 2021, 15:51:38) \n[GCC 10.2.0]', 'platform': 'Linux-5.11.16-arch1-1-x86_64-with-glibc2.33', 'event': 'created'}
2021-05-15 14:45:05,879 : INFO : using autotuned alpha, starting with [0.028571429, 0.028571429, 0.028571429, 0.028571429, 0.028571429, 0.028571429, 0.028571429, 0.028571429, 0.028571429, 0.028571429, 0.028571429, 0.028571429, 0.028571429, 0.028571429, 0.028571429, 0.028571429, 0.028571429, 0.028571429, 0.028571429, 0.028571429, 0.028571429, 0.028571429, 0.028571429, 0.028571429, 0.028571429, 0.028571429, 0.028571429, 0.028571429, 0.028571429, 0.028571429, 0.028571429, 0.028571429, 0.028571429, 0.028571429, 0.028571429]
2021-05-15 14:45:05,879 : INFO : using serial LDA version on this node
2021-05-15 14:45:05,882 : INFO : running online (multi-pass) LDA training, 35 topics, 10 passes over the supplied corpus of 233 documents, updating model once every 233 documents, evaluating perplexity every 233 documents, iterating 50x with a convergence threshold of 0.001000
2021-05-15 14:45:05,883 : DEBUG : bound: at document #0
2021-05-15 14:45:05,964 : INFO : -30.591 per-word bound, 1617711258.4 perplexity estimate based on a held-out corpus of 233 documents with 3130 words
2021-05-15 14:45:05,964 : INFO : PROGRESS: pass 0, at document #233/233
2021-05-15 14:45:05,964 : DEBUG : performing inference on a chunk of 233 documents
2021-05-15 14:45:06,033 : DEBUG : 212/233 documents converged within 50 iterations
2021-05-15 14:45:06,034 : INFO : optimized alpha [0.027009366, 0.027014399, 0.027675198, 0.026217017, 0.027005961, 0.026089236, 0.026483338, 0.027421318, 0.026349714, 0.026321655, 0.026627176, 0.026883308, 0.027721286, 0.027671747, 0.027270429, 0.0264865, 0.027275477, 0.026673404, 0.027015453, 0.026756043, 0.0271471, 0.026879724, 0.026620071, 0.02662334, 0.026619188, 0.027554616, 0.027273968, 0.027422285, 0.026879258, 0.026882894, 0.026753504, 0.02727625, 0.027147632, 0.026617521, 0.026882365]
2021-05-15 14:45:06,035 : DEBUG : updating topics
2021-05-15 14:45:06,037 : INFO : topic #5 (0.026): 0.047*"genome" + 0.047*"1990–2003" + 0.047*"project" + 0.047*"human" + 0.047*"cooperation" + 0.047*"competition" + 0.047*"biology" + 0.047*"big" + 0.001*"interview" + 0.001*"scientist"
2021-05-15 14:45:06,038 : INFO : topic #3 (0.026): 0.049*"virology" + 0.049*"model" + 0.049*"formal" + 0.049*"prospect" + 0.049*"biology" + 0.049*"reason" + 0.049*"philosophy" + 0.001*"baby" + 0.001*"policy" + 0.001*"key"
2021-05-15 14:45:06,038 : INFO : topic #13 (0.028): 0.041*"biology" + 0.031*"explanation" + 0.031*"biological" + 0.021*"law" + 0.021*"early" + 0.021*"process" + 0.018*"war" + 0.015*"woman" + 0.011*"function" + 0.011*"cell"
2021-05-15 14:45:06,038 : INFO : topic #2 (0.028): 0.043*"biology" + 0.029*"science" + 0.022*"explanation" + 0.022*"scientific" + 0.022*"molecular" + 0.022*"evolution" + 0.018*"contribution" + 0.011*"protein" + 0.011*"bois" + 0.011*"behavior"
2021-05-15 14:45:06,038 : INFO : topic #12 (0.028): 0.035*"process" + 0.023*"ontology" + 0.023*"biology" + 0.017*"synthesis" + 0.012*"cell" + 0.012*"therapy" + 0.012*"discovery" + 0.012*"humans" + 0.012*"splicing" + 0.012*"substance"
2021-05-15 14:45:06,038 : INFO : topic diff=31.279711, rho=1.000000
2021-05-15 14:45:06,042 : DEBUG : bound: at document #0
2021-05-15 14:45:06,076 : INFO : -6.424 per-word bound, 85.9 perplexity estimate based on a held-out corpus of 233 documents with 3130 words
2021-05-15 14:45:06,076 : INFO : PROGRESS: pass 1, at document #233/233
2021-05-15 14:45:06,077 : DEBUG : performing inference on a chunk of 233 documents
2021-05-15 14:45:06,097 : DEBUG : 232/233 documents converged within 50 iterations
2021-05-15 14:45:06,099 : INFO : optimized alpha [0.026124075, 0.026129087, 0.027031455, 0.02504452, 0.02604916, 0.024861619, 0.025427315, 0.026652532, 0.025301814, 0.02507151, 0.025634866, 0.026007421, 0.027073584, 0.027030893, 0.026431767, 0.025432711, 0.026583403, 0.025669392, 0.026201846, 0.025822822, 0.026322521, 0.026002308, 0.025625002, 0.025629437, 0.025623586, 0.026996609, 0.02643885, 0.0267265, 0.025932617, 0.026076142, 0.02581939, 0.02637033, 0.026394343, 0.025622597, 0.025936136]
2021-05-15 14:45:06,099 : DEBUG : updating topics
2021-05-15 14:45:06,102 : INFO : topic #5 (0.025): 0.047*"genome" + 0.047*"1990–2003" + 0.047*"project" + 0.047*"human" + 0.047*"cooperation" + 0.047*"competition" + 0.047*"biology" + 0.047*"big" + 0.001*"interview" + 0.001*"scientist"
2021-05-15 14:45:06,102 : INFO : topic #3 (0.025): 0.049*"virology" + 0.049*"model" + 0.049*"formal" + 0.049*"prospect" + 0.049*"biology" + 0.049*"reason" + 0.049*"philosophy" + 0.001*"baby" + 0.001*"policy" + 0.001*"key"
2021-05-15 14:45:06,102 : INFO : topic #13 (0.027): 0.041*"biology" + 0.031*"explanation" + 0.031*"biological" + 0.020*"law" + 0.020*"early" + 0.020*"process" + 0.019*"war" + 0.018*"woman" + 0.011*"function" + 0.011*"cell"
2021-05-15 14:45:06,102 : INFO : topic #12 (0.027): 0.035*"process" + 0.024*"ontology" + 0.024*"biology" + 0.014*"synthesis" + 0.012*"cell" + 0.012*"therapy" + 0.012*"discovery" + 0.012*"humans" + 0.012*"splicing" + 0.012*"substance"
2021-05-15 14:45:06,102 : INFO : topic #2 (0.027): 0.044*"biology" + 0.032*"science" + 0.022*"explanation" + 0.022*"scientific" + 0.022*"molecular" + 0.022*"evolution" + 0.021*"contribution" + 0.011*"protein" + 0.011*"bois" + 0.011*"behavior"
2021-05-15 14:45:06,103 : INFO : topic diff=0.040326, rho=0.577350
2021-05-15 14:45:06,106 : DEBUG : bound: at document #0
2021-05-15 14:45:06,138 : INFO : -6.356 per-word bound, 81.9 perplexity estimate based on a held-out corpus of 233 documents with 3130 words
2021-05-15 14:45:06,138 : INFO : PROGRESS: pass 2, at document #233/233
2021-05-15 14:45:06,138 : DEBUG : performing inference on a chunk of 233 documents
2021-05-15 14:45:06,158 : DEBUG : 233/233 documents converged within 50 iterations
2021-05-15 14:45:06,160 : INFO : optimized alpha [0.02540116, 0.025406135, 0.026494598, 0.024105629, 0.02527163, 0.023881845, 0.024575356, 0.026018685, 0.024456259, 0.024074677, 0.02483072, 0.025291864, 0.02653361, 0.026496243, 0.025744425, 0.02458236, 0.026009373, 0.02485657, 0.025534108, 0.025063502, 0.025646029, 0.025285628, 0.024818882, 0.024824161, 0.02481709, 0.02652704, 0.025754133, 0.026149292, 0.025163006, 0.025413735, 0.02505941, 0.025573462, 0.025772896, 0.024816591, 0.025166813]
2021-05-15 14:45:06,160 : DEBUG : updating topics
2021-05-15 14:45:06,163 : INFO : topic #5 (0.024): 0.047*"genome" + 0.047*"1990–2003" + 0.047*"project" + 0.047*"human" + 0.047*"cooperation" + 0.047*"competition" + 0.047*"biology" + 0.047*"big" + 0.001*"interview" + 0.001*"scientist"
2021-05-15 14:45:06,163 : INFO : topic #9 (0.024): 0.046*"life" + 0.046*"cause" + 0.046*"final" + 0.046*"vindicate" + 0.046*"metaethical" + 0.046*"case" + 0.046*"science" + 0.046*"naturalism" + 0.002*"biological" + 0.002*"ontology"
2021-05-15 14:45:06,163 : INFO : topic #12 (0.027): 0.035*"process" + 0.024*"ontology" + 0.024*"biology" + 0.013*"synthesis" + 0.012*"cell" + 0.012*"therapy" + 0.012*"discovery" + 0.012*"humans" + 0.012*"splicing" + 0.012*"substance"
2021-05-15 14:45:06,163 : INFO : topic #2 (0.026): 0.044*"biology" + 0.032*"science" + 0.022*"explanation" + 0.022*"scientific" + 0.022*"molecular" + 0.022*"evolution" + 0.021*"contribution" + 0.011*"protein" + 0.011*"bois" + 0.011*"behavior"
2021-05-15 14:45:06,163 : INFO : topic #25 (0.027): 0.049*"life" + 0.040*"evolutionary" + 0.030*"evolution" + 0.020*"need" + 0.020*"cultural" + 0.020*"death" + 0.020*"natural" + 0.019*"study" + 0.010*"add" + 0.010*"medieval"
2021-05-15 14:45:06,163 : INFO : topic diff=0.030037, rho=0.500000
2021-05-15 14:45:06,166 : DEBUG : bound: at document #0
2021-05-15 14:45:06,199 : INFO : -6.322 per-word bound, 80.0 perplexity estimate based on a held-out corpus of 233 documents with 3130 words
2021-05-15 14:45:06,199 : INFO : PROGRESS: pass 3, at document #233/233
2021-05-15 14:45:06,199 : DEBUG : performing inference on a chunk of 233 documents
2021-05-15 14:45:06,219 : DEBUG : 233/233 documents converged within 50 iterations
2021-05-15 14:45:06,221 : INFO : optimized alpha [0.024785886, 0.024790816, 0.026029842, 0.023319004, 0.024612514, 0.023063138, 0.023857312, 0.025475025, 0.023743516, 0.023242336, 0.024150541, 0.02468265, 0.026066273, 0.026033271, 0.025157724, 0.02386553, 0.025514618, 0.024169572, 0.02496337, 0.024419121, 0.025068121, 0.024675537, 0.024137214, 0.024143113, 0.024135116, 0.026117552, 0.025169477, 0.02565172, 0.024510331, 0.024847481, 0.024414545, 0.024898453, 0.025239617, 0.024135005, 0.024514368]
2021-05-15 14:45:06,221 : DEBUG : updating topics
2021-05-15 14:45:06,225 : INFO : topic #5 (0.023): 0.047*"genome" + 0.047*"1990–2003" + 0.047*"project" + 0.047*"human" + 0.047*"cooperation" + 0.047*"competition" + 0.047*"biology" + 0.047*"big" + 0.001*"interview" + 0.001*"scientist"
2021-05-15 14:45:06,225 : INFO : topic #9 (0.023): 0.047*"life" + 0.047*"cause" + 0.047*"final" + 0.047*"vindicate" + 0.047*"metaethical" + 0.047*"case" + 0.047*"science" + 0.047*"naturalism" + 0.001*"biological" + 0.001*"ontology"
2021-05-15 14:45:06,225 : INFO : topic #12 (0.026): 0.036*"process" + 0.024*"ontology" + 0.024*"biology" + 0.013*"synthesis" + 0.012*"cell" + 0.012*"therapy" + 0.012*"discovery" + 0.012*"humans" + 0.012*"splicing" + 0.012*"substance"
2021-05-15 14:45:06,225 : INFO : topic #13 (0.026): 0.040*"biology" + 0.030*"explanation" + 0.030*"biological" + 0.020*"law" + 0.020*"early" + 0.020*"process" + 0.020*"war" + 0.020*"woman" + 0.010*"function" + 0.010*"cell"
2021-05-15 14:45:06,225 : INFO : topic #25 (0.026): 0.049*"life" + 0.040*"evolutionary" + 0.030*"evolution" + 0.020*"need" + 0.020*"cultural" + 0.020*"death" + 0.020*"natural" + 0.019*"study" + 0.010*"add" + 0.010*"medieval"
2021-05-15 14:45:06,226 : INFO : topic diff=0.027050, rho=0.447214
2021-05-15 14:45:06,229 : DEBUG : bound: at document #0
2021-05-15 14:45:06,262 : INFO : -6.300 per-word bound, 78.8 perplexity estimate based on a held-out corpus of 233 documents with 3130 words
2021-05-15 14:45:06,263 : INFO : PROGRESS: pass 4, at document #233/233
2021-05-15 14:45:06,263 : DEBUG : performing inference on a chunk of 233 documents
2021-05-15 14:45:06,282 : DEBUG : 233/233 documents converged within 50 iterations
2021-05-15 14:45:06,283 : INFO : optimized alpha [0.024248138, 0.024253014, 0.025617743, 0.022640526, 0.024038354, 0.022358542, 0.023234872, 0.024996752, 0.023125606, 0.022526445, 0.023559162, 0.02415003, 0.025651973, 0.025622664, 0.024643669, 0.023244055, 0.025077553, 0.023572642, 0.024462732, 0.02385731, 0.024561428, 0.024142215, 0.023544649, 0.023551054, 0.023542315, 0.025752228, 0.02465708, 0.025212113, 0.023941608, 0.024350727, 0.023852358, 0.024310837, 0.024770258, 0.023542518, 0.023945812]
2021-05-15 14:45:06,283 : DEBUG : updating topics
2021-05-15 14:45:06,286 : INFO : topic #5 (0.022): 0.047*"genome" + 0.047*"1990–2003" + 0.047*"project" + 0.047*"cooperation" + 0.047*"human" + 0.047*"competition" + 0.047*"biology" + 0.047*"big" + 0.001*"interview" + 0.001*"scientist"
2021-05-15 14:45:06,286 : INFO : topic #9 (0.023): 0.047*"life" + 0.047*"cause" + 0.047*"final" + 0.047*"vindicate" + 0.047*"metaethical" + 0.047*"case" + 0.047*"science" + 0.047*"naturalism" + 0.001*"biological" + 0.001*"ontology"
2021-05-15 14:45:06,287 : INFO : topic #13 (0.026): 0.040*"biology" + 0.030*"explanation" + 0.030*"biological" + 0.020*"law" + 0.020*"early" + 0.020*"process" + 0.020*"war" + 0.020*"woman" + 0.010*"function" + 0.010*"cell"
2021-05-15 14:45:06,287 : INFO : topic #12 (0.026): 0.036*"process" + 0.024*"ontology" + 0.024*"biology" + 0.012*"synthesis" + 0.012*"therapy" + 0.012*"cell" + 0.012*"discovery" + 0.012*"humans" + 0.012*"splicing" + 0.012*"substance"
2021-05-15 14:45:06,287 : INFO : topic #25 (0.026): 0.049*"life" + 0.040*"evolutionary" + 0.030*"evolution" + 0.020*"need" + 0.020*"cultural" + 0.020*"death" + 0.020*"natural" + 0.020*"study" + 0.010*"add" + 0.010*"paradigm"
2021-05-15 14:45:06,287 : INFO : topic diff=0.024817, rho=0.408248
2021-05-15 14:45:06,290 : DEBUG : bound: at document #0
2021-05-15 14:45:06,328 : INFO : -6.285 per-word bound, 78.0 perplexity estimate based on a held-out corpus of 233 documents with 3130 words
2021-05-15 14:45:06,328 : INFO : PROGRESS: pass 5, at document #233/233
2021-05-15 14:45:06,328 : DEBUG : performing inference on a chunk of 233 documents
2021-05-15 14:45:06,348 : DEBUG : 233/233 documents converged within 50 iterations
2021-05-15 14:45:06,349 : INFO : optimized alpha [0.023769319, 0.023774143, 0.025246194, 0.022043295, 0.023528589, 0.021739505, 0.022684595, 0.024568459, 0.022579264, 0.021897798, 0.023034984, 0.023675647, 0.025278514, 0.02525237, 0.024184955, 0.022694543, 0.024684755, 0.023043837, 0.024015555, 0.023358135, 0.024109025, 0.023667261, 0.02301951, 0.023026317, 0.023016986, 0.025421059, 0.024199747, 0.024816992, 0.023436537, 0.023906978, 0.023352873, 0.023789426, 0.024349809, 0.023017436, 0.023440873]
2021-05-15 14:45:06,349 : DEBUG : updating topics
2021-05-15 14:45:06,352 : INFO : topic #5 (0.022): 0.047*"genome" + 0.047*"1990–2003" + 0.047*"project" + 0.047*"competition" + 0.047*"human" + 0.047*"cooperation" + 0.047*"biology" + 0.047*"big" + 0.001*"interview" + 0.001*"scientist"
2021-05-15 14:45:06,352 : INFO : topic #9 (0.022): 0.047*"life" + 0.047*"cause" + 0.047*"final" + 0.047*"vindicate" + 0.047*"case" + 0.047*"metaethical" + 0.047*"science" + 0.047*"naturalism" + 0.001*"biological" + 0.001*"ontology"
2021-05-15 14:45:06,353 : INFO : topic #13 (0.025): 0.040*"biology" + 0.030*"explanation" + 0.030*"biological" + 0.020*"law" + 0.020*"early" + 0.020*"process" + 0.020*"war" + 0.020*"woman" + 0.010*"function" + 0.010*"cell"
2021-05-15 14:45:06,353 : INFO : topic #2 (0.025): 0.044*"biology" + 0.033*"science" + 0.022*"explanation" + 0.022*"molecular" + 0.022*"scientific" + 0.022*"evolution" + 0.022*"contribution" + 0.011*"protein" + 0.011*"bois" + 0.011*"behavior"
2021-05-15 14:45:06,353 : INFO : topic #25 (0.025): 0.049*"life" + 0.040*"evolutionary" + 0.030*"evolution" + 0.020*"need" + 0.020*"cultural" + 0.020*"death" + 0.020*"natural" + 0.020*"study" + 0.010*"add" + 0.010*"paradigm"
2021-05-15 14:45:06,353 : INFO : topic diff=0.022041, rho=0.377964
2021-05-15 14:45:06,356 : DEBUG : bound: at document #0
2021-05-15 14:45:06,390 : INFO : -6.276 per-word bound, 77.5 perplexity estimate based on a held-out corpus of 233 documents with 3130 words
2021-05-15 14:45:06,390 : INFO : PROGRESS: pass 6, at document #233/233
2021-05-15 14:45:06,390 : DEBUG : performing inference on a chunk of 233 documents
2021-05-15 14:45:06,411 : DEBUG : 233/233 documents converged within 50 iterations
2021-05-15 14:45:06,412 : INFO : optimized alpha [0.023337051, 0.023341833, 0.024907025, 0.021509552, 0.023069562, 0.021187227, 0.022190945, 0.024179863, 0.022089114, 0.021337206, 0.022563674, 0.023247302, 0.024937667, 0.024914285, 0.023770057, 0.022201538, 0.024327213, 0.022568595, 0.023610733, 0.022908341, 0.023699619, 0.023238428, 0.022547413, 0.022554545, 0.022544723, 0.025117299, 0.023785992, 0.024457315, 0.022981629, 0.023505233, 0.022902831, 0.023320153, 0.023968184, 0.0225454, 0.022986071]
2021-05-15 14:45:06,412 : DEBUG : updating topics
2021-05-15 14:45:06,415 : INFO : topic #5 (0.021): 0.047*"1990–2003" + 0.047*"genome" + 0.047*"project" + 0.047*"competition" + 0.047*"human" + 0.047*"cooperation" + 0.047*"biology" + 0.047*"big" + 0.001*"interview" + 0.001*"scientist"
2021-05-15 14:45:06,415 : INFO : topic #9 (0.021): 0.047*"life" + 0.047*"cause" + 0.047*"final" + 0.047*"vindicate" + 0.047*"case" + 0.047*"metaethical" + 0.047*"science" + 0.047*"naturalism" + 0.001*"biological" + 0.001*"ontology"
2021-05-15 14:45:06,415 : INFO : topic #13 (0.025): 0.040*"biology" + 0.030*"explanation" + 0.030*"biological" + 0.020*"law" + 0.020*"early" + 0.020*"process" + 0.020*"war" + 0.020*"woman" + 0.010*"function" + 0.010*"cell"
2021-05-15 14:45:06,415 : INFO : topic #12 (0.025): 0.036*"process" + 0.024*"ontology" + 0.024*"biology" + 0.012*"synthesis" + 0.012*"cell" + 0.012*"therapy" + 0.012*"discovery" + 0.012*"humans" + 0.012*"splicing" + 0.012*"substance"
2021-05-15 14:45:06,415 : INFO : topic #25 (0.025): 0.049*"life" + 0.040*"evolutionary" + 0.030*"evolution" + 0.020*"need" + 0.020*"cultural" + 0.020*"death" + 0.020*"natural" + 0.020*"study" + 0.010*"add" + 0.010*"south"
2021-05-15 14:45:06,415 : INFO : topic diff=0.018767, rho=0.353553
2021-05-15 14:45:06,418 : DEBUG : bound: at document #0
2021-05-15 14:45:06,452 : INFO : -6.269 per-word bound, 77.1 perplexity estimate based on a held-out corpus of 233 documents with 3130 words
2021-05-15 14:45:06,452 : INFO : PROGRESS: pass 7, at document #233/233
2021-05-15 14:45:06,452 : DEBUG : performing inference on a chunk of 233 documents
2021-05-15 14:45:06,472 : DEBUG : 233/233 documents converged within 50 iterations
2021-05-15 14:45:06,473 : INFO : optimized alpha [0.022942638, 0.022947375, 0.02459444, 0.021026969, 0.0226517, 0.02068863, 0.021743076, 0.023823675, 0.021644372, 0.020831315, 0.02213518, 0.022856373, 0.024623573, 0.02460264, 0.023390833, 0.0217542, 0.023998544, 0.022136707, 0.023240436, 0.022498658, 0.023325246, 0.0228471, 0.02211826, 0.022125667, 0.022115434, 0.024836155, 0.023407757, 0.02412666, 0.02256742, 0.023137717, 0.02249292, 0.022893155, 0.02361829, 0.022116298, 0.022571951]
2021-05-15 14:45:06,474 : DEBUG : updating topics
2021-05-15 14:45:06,477 : INFO : topic #5 (0.021): 0.047*"genome" + 0.047*"1990–2003" + 0.047*"project" + 0.047*"competition" + 0.047*"human" + 0.047*"cooperation" + 0.047*"biology" + 0.047*"big" + 0.001*"interview" + 0.001*"scientist"
2021-05-15 14:45:06,477 : INFO : topic #9 (0.021): 0.047*"life" + 0.047*"cause" + 0.047*"vindicate" + 0.047*"final" + 0.047*"case" + 0.047*"metaethical" + 0.047*"science" + 0.047*"naturalism" + 0.001*"biological" + 0.001*"ontology"
2021-05-15 14:45:06,477 : INFO : topic #12 (0.025): 0.036*"process" + 0.024*"ontology" + 0.024*"biology" + 0.012*"synthesis" + 0.012*"cell" + 0.012*"therapy" + 0.012*"humans" + 0.012*"discovery" + 0.012*"substance" + 0.012*"splicing"
2021-05-15 14:45:06,477 : INFO : topic #13 (0.025): 0.040*"biology" + 0.030*"explanation" + 0.030*"biological" + 0.020*"law" + 0.020*"early" + 0.020*"process" + 0.020*"war" + 0.020*"woman" + 0.010*"function" + 0.010*"cell"
2021-05-15 14:45:06,477 : INFO : topic #25 (0.025): 0.049*"life" + 0.040*"evolutionary" + 0.030*"evolution" + 0.020*"need" + 0.020*"cultural" + 0.020*"death" + 0.020*"natural" + 0.020*"study" + 0.010*"paradigm" + 0.010*"add"
2021-05-15 14:45:06,477 : INFO : topic diff=0.015348, rho=0.333333
2021-05-15 14:45:06,480 : DEBUG : bound: at document #0
2021-05-15 14:45:06,514 : INFO : -6.265 per-word bound, 76.9 perplexity estimate based on a held-out corpus of 233 documents with 3130 words
2021-05-15 14:45:06,515 : INFO : PROGRESS: pass 8, at document #233/233
2021-05-15 14:45:06,515 : DEBUG : performing inference on a chunk of 233 documents
2021-05-15 14:45:06,535 : DEBUG : 233/233 documents converged within 50 iterations
2021-05-15 14:45:06,536 : INFO : optimized alpha [0.022579696, 0.02258439, 0.02430416, 0.020586539, 0.022267984, 0.020234209, 0.021333054, 0.02349455, 0.021237187, 0.020370414, 0.021742163, 0.022496574, 0.024331938, 0.024313193, 0.023041323, 0.021344626, 0.023694046, 0.021740733, 0.022898907, 0.022122238, 0.022980044, 0.022486947, 0.021724693, 0.021732328, 0.021721749, 0.024574056, 0.02305909, 0.0238203, 0.022186982, 0.022798713, 0.022116316, 0.022501197, 0.023294901, 0.021722777, 0.022191593]
2021-05-15 14:45:06,537 : DEBUG : updating topics
2021-05-15 14:45:06,541 : INFO : topic #5 (0.020): 0.047*"genome" + 0.047*"1990–2003" + 0.047*"project" + 0.047*"competition" + 0.047*"human" + 0.047*"cooperation" + 0.047*"biology" + 0.047*"big" + 0.001*"interview" + 0.001*"scientist"
2021-05-15 14:45:06,542 : INFO : topic #9 (0.020): 0.047*"life" + 0.047*"cause" + 0.047*"vindicate" + 0.047*"final" + 0.047*"case" + 0.047*"metaethical" + 0.047*"naturalism" + 0.047*"science" + 0.001*"biological" + 0.001*"ontology"
2021-05-15 14:45:06,542 : INFO : topic #12 (0.024): 0.036*"process" + 0.024*"ontology" + 0.024*"biology" + 0.012*"synthesis" + 0.012*"cell" + 0.012*"therapy" + 0.012*"discovery" + 0.012*"humans" + 0.012*"substance" + 0.012*"splicing"
2021-05-15 14:45:06,542 : INFO : topic #13 (0.024): 0.040*"biology" + 0.030*"explanation" + 0.030*"biological" + 0.020*"law" + 0.020*"early" + 0.020*"process" + 0.020*"war" + 0.020*"woman" + 0.010*"function" + 0.010*"cell"
2021-05-15 14:45:06,542 : INFO : topic #25 (0.025): 0.049*"life" + 0.039*"evolutionary" + 0.030*"evolution" + 0.020*"need" + 0.020*"cultural" + 0.020*"death" + 0.020*"natural" + 0.020*"study" + 0.010*"add" + 0.010*"paradigm"
2021-05-15 14:45:06,542 : INFO : topic diff=0.012141, rho=0.316228
2021-05-15 14:45:06,545 : DEBUG : bound: at document #0
2021-05-15 14:45:06,597 : INFO : -6.261 per-word bound, 76.7 perplexity estimate based on a held-out corpus of 233 documents with 3130 words
2021-05-15 14:45:06,597 : INFO : PROGRESS: pass 9, at document #233/233
2021-05-15 14:45:06,597 : DEBUG : performing inference on a chunk of 233 documents
2021-05-15 14:45:06,618 : DEBUG : 233/233 documents converged within 50 iterations
2021-05-15 14:45:06,619 : INFO : optimized alpha [0.02224338, 0.022248028, 0.024032922, 0.020181501, 0.0219131, 0.019816827, 0.020954909, 0.023188414, 0.020861631, 0.01994722, 0.02137908, 0.022163114, 0.02405946, 0.024042686, 0.022716984, 0.020966863, 0.023410132, 0.021375041, 0.022581765, 0.021773929, 0.022659581, 0.022153188, 0.02136114, 0.02136897, 0.021358093, 0.024328262, 0.02273549, 0.023534639, 0.021835066, 0.022483906, 0.021767851, 0.022138827, 0.02299402, 0.021359265, 0.021839738]
2021-05-15 14:45:06,619 : DEBUG : updating topics
2021-05-15 14:45:06,623 : INFO : topic #5 (0.020): 0.047*"1990–2003" + 0.047*"genome" + 0.047*"project" + 0.047*"competition" + 0.047*"human" + 0.047*"cooperation" + 0.047*"biology" + 0.047*"big" + 0.001*"interview" + 0.001*"scientist"
2021-05-15 14:45:06,623 : INFO : topic #9 (0.020): 0.047*"life" + 0.047*"cause" + 0.047*"vindicate" + 0.047*"final" + 0.047*"case" + 0.047*"metaethical" + 0.047*"naturalism" + 0.047*"science" + 0.001*"biological" + 0.001*"ontology"
2021-05-15 14:45:06,624 : INFO : topic #13 (0.024): 0.040*"biology" + 0.030*"explanation" + 0.030*"biological" + 0.020*"law" + 0.020*"early" + 0.020*"process" + 0.020*"war" + 0.020*"woman" + 0.010*"function" + 0.010*"cell"
2021-05-15 14:45:06,625 : INFO : topic #12 (0.024): 0.036*"process" + 0.024*"ontology" + 0.024*"biology" + 0.012*"synthesis" + 0.012*"humans" + 0.012*"discovery" + 0.012*"cell" + 0.012*"substance" + 0.012*"influence" + 0.012*"cross"
2021-05-15 14:45:06,625 : INFO : topic #25 (0.024): 0.049*"life" + 0.039*"evolutionary" + 0.030*"evolution" + 0.020*"need" + 0.020*"cultural" + 0.020*"death" + 0.020*"natural" + 0.020*"study" + 0.010*"south" + 0.010*"add"
2021-05-15 14:45:06,625 : INFO : topic diff=0.009363, rho=0.301511
2021-05-15 14:45:06,627 : DEBUG : starting a new internal lifecycle event log for LdaModel
2021-05-15 14:45:06,627 : INFO : LdaModel lifecycle event {'msg': 'trained LdaModel(num_terms=960, num_topics=35, decay=0.5, chunksize=2000) in 0.75s', 'datetime': '2021-05-15T14:45:06.627570', 'gensim': '4.0.1', 'python': '3.9.4 (default, Apr 20 2021, 15:51:38) \n[GCC 10.2.0]', 'platform': 'Linux-5.11.16-arch1-1-x86_64-with-glibc2.33', 'event': 'created'}
2021-05-15 14:45:06,631 : DEBUG : Setting topics to those of the model: LdaModel(num_terms=960, num_topics=35, decay=0.5, chunksize=2000)
2021-05-15 14:45:06,653 : INFO : COHERENCE: -16.96967202879571
2021-05-15 14:45:25,132 : INFO : MATCH PERCENT: 0.24678111587982832
