None
----- Time Slot  1 -----

Session 1:
  - Building the case for comparative neurobiology
  - A Complementary Grounding for Contrasting Topological Explanation with Mechanistic Explanation
  - Theory of mind in nonhuman primates

Session 2:
  - Chromatin landscape, nuclear architecture, and genetic circuits: Integrating multiple perspectives of cell organization and behaviour
  - Susanne K. Langer’s process philosophy of biology
  - Vindicating metaethical naturalism: The case for final causes in the life sciences

Session 3:
  - A farewell to unity: The case for context in host-microbiota research
  - Phylogenetic competition: Defining the selective environment
  - The human nature of E. O. Wilson: A critical response from dialectical neo-Lamarckism

Session 4:
  - Sérgio Henrique Ferreira’s investigation on Bothrops jararaca and its repercussion (1965–1971)
  - Evolution of music and language
  - Stability and the looping effects of human kinds

Session 5:
  - Balancing both the philosophical and practical problems that arise from advanced pediatric medical technologies
  - The Darwinian analogy between artificial and natural selection and its social dimension
  - Materially-continuous genidentity: A synthesis of substance and process ontology


----- Time Slot  2 -----

Session 1:
  - Medical genetics does not need a racial classification
  - The fruitful shrub: The necessity of female pubic hair in early modern Europe
  - Changing the perspective in the study of social learning: From “transmission of information” to “relational developmental process”

Session 2:
  - An immunological view of organismal identity
  - How does competitive research funding affect science? Insights from interviews with scientists
  - Explanations in classical genetics: A modeltheoretic account

Session 3:
  - Scientific conceptions of race and their impact on pictorial representations of Homo sapiens in Mexico
  - A game-theoretic model of Richard Prum’s “aesthetic selection”
  - Beyond cheating: The emotional component

Session 4:
  - Mapping vs. representational accounts of models and simulations
  - “A ripple rather than a revolution”? – John Maynard Smith on Stephen J. Gould’s challenges to neo-Darwinism
  - Self-organization as level property: Towards a non-eliminativist reducctionist approach to organizational closure

Session 5:
  - “Playing with a dog:” Edward Stuart Russell on animal behavior
  - Epidemiologic evidence: Use at your “own risk”?
  - Is a broken clock only right twice a day? Strategies in the calibration and synchronization of biological clocks


----- Time Slot  3 -----

Session 1:
  - 3D/4D metaphysical equivalence: Lessons from the species debate for the metaphysics of change and persistence
  - Blind cooperation: The evolution of redundancy via ignorance
  - Abel Salazar’s contributions on the structure (and function) of the Golgi region in mammalian cells

Session 2:
  - Contingency as a causal force (or not)
  - Cui bono? Experiments on animals in the light of the activity of ethical committees in Poland
  - Extended genotypes? The consequences of adopting non-genetic inheritance to a classical framework

Session 3:
  - Students’ attitudes towards new genetic technologies: Is there a relationship with students’ knowledge of modern genetics and genomics?
  - From gut to glass: Microbial cultures and biological ontologies
  - What does it take to be a psychological primitive? Separating innateness from foundationalism

Session 4:
  - Art’s take on CRISPR: An argument for artistic complications to the metaphors of genome editing
  - Evidence in default
  - Does every cell have a sex? Four approaches

Session 5:
  - A new foundation for the force interpretation of evolutionary theory
  - A novel explanatory strategy in structural biology: Ensemble explanations of protein function
  - Bichat’s two lives


----- Time Slot  4 -----

Session 1:
  - “The body for family”: Biopolitics of living donor liver transplantation in South Korea
  - Anti-essentialist arguments and the “essentialism myth” in the context of pre-Darwinian classification and stereotyping human categories
  - A novel account of activities

Session 2:
  - Constitutive relevance discovery without interventions: Boole meets Bayes nets
  - “But is it cognition?” Interpreting claims about cognition in bacteria, plants and tissues
  - Agential niche construction

Session 3:
  - Bacterial organelles: From metaphor to conceptual change
  - The extended evolutionary synthesis debate: Some ontological, epistemological and historical dimensions
  - When is a model?

Session 4:
  - Transplantation and tomatoes: Retracing Anne McLaren’s cold war search for graft hybrids
  - What research practice shows about fitness and natural selection
  - From biological traces to forensic evidence: A comparative study

Session 5:
  - What is “narrative possibility”?
  - Prospects for philosophy of virology
  - Staging of the natural sciences, or the making of young naturalists (Spain, 1960–1970s)


----- Time Slot  5 -----

Session 1:
  - We need to talk about Richard Owen: A contextual analysis of Owen’s period in the Royal College of Surgeons (1827–1856)
  - Two senses of relative importance and the testability of empirical adaptationism
  - Religious signalling as an evolutionary explanation: What formal modelling can add

Session 2:
  - A process-oriented metaphysics of the Anthropocene
  - Partial proper functions
  - Observation and experiment in agricultural meteorology

Session 3:
  - Bridging between biology and law: European GMO law as a case for applied &HPS
  - Carving brains: Are modules and mechanisms the same?
  - A Darwinian account of war literature

Session 4:
  - Kuhn’s scientific objectivity: The role of anomalies in revolutionary science
  - Multiple realization in systems biology
  - Practices of comparison in early molecular genetics

Session 5:
  - How does plant reproduction challenge sexual criteria of biological individuality?
  - Behavioural studies of humans and non-human primates: Crossed influences
  - A historical epistemology for contemporary phage therapy


----- Time Slot  6 -----

Session 1:
  - The chimerical nature of scientific theories
  - Environmental ethics, meet modelling: Evaluating nature-society dualism with tools from philosophy of science
  - Life, death, and a puzzle of continuity

Session 2:
  - Generalized Darwinism revisited: How a new synthesis changes our view on cultural evolution
  - Cooperation and competition in big biology: The Human Genome Project (1990–2003)
  - Mapping experimental biology: From the laboratory to the biological world

Session 3:
  - Interdisciplinarity and the role of differing conceptual contexts in research programs
  - The naming of the Mikado pheasant: Ornithology, aviculture and zoogeography in the Age of Empires
  - Pregnancy as an evolved reproductive relation

Session 4:
  - Dynamic natural kinds
  - What is a mental symptom?
  - Shark attack! The sensationalizing of ecological issues

Session 5:
  - Forms and limits of reductionism in stem cell research
  - Biological individuality and the new natural kinds philosophy
  - Darwin’s causal argument against special creation


----- Time Slot  7 -----

Session 1:
  - The hidden mechanisms of life. The transformation of the image of biology through the contributions of Emil du Bois-Reymond
  - The modern evolutionary synthesis: A radical reformulation of a structurally flawed paradigm
  - The failure of research guidelines and the CRISPR babies: An historical review and analysis of key human embryo policy guidelines and why they can be ineffective

Session 2:
  - Informal models in biology
  - Engineering regeneration in corals
  - Darwin and disenchantment

Session 3:
  - Shallow predictions vs. mechanistic predictions
  - Animals, pleasure, and animal pleasures
  - You can be Jane Goodall: The history behind today’s far-reaching campaigns encouraging K-12 girls to pursue biology careers, 1960s–today

Session 4:
  - The discovery of RNA splicing at 40: From public memory to epistemic justice
  - Major evolutionary transitions, human evolution and transnaturalization
  - Historicizing the homology problem

Session 5:
  - The exposome as a postgenomic repertoire: Exploring scientific change in contemporary epidemiology
  - How to look at burrows and dams? Proposing an artistic field experiment to engage with the beaver question
  - Organizational etiological teleology: A selectedeffect approach to biological self-regulation


----- Time Slot  8 -----

Session 1:
  - Are conservatism and genetic determinism the same thing?
  - Artificial and natural reconstructions of evolution in the study of lateral gene transfer
  - How do we reason about formal models in biology?

Session 2:
  - Does cultural selection have to be blind?
  - Meta-parsimony
  - Science “Fiction”: The future according to the founders of modern biology

Session 3:
  - What, if anything, do we know about the history of model building in ecology and population biology in the 1960s?
  - Biological functions of episodic memory
  - Carrier screening in Israel: Tay Sachs and other genetic disorders

Session 4:
  - Research ethical requirements and standards in the archiving of eyewitness documents
  - Noise in gene expression and the power of chance in explaining behavior at molecular and cellular levels
  - Justifying the use of temporal idealizations in biological modeling

Session 5:
  - Gender and the measurement of fertility: A case study in critical metrology
  - Exploring biological possibility through synthetic biology
  - Ecology of orchid pollination and scientific explanation: The case study of deception strategy from Darwinian botanists till the current scientific results


----- Time Slot  9 -----

Session 1:
  - From dancing bees to step-counting ants: A productive tradition of finding meaning in the actions of insects
  - Organisms and Darwinian individuals: A metaphysical perspective
  - The contributions of George Newport (1803–1854) to the studies on animal reproduction and possibilities for science teaching

Session 2:
  - Dreaming of a universal biology
  - Anthropocene and the systemic dimension of biological functions. The case of epigenetics, horizontal gene transfer, and antibiotic resistance
  - Catching the protean concept of fitness and its metamorphoses

Session 3:
  - Questioning our evolutionary loneliness: Archaic hominin admixture through a philosophical lens
  - Against naturalistic theories of disease
  - Engineering the environment: Plants, phytotrons, and climate control in the cold war

Session 4:
  - Paper care: Animal research applications as genre
  - Psychic disturbances at organic functions. About an anomaly in the relation between organ and function
  - Norm psychology, normative stress, and polarization

Session 5:
  - Group selection does not act on “belonging to” properties of individuals
  - “Diversity” conflicts with “heterogeneity”
  - Correcting life in the postgenomic era? The rise of genome editing in South Korea


----- Time Slot 10 -----

Session 1:
  - The generalized selected effects theory of function: A critical analysis
  - Biology and the lawlike
  - The Florida Everglades: An essay in ecology

Session 2:
  - Sources of evolutionary contingency and entropy: Chance variation and genetic drift
  - Lessons from embedding in animal behavior science
  - A feeling for the neuron: The status of the “discrete-gating picture” in Hodgkin and Huxley’s model of the action potential

Session 3:
  - Extracting morality from evolution: What a strong evolutionary moral realism means for moral facts
  - Are model organisms like theoretical models?
  - Biological argumentation in early Norwegian salmon farming legislation

Session 4:
  - Canguilhem’s notion of “milieu” and its relevance for environmental epigenetics
  - The making of the “Butterfly Kingdom”: Hans Sauter (1871–1943) and the institutionalization of Japanese entomology in the early twentieth century
  - Pain in psychology, biology and medicine: Some implications for eliminativist and physicalist accounts

Session 5:
  - The human cognition at the era of the holobiont
  - Microbial signaling
  - Nested agency


----- Time Slot 11 -----

Session 1:
  - “Servants of science”: The lives of Rose, Farce and Tarzan, chimpanzees at the Pasteur Institute of Kindia (Guinea) during the French colonial period
  - Can adaptiveness and rationality part ways?
  - A model of phenotypic architectural changes to account for selective and non-selective processes of evolutionary changes

Session 2:
  - Automated judgments: Historicizing algorithms and AI in biomedicine
  - The observation of animals throughout history: The descriptions of snakes
  - Is there such a thing as biological altruism?

Session 3:
  - Ontology and symmetry of evolutionary theories
  - Wallace’s 1858 essay on natural selection: Immediate and remote contexts
  - Error repertoires and the role of negative knowledge in biology

Session 4:
  - From underdetermination to explanation in biology and climate science
  - Types or traits? Typal personality disorder constructs in DSM-5 as natural kinds
  - Configuration of hypotheses with trace observations forms paleobiology’s epistemic challenge

Session 5:
  - Evolution in contention: The mobilization of scientific creationism in Mexico (1973–2000)
  - Population, metapopulation and metahabitat
  - Dealing with genetic uncertainty: Risks, errors, and value


----- Time Slot 12 -----

Session 1:
  - Theory construction by exploration: From the PaJaMo experiment to the synthesis of protein
  - Between science and religion: How biology teachers teach evolution?
  - The only real units in existence? Biological individuals and the transplantation experiments of Leo Loeb (1869–1959)

Session 2:
  - Human infants are born into a social womb: The biosocial philosophy of Adolf Portmann
  - What is evidence for sustainability? Engaging theories and shaping practices in sustainability science
  - Is there a unique zero-force law in evolutionary biology?

Session 3:
  - The evolution of multicellularity and lineage pluralism
  - Forms of insanity: The development of the British psychiatric classification system 1870–1950
  - Extrapolation and cascading uncertainty across dynamical scales in modelling: From ecological modelling to climate downscaling

Session 4:
  - A critical analysis of process ontology
  - Epistemic values, trade-offs, and multiplemodels juxtaposition
  - The art of growing old: Environmental manipulations, physiological temporalities and the advent of Microcebus murinus as a primate model of aging

Session 5:
  - Does the scorpion sting itself to death? The history and philosophy of an “easy” question
  - Direct-to-consumer genetic testing’s red herring: “Genetic ancestry” and personalized medicine
  - An explanatory role for brute facts in biology


----- Time Slot 13 -----

Session 1:
  - A unifying account of function
  - What can we learn from how a parrot learns to speak like a human? A model for referential communication learning
  - Comprehension as compression: Understanding understanding

Session 2:
  - Where in the world is Aldo Leopold?: An examination of “The Land Ethic” in relation to late 20th century animal liberation ethics and environmentalism
  - Epistemic injustice in psychiatry
  - RNA-DNA hybridization: A story of invention and vanished recognition

Session 3:
  - Explaining body’s disruption and natural death. A late-medieval paradigm shift
  - The genesis, reception and development of the tumor angiogenesis research program
  - How to be unique

Session 4:
  - The womb and the war: The construction of women’s body and the conscription of women to military service
  - Is it ever morally permissible to select for deafness in one’s child?
  - Complexity, flexibility, and the modular stance

Session 5:
  - Life stories, life structures, and systems of equations
  - Rethinking early American genetics and its contexts: The castle-east controversy and fancy mouse vs. corn breeding
  - An exploration of the role of technology companies in cultural evolution: A case study of PCR


----- Time Slot 14 -----

Session 1:
  - Shifting ranges, shifting meanings
  - Element analyses or rats’ tail-raising: Total synthesis and determination of insulin in China, 1958–1972
  - Classifying evidence and representation: Two status types of cell biological data images

Session 2:
  - Be fruitful and multiply: Fitness and health in evolutionary mismatch and clinical research
  - B. Spinoza and J. P. Müller: How the Dutch philosopher inspired the father of contemporary physiology
  - Some considerations about the fallacious defense of genetically modified organisms in Mexico

Session 3:
  - Evolution’s invisible hand? From Adam Smith to contemporary evolutionary biology
  - Thou doth process too much: Why process ontology of biology should halt the war on machines and learn to love technology
  - Indigeneity within datasets: DNA sequences journeys and genomic representations about the Karitiana people

Session 4:
  - What can cultural selection explain?
  - Understanding cancer progression and its control: An analysis of immune contribution to metastasis causality
  - Schrödinger’s What is life? 75 years on

Session 5:
  - The many faces of the postgenomic revolution
  - Interactionism through and interlevel relational perspective


----- Time Slot 15 -----

Session 1:
  - The concept of the cell as an active conception of life: The case study of Haeckel, his monism and his monera
  - Materializing trans identities. A dialogue between the new biology and the new feminist materialisms
  - Personalized medicine: A science of the individual?

Session 2:
  - The evolution of moral belief: Support for the Debunker’s causal premise
  - Investigating animal minds from the perspective of mental health
  - Genetic engineering, synthetic biology and reductionists explanations in philosophy of biology

Session 3:
  - Whither the biosocial? Local epistemic goals and collective interests in integrative biosocial research
  - History of the symbiogenic theory of evolution
  - How the choice of model phenomenon matters. Exploring an understudied topic in the history and philosophy of the life sciences

Session 4:
  - The case of mechanistic explanation in molecular biology: Abstraction or idealization?
  - Preserving the evolvability of what? Biological conservation and the objects of persistence
  - Misconceptions behind visual representations in the teaching of evolution in Mexico


----- Time Slot 16 -----

Session 1:
  - Cultural evolutionary theory needs an account of cultural information
  - Who should not be harmed: Human and nonhuman in the animal experimentation debate
  - From infusions to onions: Goethe’s theorybuilding practice

Session 2:
  - Reproducibility in machine learning: The case of AlphaFold
  - Cell culture and the (re-)articulation of biological time
  - Causality and explanation in phylogenetic systematics

Session 3:
  - Into the next stage of the microbiome revolution: impact assessment of microbiome interventions
  - Empirical support and relevance for models of the evolution of cooperation: Problems and prospects
  - Preliminary considerations for an epistemology of organoids

Session 4:
  - Mechanisms as causal pathways
  - From the Cold War to Genomic Era: visual representations of genetics in Mexican high school textbooks
  - Humans, animals, and robots: An evolutionary perspective to moral agency and responsibility




