None
----- Time Slot  1 -----

Session 1:
  - Epidemiologic evidence: Use at your “own risk”?
  - Paper care: Animal research applications as genre
  - Rethinking early American genetics and its contexts: The castle-east controversy and fancy mouse vs. corn breeding

Session 2:
  - Is there such a thing as biological altruism?
  - Are model organisms like theoretical models?
  - “Playing with a dog:” Edward Stuart Russell on animal behavior

Session 3:
  - Who should not be harmed: Human and nonhuman in the animal experimentation debate
  - “But is it cognition?” Interpreting claims about cognition in bacteria, plants and tissues
  - Where in the world is Aldo Leopold?: An examination of “The Land Ethic” in relation to late 20th century animal liberation ethics and environmentalism

Session 4:
  - Cooperation and competition in big biology: The Human Genome Project (1990–2003)
  - What can we learn from how a parrot learns to speak like a human? A model for referential communication learning
  - Multiple realization in systems biology

Session 5:
  - A game-theoretic model of Richard Prum’s “aesthetic selection”
  - Comprehension as compression: Understanding understanding
  - 3D/4D metaphysical equivalence: Lessons from the species debate for the metaphysics of change and persistence


----- Time Slot  2 -----

Session 1:
  - What can cultural selection explain?
  - Norm psychology, normative stress, and polarization
  - Sources of evolutionary contingency and entropy: Chance variation and genetic drift

Session 2:
  - Thou doth process too much: Why process ontology of biology should halt the war on machines and learn to love technology
  - Shark attack! The sensationalizing of ecological issues
  - Evidence in default

Session 3:
  - How the choice of model phenomenon matters. Exploring an understudied topic in the history and philosophy of the life sciences
  - Indigeneity within datasets: DNA sequences journeys and genomic representations about the Karitiana people
  - Element analyses or rats’ tail-raising: Total synthesis and determination of insulin in China, 1958–1972

Session 4:
  - The womb and the war: The construction of women’s body and the conscription of women to military service
  - The modern evolutionary synthesis: A radical reformulation of a structurally flawed paradigm
  - How to look at burrows and dams? Proposing an artistic field experiment to engage with the beaver question

Session 5:
  - Behavioural studies of humans and non-human primates: Crossed influences
  - Dynamic natural kinds
  - From infusions to onions: Goethe’s theorybuilding practice


----- Time Slot  3 -----

Session 1:
  - The fruitful shrub: The necessity of female pubic hair in early modern Europe
  - Misconceptions behind visual representations in the teaching of evolution in Mexico
  - Phylogenetic competition: Defining the selective environment

Session 2:
  - Biology and the lawlike
  - “Servants of science”: The lives of Rose, Farce and Tarzan, chimpanzees at the Pasteur Institute of Kindia (Guinea) during the French colonial period
  - Forms of insanity: The development of the British psychiatric classification system 1870–1950

Session 3:
  - The many faces of the postgenomic revolution
  - Two senses of relative importance and the testability of empirical adaptationism
  - Are conservatism and genetic determinism the same thing?

Session 4:
  - A process-oriented metaphysics of the Anthropocene
  - Between science and religion: How biology teachers teach evolution?
  - Building the case for comparative neurobiology

Session 5:
  - Is there a unique zero-force law in evolutionary biology?
  - Justifying the use of temporal idealizations in biological modeling
  - What is “narrative possibility”?


----- Time Slot  4 -----

Session 1:
  - Science “Fiction”: The future according to the founders of modern biology
  - Nested agency
  - Research ethical requirements and standards in the archiving of eyewitness documents

Session 2:
  - We need to talk about Richard Owen: A contextual analysis of Owen’s period in the Royal College of Surgeons (1827–1856)
  - Schrödinger’s What is life? 75 years on
  - Population, metapopulation and metahabitat

Session 3:
  - Explanations in classical genetics: A modeltheoretic account
  - The Florida Everglades: An essay in ecology
  - Psychic disturbances at organic functions. About an anomaly in the relation between organ and function

Session 4:
  - Gender and the measurement of fertility: A case study in critical metrology
  - The making of the “Butterfly Kingdom”: Hans Sauter (1871–1943) and the institutionalization of Japanese entomology in the early twentieth century
  - Carrier screening in Israel: Tay Sachs and other genetic disorders

Session 5:
  - A Darwinian account of war literature
  - How does competitive research funding affect science? Insights from interviews with scientists
  - Chromatin landscape, nuclear architecture, and genetic circuits: Integrating multiple perspectives of cell organization and behaviour


----- Time Slot  5 -----

Session 1:
  - Changing the perspective in the study of social learning: From “transmission of information” to “relational developmental process”
  - Preserving the evolvability of what? Biological conservation and the objects of persistence
  - Reproducibility in machine learning: The case of AlphaFold

Session 2:
  - Mapping vs. representational accounts of models and simulations
  - Agential niche construction
  - Pain in psychology, biology and medicine: Some implications for eliminativist and physicalist accounts

Session 3:
  - An explanatory role for brute facts in biology
  - Animals, pleasure, and animal pleasures
  - A model of phenotypic architectural changes to account for selective and non-selective processes of evolutionary changes

Session 4:
  - Interdisciplinarity and the role of differing conceptual contexts in research programs
  - The case of mechanistic explanation in molecular biology: Abstraction or idealization?
  - Dreaming of a universal biology

Session 5:
  - The only real units in existence? Biological individuals and the transplantation experiments of Leo Loeb (1869–1959)
  - What is evidence for sustainability? Engaging theories and shaping practices in sustainability science
  - Transplantation and tomatoes: Retracing Anne McLaren’s cold war search for graft hybrids


----- Time Slot  6 -----

Session 1:
  - Extended genotypes? The consequences of adopting non-genetic inheritance to a classical framework
  - Microbial signaling
  - The contributions of George Newport (1803–1854) to the studies on animal reproduction and possibilities for science teaching

Session 2:
  - Forms and limits of reductionism in stem cell research
  - Religious signalling as an evolutionary explanation: What formal modelling can add
  - Prospects for philosophy of virology

Session 3:
  - Historicizing the homology problem
  - Mechanisms as causal pathways
  - The observation of animals throughout history: The descriptions of snakes

Session 4:
  - Bridging between biology and law: European GMO law as a case for applied &HPS
  - What does it take to be a psychological primitive? Separating innateness from foundationalism
  - An immunological view of organismal identity

Session 5:
  - What, if anything, do we know about the history of model building in ecology and population biology in the 1960s?
  - Evolution in contention: The mobilization of scientific creationism in Mexico (1973–2000)
  - Anti-essentialist arguments and the “essentialism myth” in the context of pre-Darwinian classification and stereotyping human categories


----- Time Slot  7 -----

Session 1:
  - Epistemic values, trade-offs, and multiplemodels juxtaposition
  - Mapping experimental biology: From the laboratory to the biological world
  - Materially-continuous genidentity: A synthesis of substance and process ontology

Session 2:
  - Questioning our evolutionary loneliness: Archaic hominin admixture through a philosophical lens
  - Contingency as a causal force (or not)
  - Engineering the environment: Plants, phytotrons, and climate control in the cold war

Session 3:
  - What is a mental symptom?
  - Pregnancy as an evolved reproductive relation
  - History of the symbiogenic theory of evolution

Session 4:
  - Biological functions of episodic memory
  - From biological traces to forensic evidence: A comparative study
  - Direct-to-consumer genetic testing’s red herring: “Genetic ancestry” and personalized medicine

Session 5:
  - Into the next stage of the microbiome revolution: impact assessment of microbiome interventions
  - The human cognition at the era of the holobiont
  - Medical genetics does not need a racial classification


----- Time Slot  8 -----

Session 1:
  - Abel Salazar’s contributions on the structure (and function) of the Golgi region in mammalian cells
  - Students’ attitudes towards new genetic technologies: Is there a relationship with students’ knowledge of modern genetics and genomics?
  - Genetic engineering, synthetic biology and reductionists explanations in philosophy of biology

Session 2:
  - Balancing both the philosophical and practical problems that arise from advanced pediatric medical technologies
  - Darwin and disenchantment
  - Extracting morality from evolution: What a strong evolutionary moral realism means for moral facts

Session 3:
  - Does cultural selection have to be blind?
  - Lessons from embedding in animal behavior science
  - Partial proper functions

Session 4:
  - Theory of mind in nonhuman primates
  - Vindicating metaethical naturalism: The case for final causes in the life sciences
  - Classifying evidence and representation: Two status types of cell biological data images

Session 5:
  - “A ripple rather than a revolution”? – John Maynard Smith on Stephen J. Gould’s challenges to neo-Darwinism
  - An exploration of the role of technology companies in cultural evolution: A case study of PCR
  - The discovery of RNA splicing at 40: From public memory to epistemic justice


----- Time Slot  9 -----

Session 1:
  - Biological argumentation in early Norwegian salmon farming legislation
  - Art’s take on CRISPR: An argument for artistic complications to the metaphors of genome editing
  - Materializing trans identities. A dialogue between the new biology and the new feminist materialisms

Session 2:
  - The exposome as a postgenomic repertoire: Exploring scientific change in contemporary epidemiology
  - You can be Jane Goodall: The history behind today’s far-reaching campaigns encouraging K-12 girls to pursue biology careers, 1960s–today
  - Major evolutionary transitions, human evolution and transnaturalization

Session 3:
  - Evolution’s invisible hand? From Adam Smith to contemporary evolutionary biology
  - The evolution of moral belief: Support for the Debunker’s causal premise
  - Explaining body’s disruption and natural death. A late-medieval paradigm shift

Session 4:
  - Ecology of orchid pollination and scientific explanation: The case study of deception strategy from Darwinian botanists till the current scientific results
  - A unifying account of function
  - Evolution of music and language

Session 5:
  - The generalized selected effects theory of function: A critical analysis
  - Bacterial organelles: From metaphor to conceptual change
  - Noise in gene expression and the power of chance in explaining behavior at molecular and cellular levels


----- Time Slot 10 -----

Session 1:
  - Organisms and Darwinian individuals: A metaphysical perspective
  - Catching the protean concept of fitness and its metamorphoses
  - Blind cooperation: The evolution of redundancy via ignorance

Session 2:
  - “The body for family”: Biopolitics of living donor liver transplantation in South Korea
  - A historical epistemology for contemporary phage therapy
  - The hidden mechanisms of life. The transformation of the image of biology through the contributions of Emil du Bois-Reymond

Session 3:
  - Life stories, life structures, and systems of equations
  - Shifting ranges, shifting meanings
  - Extrapolation and cascading uncertainty across dynamical scales in modelling: From ecological modelling to climate downscaling

Session 4:
  - Meta-parsimony
  - The Darwinian analogy between artificial and natural selection and its social dimension
  - Understanding cancer progression and its control: An analysis of immune contribution to metastasis causality

Session 5:
  - Preliminary considerations for an epistemology of organoids
  - A farewell to unity: The case for context in host-microbiota research
  - Complexity, flexibility, and the modular stance


----- Time Slot 11 -----

Session 1:
  - Group selection does not act on “belonging to” properties of individuals
  - Causality and explanation in phylogenetic systematics
  - B. Spinoza and J. P. Müller: How the Dutch philosopher inspired the father of contemporary physiology

Session 2:
  - Error repertoires and the role of negative knowledge in biology
  - Be fruitful and multiply: Fitness and health in evolutionary mismatch and clinical research
  - Generalized Darwinism revisited: How a new synthesis changes our view on cultural evolution

Session 3:
  - Interactionism through and interlevel relational perspective
  - Engineering regeneration in corals
  - Automated judgments: Historicizing algorithms and AI in biomedicine

Session 4:
  - Ontology and symmetry of evolutionary theories
  - When is a model?
  - Scientific conceptions of race and their impact on pictorial representations of Homo sapiens in Mexico

Session 5:
  - Cui bono? Experiments on animals in the light of the activity of ethical committees in Poland
  - Empirical support and relevance for models of the evolution of cooperation: Problems and prospects
  - From dancing bees to step-counting ants: A productive tradition of finding meaning in the actions of insects


----- Time Slot 12 -----

Session 1:
  - Whither the biosocial? Local epistemic goals and collective interests in integrative biosocial research
  - A feeling for the neuron: The status of the “discrete-gating picture” in Hodgkin and Huxley’s model of the action potential
  - Types or traits? Typal personality disorder constructs in DSM-5 as natural kinds

Session 2:
  - Investigating animal minds from the perspective of mental health
  - Staging of the natural sciences, or the making of young naturalists (Spain, 1960–1970s)
  - From gut to glass: Microbial cultures and biological ontologies

Session 3:
  - The extended evolutionary synthesis debate: Some ontological, epistemological and historical dimensions
  - Cultural evolutionary theory needs an account of cultural information
  - Configuration of hypotheses with trace observations forms paleobiology’s epistemic challenge

Session 4:
  - RNA-DNA hybridization: A story of invention and vanished recognition
  - From the Cold War to Genomic Era: visual representations of genetics in Mexican high school textbooks
  - Biological individuality and the new natural kinds philosophy

Session 5:
  - Constitutive relevance discovery without interventions: Boole meets Bayes nets
  - Life, death, and a puzzle of continuity
  - The naming of the Mikado pheasant: Ornithology, aviculture and zoogeography in the Age of Empires


----- Time Slot 13 -----

Session 1:
  - How to be unique
  - A novel account of activities
  - Is a broken clock only right twice a day? Strategies in the calibration and synchronization of biological clocks

Session 2:
  - The failure of research guidelines and the CRISPR babies: An historical review and analysis of key human embryo policy guidelines and why they can be ineffective
  - Susanne K. Langer’s process philosophy of biology
  - Some considerations about the fallacious defense of genetically modified organisms in Mexico

Session 3:
  - The chimerical nature of scientific theories
  - Kuhn’s scientific objectivity: The role of anomalies in revolutionary science
  - Dealing with genetic uncertainty: Risks, errors, and value

Session 4:
  - Shallow predictions vs. mechanistic predictions
  - Does every cell have a sex? Four approaches
  - Carving brains: Are modules and mechanisms the same?

Session 5:
  - A novel explanatory strategy in structural biology: Ensemble explanations of protein function
  - Is it ever morally permissible to select for deafness in one’s child?
  - Does the scorpion sting itself to death? The history and philosophy of an “easy” question


----- Time Slot 14 -----

Session 1:
  - Personalized medicine: A science of the individual?
  - The human nature of E. O. Wilson: A critical response from dialectical neo-Lamarckism
  - Self-organization as level property: Towards a non-eliminativist reducctionist approach to organizational closure

Session 2:
  - What research practice shows about fitness and natural selection
  - The evolution of multicellularity and lineage pluralism
  - Humans, animals, and robots: An evolutionary perspective to moral agency and responsibility

Session 3:
  - Against naturalistic theories of disease
  - The genesis, reception and development of the tumor angiogenesis research program
  - A critical analysis of process ontology

Session 4:
  - Human infants are born into a social womb: The biosocial philosophy of Adolf Portmann
  - Anthropocene and the systemic dimension of biological functions. The case of epigenetics, horizontal gene transfer, and antibiotic resistance
  - Cell culture and the (re-)articulation of biological time

Session 5:
  - How do we reason about formal models in biology?
  - Bichat’s two lives


----- Time Slot 15 -----

Session 1:
  - Theory construction by exploration: From the PaJaMo experiment to the synthesis of protein
  - “Diversity” conflicts with “heterogeneity”
  - From underdetermination to explanation in biology and climate science

Session 2:
  - Darwin’s causal argument against special creation
  - Correcting life in the postgenomic era? The rise of genome editing in South Korea
  - Organizational etiological teleology: A selectedeffect approach to biological self-regulation

Session 3:
  - Beyond cheating: The emotional component
  - Exploring biological possibility through synthetic biology
  - The art of growing old: Environmental manipulations, physiological temporalities and the advent of Microcebus murinus as a primate model of aging

Session 4:
  - Wallace’s 1858 essay on natural selection: Immediate and remote contexts
  - A new foundation for the force interpretation of evolutionary theory
  - Practices of comparison in early molecular genetics


----- Time Slot 16 -----

Session 1:
  - Canguilhem’s notion of “milieu” and its relevance for environmental epigenetics
  - How does plant reproduction challenge sexual criteria of biological individuality?
  - Can adaptiveness and rationality part ways?

Session 2:
  - A Complementary Grounding for Contrasting Topological Explanation with Mechanistic Explanation
  - Epistemic injustice in psychiatry
  - The concept of the cell as an active conception of life: The case study of Haeckel, his monism and his monera

Session 3:
  - Environmental ethics, meet modelling: Evaluating nature-society dualism with tools from philosophy of science
  - Stability and the looping effects of human kinds
  - Artificial and natural reconstructions of evolution in the study of lateral gene transfer

Session 4:
  - Observation and experiment in agricultural meteorology
  - Informal models in biology
  - Sérgio Henrique Ferreira’s investigation on Bothrops jararaca and its repercussion (1965–1971)




