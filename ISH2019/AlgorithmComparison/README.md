# 4-Way Algorithm Shootout

This folder contains a four-way comparison between the four algorithms for
document similarity that are present in this repository.

Basic insights:

- All of the WordVec methods seem to give very low quality results, on human
  inspection. My hypothesis is that the GloVe model that we're using just
  doesn't capture the strange and technical vocabulary of these abstracts, so
  you get low quality output as a result.
- With respect to the choice between cosine similarity and Hellinger distance,
  there doesn't seem to be much in it. Both get some sessions really right, and
  offer some rather confusing combinations.
- I decided to opt for Hellinger distance, only because it seems to give
  significantly larger values -- cosine similarity was giving very small
  similarities for the majority of document pairs.
