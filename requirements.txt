appdirs==1.4.4
astroid==2.5.5
black==20.8b1
blis==0.7.4
catalogue==2.0.2
certifi==2020.12.5
chardet==4.0.0
click==7.1.2
cymem==2.0.5
en-core-web-sm @ https://github.com/explosion/spacy-models/releases/download/en_core_web_sm-3.0.0/en_core_web_sm-3.0.0-py3-none-any.whl
gensim==4.0.1
idna==2.10
isort==5.8.0
Jinja2==2.11.3
joblib==1.0.1
lazy-object-proxy==1.6.0
MarkupSafe==1.1.1
mccabe==0.6.1
murmurhash==1.0.5
mypy-extensions==0.4.3
nltk==3.6.1
numpy==1.20.2
packaging==20.9
pathspec==0.8.1
pathy==0.4.0
preshed==3.0.5
pydantic==1.7.3
pyemd==0.5.1
pylint==2.8.0
pyparsing==2.4.7
python-Levenshtein==0.12.2
regex==2021.4.4
requests==2.25.1
scipy==1.6.2
smart-open==3.0.0
spacy==3.0.5
spacy-legacy==3.0.2
srsly==2.4.1
thinc==8.0.2
toml==0.10.2
tqdm==4.60.0
typed-ast==1.4.3
typer==0.3.2
typing-extensions==3.7.4.3
urllib3==1.26.4
wasabi==0.8.2
wrapt==1.12.1
