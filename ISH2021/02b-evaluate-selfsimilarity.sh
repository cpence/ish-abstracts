#!/bin/bash
# Display the self-sim values for the given set of topic models, in gnuplot,
# for manual inspection.

data_str=""

for file in 02-topic-models/Log-*.txt
do
  num=`echo $file | sed 's@02-topic-models/Log-\([0-9]*\).txt@\1@'`
  sim=`grep "SELFSIMILARITY:" $file | sed 's@.*SELFSIMILARITY: @@'`

  data_str="$data_str
$num $sim"
done

echo "$data_str" | sort -g | gnuplot -p -e "unset key; plot '<cat' with linespoints pointtype 7"
