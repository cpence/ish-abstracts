#!/bin/bash
# Print the perplexity values, better models will be shown near the top.

data_str=""

for file in 02-topic-models/Log-*.txt
do
  num=`echo $file | sed 's@02-topic-models/Log-\([0-9]*\).txt@\1@'`
  coh=`grep "perplexity estimate" $file | tail -n 1 | sed 's@.*bound, \([.0-9]*\) perplexity estimate.*@\1@'`

  data_str="$data_str
$num $coh"
done

echo "$data_str" | sort -k2 -g
