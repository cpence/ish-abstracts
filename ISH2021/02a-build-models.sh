#!/bin/bash
# Build topic models for numbers of topics from 10 to 350, using LDA
# and cosine similarity.

for i in 5 10 15 20 25 30 35 40 45 50; do
  python ../src/Similarity-LDA.py \
    01-clean-data/Individual.tsv 01-clean-data/Sessions.tsv \
    --title-column 1 --abstract-column 5 \
    --num-topics $i \
    --passes 25 \
    --log 02-topic-models/Log-$i.txt \
    --output-similarity 02-topic-models/DocumentSimilarity-$i.txt \
    --output-topic-keywords 02-topic-models/TopicKeywords-$i.txt \
    --output-document-keywords 02-topic-models/DocumentKeywords-$i.txt
done
