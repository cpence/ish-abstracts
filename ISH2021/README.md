# ISH2021 Scheduling Data

This is the final data used for the initial schedule clustering for
ISHPSSB 2021.

## Raw Data

The 00-initial-data folder contains the raw files that were either received from
CSHL or assembled by the program committee.

## Clean Data

The 01-clean-data folder was obtained by manual cleaning -- passing the .docx
files through `docx2txt`, and adding the time preference information that was
given from member registrations in the .xlsx files collated by the program
committee.

- `01a-check-data.py` --- checks each of the .txt files in the 01-clean-data
  folder, to make sure that they contain the right kind of information (right
  number of columns, etc.). This is basically a typo-detection script.
- `01b-make-tsv.py` --- turns the .txt files into .tsv files, which are the file
  formats needed for the other scripts here.

## Build Models

After model evaluation on the testing ISHPSSB 2019 data, I decided to use LDA
topic models, and to prepare distance with Hellinger distance (see the
`ISH2019/AlgorithmComparison` folder for some sample runs).

- `02a-build-models.sh` --- build topic models at a variety of sizes, from 0 to
  50 topics. We only have 195 documents here, so using many more than 50 topics
  is going to produce serious overfitting.
- `02b-evaluate-coherence.sh` --- show coherence graph (higher is better)
- `02b-evaluate-match-percent.sh` --- show match percent graph (higher is
  better; measures whether keywords are found in abstracts)
- `02b-evaluate-stddevs.sh` --- show the standard deviation of off-diagonal
  document similarities (higher is better; measures how much similarity we're
  detecting between documents)
- `02b-evaluate-selfsimilarity.sh` --- show self-similarity graph (lower is
  better; ensures we aren't generating many similar topics)
- `02b-evaluate-perplexity.sh` --- print perplexities (lower is better; though
  this is the worst metric of the four)

There's five different metrics here, all of which will tell you something
slightly different, and there are clearly trade-offs between them. Here, the
25-topic model has peaks in coherence and stddevs, and shows a good match
percent, while not being ruled out by high self-similarity. This is the model we
used, and I copied it to the 03-selected-model folder.

## Random Schedule

The random schedule was generated with `04-make-schedule.sh`.

## Optimized Schedule

The schedule was optimized with `05-make-optimized.sh`.

## Printing

A simple printed version of the schedule can be made with
`06-print-schedule.sh`, and a more complex CSV version for the rest of the
program committee (for importing into Trello) can be made with
`07-print-committee.sh`.
