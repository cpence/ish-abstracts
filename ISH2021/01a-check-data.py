#!/usr/bin/env python


def check_file(f):
    for i, line in enumerate(f):
        if i % 6 == 1:
            if not line.strip().endswith("]"):
                print("WARNING: Expected line #2 (%d) to end with ]: " % (i) + line)
        elif i % 6 == 2:
            if not line.strip().startswith("["):
                print("WARNING: Expected line #3 (%d) to start with [: " % (i) + line)
        elif i % 6 == 3:
            line = line.strip()
            if (
                line != "A"
                and line != "B"
                and line != "C"
                and line != "AB"
                and line != "BC"
                and line != "AC"
                and line != "NP"
            ):
                print("WARNING: Expected line #4 (%d) to be a time pref: " % (i) + line)
        elif i % 6 == 5:
            if line != "\n":
                print(
                    "MAJOR ERROR: Expected line #6 (%d) to be empty newline: '" % (i)
                    + line
                    + "'"
                )


for filename in ["01-clean-data/Individual.txt", "01-clean-data/Sessions.txt"]:
    with open(filename) as f:
        check_file(f)
