#!/bin/bash
# Create the random schedule with the ISH2021 scheduling parameters.

python ../src/RandomSchedule.py \
  --abstract-file 01-clean-data/Individual.tsv --prefs-column 4 \
  --session-file 01-clean-data/Sessions.tsv \
  --blocks 3 \
  --time-slots 6,9,4 \
  --talks 3 \
  --out 04-random-schedule/RandomSchedule.txt
