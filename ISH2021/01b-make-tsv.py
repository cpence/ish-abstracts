#!/usr/bin/env python
from pathlib import Path


def input_to_tsv(in_file, out_file):
    lines = []
    for i, line in enumerate(in_file):
        if i % 6 != 5:
            lines.append(line.strip())
        if i % 6 == 4:
            out_file.write("	".join(lines) + "\n")
            lines = []


for filename in ["01-clean-data/Individual.txt", "01-clean-data/Sessions.txt"]:
    path = Path(filename)
    out_path = path.with_suffix(".tsv")

    with open(path) as in_file:
        with open(out_path, "w") as out_file:
            input_to_tsv(in_file, out_file)
