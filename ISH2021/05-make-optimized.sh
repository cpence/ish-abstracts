#!/bin/bash
# Optimize the ISH2021 schedule.

python ../src/OptimizeSchedule.py \
  04-random-schedule/RandomSchedule.txt \
  03-selected-model/DocumentSimilarity.txt \
  --abstract-file 01-clean-data/Individual.tsv --prefs-column 4 \
  --session-file 01-clean-data/Sessions.tsv \
  --out 05-optimized-schedule/Optimized.txt
