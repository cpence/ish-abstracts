# Prints out the sessions in a schedule after optimization has been run. You
# can either specify a particular run number that you would like, or without
# one, it'll take the largest value (beware of artifacts).
#
# For information about the available parameters, run:
# > python PrintSchedule.py --help
import argparse
import json


def readabstracts(filenames):
    ret = []
    for infile in filenames:
        with open(infile, "r") as file:
            for line in file:
                strs = line.strip().split("\t")
                ret.append([s.replace('"', '""') for s in strs])
    return ret


def readschedulelist(infile):
    ret = []
    with open(infile, "r") as file:
        for line in file:
            elts = line.strip().split("\t")
            sched = dict(run_number=int(elts[0]), score=float(elts[1]), sched=elts[2])
            ret.append(sched)

    return ret


def findbestschedule(l):
    item = max(l, key=lambda item: item["score"])
    return item["sched"]


def findschedulenum(l, num):
    return [x for x in l if x["run_number"] == num][0]["sched"]


def main():
    parser = argparse.ArgumentParser(
        description="Print a schedule in a human-readable format",
        # formatter_class=argparse.ArgumentDefaultsHelpFormatter,
    )
    parser.add_argument(
        "-t",
        "--title-column",
        type=int,
        default=1,
        metavar="COL",
        help="column in TSV file (1-indexed) containing titles",
    )
    parser.add_argument(
        "-p",
        "--prefs-column",
        type=int,
        metavar="COL",
        help="column in TSV file (1-indexed) containing time preferences (if desired)",
    )
    parser.add_argument(
        "-e",
        "--session-file",
        type=str,
        metavar="FILE",
        nargs="*",
        help="if also scheduling sessions, the file containing session abstracts and time preferences (pass multiple files and they will be concatenated",
    )
    parser.add_argument(
        "-r",
        "--run",
        type=int,
        default=-1,
        metavar="NUM",
        help="the run number you would like to print (defaults to printing the run with the highest solution score)",
    )
    parser.add_argument(
        "schedule", metavar="<schedule.txt>", type=str, help="the schedule to print"
    )
    parser.add_argument(
        "filenames",
        metavar="<file.tsv>",
        type=str,
        nargs="+",
        help="a file with titles and abstracts (multiple files will be concatenated)",
    )

    args = parser.parse_args()

    abstracts = readabstracts(args.filenames)
    if args.session_file:
        session_abstracts = readabstracts(args.session_file)

    schedule_list = readschedulelist(args.schedule)

    if args.run == -1:
        schedule = findbestschedule(schedule_list)
    else:
        schedule = findschedulenum(schedule_list, args.run)

    schedule = json.loads(schedule)

    # Print CSV column headers
    print("Date and Time,Session Title,Session Description")

    # Arbitrary, of course
    time_slot_names = [
        [
            "July 13, 11:05-12:30 (Group A2)",
            "July 14, 9:30-10:55 (Group A1)",
            "July 15, 11:05-12:30 (Group A2)",
            "July 19, 9:30-10:55 (Group A1)",
            "July 19, 11:05-12:30 (Group A2)",
            "July 20, 11:05-12:30 (Group A2)",
        ],
        [
            "July 13, 14:30-15:55 (Group B3)",
            "July 13, 16:05-17:30 (Group B4)",
            "July 14, 16:05-17:30 (Group B4)",
            "July 15, 14:30-15:55 (Group B3)",
            "July 15, 16:05-17:30 (Group B4)",
            "July 19, 14:30-15:55 (Group B3)",
            "July 19, 16:05-17:30 (Group B4)",
            "July 20, 14:30-15:55 (Group B3)",
            "July 20, 16:05-17:30 (Group B4)",
        ],
        [
            "July 14, 19:00-20:25 (Group C5)",
            "July 14, 20:35-22:00 (Group C6)",
            "July 19, 19:00-20:25 (Group C5)",
            "July 19, 20:35-22:00 (Group C6)",
        ],
    ]

    indiv_sn = 1

    for bn, block in enumerate(schedule):
        for tsn, slot in enumerate(block):
            for sn, session in enumerate(slot):
                if len(session) == 0:
                    continue

                if len(session) > 1:
                    # This is an individual papers session
                    descs = []
                    for talk in session:
                        a = abstracts[talk]

                        session_desc = ""
                        session_desc += a[args.title_column - 1]
                        session_desc += "\n"
                        session_desc += a[1]
                        session_desc += "\n"
                        session_desc += a[2]
                        session_desc += "\n"
                        session_desc += a[args.prefs_column - 1]

                        descs.append(session_desc)

                    print(
                        '"%s",Individual Papers %d,"%s"'
                        % (time_slot_names[bn][tsn], indiv_sn, "\n\n".join(descs))
                    )
                    indiv_sn += 1
                else:
                    # This is a planned session
                    session_desc = ""
                    a = session_abstracts[-session[0] - 1]

                    title = a[args.title_column - 1]

                    session_desc += a[1]
                    session_desc += "\n"
                    session_desc += a[2]
                    session_desc += "\n"
                    session_desc += a[args.prefs_column - 1]

                    print(
                        '"%s","%s","%s"'
                        % (time_slot_names[bn][tsn], title, session_desc)
                    )


if __name__ == "__main__":
    main()
