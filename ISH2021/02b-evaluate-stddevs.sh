#!/bin/bash
# Display the standard-deviations for the given set of topic models, in gnuplot,
# for manual inspection.

data_str=""

for file in 02-topic-models/DocumentSimilarity-*.txt
do
  num=`echo $file | sed 's@02-topic-models/DocumentSimilarity-\([0-9]*\).txt@\1@'`
  std=`python ./02b-evaluate-stddev.py $file`

  data_str="$data_str
$num $std"
done

echo "$data_str" | sort -g | gnuplot -p -e "unset key; plot '<cat' with linespoints pointtype 7"
