# Print out the stddev of the off-diagonal elements of a document similarity
# matrix.
import numpy
import sys

with open(sys.argv[1]) as file:
    nums = []
    for i, line in enumerate(file):
        elts = line.strip().split("\t")
        elts.pop(i)
        nums += [float(e) for e in elts]

    print(numpy.std(nums))
