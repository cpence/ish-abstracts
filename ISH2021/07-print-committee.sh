#!/bin/bash
# Print out the best (or the selected) schedule from the optimized run.

python 07-program-committee.py 05-optimized-schedule/Optimized.txt \
  01-clean-data/Individual.tsv \
  --title-column 1 --prefs-column 4 \
  --session-file 01-clean-data/Sessions.tsv > Schedule.csv
